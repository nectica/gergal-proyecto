<?php 

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header.php'; 

?>

<!-- Main Wrapper Header -->

<div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras"
    data-stellar-background-ratio="0.4">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 columns">

                <div class="page-title">

                    <h1 class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        PROGRAMA REVENDEDORES GERGAL BERRIES</h1>

                </div>



            </div>

        </div>

    </div>

</div>

<!-- /Main Wrapper Header -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>





        <div class="row">

            <div class="col-md-12 columns">

                <h3 class="fancy-title text-center color-verde"><span>¿Queres formar parte de nuestro equipo?</span>
                </h3>

            </div>

            <div class="col-md-10 col-md-offset-1">

                <p class="lead text-center">En Gergal ofrecemos la posibilidad de que tengas tu propio negocio<strong>
                        sin inversión inicial, sin jefes y manejando tus horarios.</strong><br>

                </p>

                <div class="white-space space-small"></div>

                <h2 class="script-font text-center color-rojo">¿Cómo?</h2>



                <p class="lead text-center"><strong>Nuestro programa cuenta con una tienda online especial para
                        revendedores.</strong> Una vez que te inscribís podés realizar una compra para comenzar a vender
                    y <strong>el pago lo realizas 15 días después de hacer tu pedido.</strong> O si realizás una nueva
                    compra deberás abonar la compra anterior. </p>



                <div class="white-space space-small"></div>

                <h2 class="script-font text-center color-rojo">¿Queres saber más?</h2>

                <p class="lead text-center" style="font-size:18px">

                    <span class="color-verde">•</span> Los pedidos se realizan con 48hs hábiles de anticipación hasta
                    las 10AM.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Se pedirá un monto mínimo de compra y tener continuidad de 1
                    pedido por mes.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Nosotros te llevamos los productos y vos te ocupas de
                    distribuirlos. Tenemos días de entrega especiales para cada zona.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Las bolsas de productos son de 1kg.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Los jugos vienen en packs de 12 unidades de 500ml de 1 solo
                    sabor. El mínimo es 1 pack si piden congelados o 4 packs si solo piden jugos.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Contamos también con una sección especial de productos saludables
                    para sumar a tu negocio.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> En nuestra tienda online vas a poder ver los precios y el stock
                    actualizado.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Ofrecemos descuentos especiales por cantidad.<br>

                    <span class="color-rojo">-</span><br>

                    <span class="color-verde">•</span> Realizamos promociones especiales para revendedores. <br>

                </p>

            </div>

        </div>





        <div class="row">



            <div class="col-md-10 col-md-offset-1">

                <div class="white-space space-small"></div>

                <p class="lead text-center"><span class="color-verde">Con nuestra propuesta tenes la posibilidad de
                        crear tu propio emprendimiento armando tu propio equipo de revendedores obteniendo así comisión
                        por venta de quipo y pudiendo desarrollar tu negocio. Con el aval de una empresa con productos
                        de excelente calidad sin presupuesto inicial. Podes juntar pedidos y comenzar, te damos el
                        beneficio de abonar tu primer pedido los siguientes 15 días.</span>

                    <br>

                    <br>

                    ¡Te recomendamos utilizar las redes sociales para hacer conocer tu negocio en el mundo virtual!

                    ¡Esperamos seas parte del equipo Gergal!
                </p>

                <div class="white-space space-small"></div>

            </div>

        </div>

        <!-- boton de contacto  -->

        <div class="row">

            <div class="col-sm-12">

                <div class="text-center">

                    <a href="contacto.php" class="btn btn-primary btn-lg">Contacto</a>

                </div>

            </div>



        </div>

        <div class="white-space space-medium"></div>

    </div>

    <!-- /Container -->













</div>

<!-- /Main Container -->



<div class="container">

    <div class="portfolio clearfix">







        <!-- Portfolio Grid -->

        <div class="portfolio-grid  portfolio-4-cols">



            <!-- Portfolio Element -->

            <div class="element isotope-item print" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-04.jpg" width="1200" height="900"
                            alt="Instalaciones 1">

                        <div class="overlay-wrapper-content">

                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-04.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->





            <!-- Portfolio Element -->

            <div class="element isotope-item print" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-05.jpg" width="1200" height="900"
                            alt="instalaciones 2">

                        <div class="overlay-wrapper-content">

                            <div class="overlay-details">

                                <a class="color-white" href="img/demo/portfolio/instalaciones-05.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->











            <!-- Portfolio Element -->

            <div class="element isotope-item print" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-06.jpg" width="1200" height="900"
                            alt="instalaciones 3">

                        <div class="overlay-wrapper-content">

                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-06.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->



            <!-- Portfolio Element -->

            <div class="element isotope-item print" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-07.jpg" width="1200" height="900"
                            alt="instalaciones 4">

                        <div class="overlay-wrapper-content">

                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-07.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->







            <!-- Portfolio Element -->

            <div class="element isotope-item print" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-01.jpg" width="1200" height="900"
                            alt="instalaciones 5">

                        <div class="overlay-wrapper-content">

                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-01.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->



            <!-- Portfolio Element -->

            <div class="element isotope-item print" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-02.jpg" width="1200" height="900"
                            alt="instalaciones 6">

                        <div class="overlay-wrapper-content">

                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-02.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->







            <!-- Portfolio Element -->

            <div class="element isotope-item branding" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-03.jpg" width="1200" height="900"
                            alt="instalaciones 7">

                        <div class="overlay-wrapper-content">



                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-03.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->



            <!-- Portfolio Element -->

            <div class="element isotope-item branding" data-content="#">

                <div class="element-inner animation fadeInRight">



                    <div class="overlay-wrapper">

                        <img src="img/instalaciones/instalaciones-08.jpg" width="1200" height="900"
                            alt="instalaciones 8">

                        <div class="overlay-wrapper-content">



                            <div class="overlay-details">

                                <a class="color-white" href="img/instalaciones/instalaciones-08.jpg"
                                    data-lightbox="image"><span class="livicon" data-n="plus" data-color="#ffffff"
                                        data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>



                            </div>

                            <div class="overlay-bg"></div>

                        </div>

                    </div>



                </div>

            </div>

            <!-- /Portfolio Element -->

        </div>

        <!-- /Portfolio Grid -->



    </div>

    <div class="white-space space-big"></div>

</div>


<!-- Container destacado -->

<!-- Fullsize -->

<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->

<!-- /Container -->



<!-- Footer destacado -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->





<!-- login modal -->

<?php include 'loginView.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts.php'?>

<script>
document.title = "Gergal - Instalaciones";
</script>

</body>

</html>