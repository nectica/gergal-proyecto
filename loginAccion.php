<?php
require_once 'db/Config.php';
require_once 'clases/Login.php';
$login = new Login;
$response = array();

if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
    // check email action
    if($_REQUEST['action'] == 'checkEmail' && !empty($_REQUEST['data'])){
       echo 'check email'.$_REQUEST['data'];
        $x = $login->check_email($_REQUEST['data']);
        echo $x;
        die;
    }
    // logout
    if($_REQUEST['action'] == 'logout'){
        session_destroy();
        header("Location: index.php");
    }
}
// LOGIN accion
if (isset($_POST) && array_key_exists('type',$_POST) && $_POST['type'] == 'login') {
    $l = $login->login($_POST);
    if($l){
        echo 'index.php';
    }else{
        echo 'error.php?msg=1';
    }
    die;
}

// LOG OUT 
if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
    if($_REQUEST['action'] == 'checkEmail' && !empty($_REQUEST['data'])){
       echo 'check email'.$_REQUEST['data'];
        $x = $login->check_email($_REQUEST['data']);
        echo $x;
        die;
    }
}


// SIGN UP DE CLIENTE
if (isset($_POST) && array_key_exists('type',$_POST) && $_POST['type'] == 'signup'){
 $str = array_map(function($v){return trim($v);},$_POST);    
    //  NO ESTOY FILTRANDO NADA 
    $hashed_password = password_hash($str['signup-password'], PASSWORD_DEFAULT);
    // INSERTAR EN DB
    $data = [
        'tipo_usuario_id'=>1,
        'nombre'=>$str['signup-nombre'],
        'apellido'=>$str['signup-apellido'],
        'dni'=>$str['signup-dni'],        
        'email'=>$str['signup-email'],
        'password'=>$hashed_password,
        'nombre_calle'=>$str['signup-calle'],
        'calle_nro'=>$str['signup-numero'],
        'telefono'=>$str['signup-telefono'],
        'localidad'=>$str['signup-localidad'],
        'barrio'=>$str['signup-barrio'],
        'direccion2'=>$str['signup-direccion-2'],
        'fecha_creado'=>date("Y-m-d H:i:s"),
        'fecha_modif'=>date("Y-m-d H:i:s"),
        'estado'=>1,
    ];
//    $x = ;
 //   file_put_contents('rafa_logfile.log', $x, FILE_APPEND);
    if($login->signup($data)){
        $l = $login->login(['login-email'=>$str['signup-email'], 'login-password'=>$str['signup-password']]);
        echo 'OK'; 
    }else{
        echo 'error1';
    };
    exit;
}

// Edicion DE CLIENTE
if (isset($_POST) && array_key_exists('type',$_POST) && $_POST['type'] == 'edit'){
   // $x = "hola2";
   //file_put_contents('rafa_logfile.log', $x, FILE_APPEND);
 $str = array_map(function($v){return trim($v);},$_POST);    
    // INSERTAR EN DB
    $data = [
        'nombre'=>$str['signup-nombre'],
        'apellido'=>$str['signup-apellido'],
        'dni'=>$str['signup-dni'],        
        'email'=>$str['signup-email'],
        'nombre_calle'=>$str['signup-calle'],
        'calle_nro'=>$str['signup-numero'],
        'telefono'=>$str['signup-telefono'],
        'localidad'=>$str['signup-localidad'],
        'barrio'=>$str['signup-barrio'],
        'direccion2'=>$str['signup-direccion-2'],
    ];
	if (isset($str['signup-password'])) {
		if (strlen($str['signup-password'])>0) {
			$hashed_password = password_hash($str['signup-password'], PASSWORD_DEFAULT);		
			$data['password']=$hashed_password;
		}
	}
    //$x = "hola";
    //file_put_contents('rafa_logfile.log', $x, FILE_APPEND);
    if($login->edit($data)){
        $l = $login->reload(['login-email'=>$str['signup-email']]);
        echo 'OK'; 
    }else{
        echo 'error1';
    };
    exit;
}

// RECOVER PASS
if ($_POST && array_key_exists('type',$_POST) && $_POST['type'] == 'recover' && array_key_exists('recover-email',$_POST) ) {

    if($login->check_email($_POST['recover-email']) === 'not_found'){
        echo json_encode(['status' => 'error',  'msg' => 'El usuario no esta registrado en la base de datos.'] );
        die;
    }
    $msg = "<!DOCTYPE html><html lang='es' ><head><meta http-equiv='Content-type' content='text/html; charset=utf-8' />
        <meta charset='utf-8'>	
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
        <!-- Meta -->
        <meta name='description' content='Gergal' />
        <meta name='keywords' content='Gergal' />
        <meta name='author' content='Gergal'>
        <meta name='robots' content='index, follow' />
        <meta name='revisit-after' content='3 days' />
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel='preconnect' href='https://fonts.gstatic.com'>
        <link href='https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap' rel='stylesheet'>
        
        <!-- Styles -->
            <!-- Bootstrap core CSS -->
            <link rel='stylesheet' href='http://newsite.gergalberries.com/css/bootstrap.css'>        
    
            <!-- Bootstrap RTL 
            <link rel='stylesheet' href='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css'> -->
    
            <!-- Font Awesome 4.1.0 -->
            <link href='http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' rel='stylesheet'>
    
            <!-- Avendor Icons Font -->
            <link href='http://newsite.gergalberries.com/css/avendor-font-styles.css' rel='stylesheet'>
    
            
            <!-- Theme CSS -->
            <link href='http://newsite.gergalberries.com/css/avendor.css' rel='stylesheet'>
            
            <!-- Color CSS -->
            <link href='http://newsite.gergalberries.com/css/colors/color-default.css' rel='stylesheet' title='style1'>
        
            </head>
            <body>    
            <div class='main-wrapper'>
                <!-- /Header Top Container -->
                <div class='container'>
                        <!-- Main Navigation & Logo -->                    
                        <div class='main-navigation'>
                            <div class='row'>
                                <!-- Main Navigation -->
                                <div class='col-md-12 '>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class='logo '>
                                        <a href='http://newsite.gergalberries.com'><img src='http://newsite.gergalberries.com/img/theme/logo.png' alt='Logo'></a>
                                    </div>
                                </div>
                                <!-- /Main Navigation -->
                            </div>              
                        </div>
                        <!-- /Main Navigation & Logo -->
                </div>
                <!-- /Container -->
                <!-- cuerpo del mensaje -->
                <div class='container text-left mt-4 p-4'>
                <h4>Hemos recibido tu solicitud para refrescar la Clave de usuario para el sitio web gergalberries.com </h4>
                <h5><a href='http://gergalberries.com/reset_password.php?email={$_POST['recover-email']}'>Clickear aqui</a> para ingresar una nueva clave de acceso para el usuario: {$_POST['recover-email']}  </h5>
                <hr/>
                <br><br>
                </div>
                <div class='footer-wrapper'>
                <!-- Footer Bottom Container -->
                <div class='footer-bottom'>
                    <div class='container'>
                        <div class='row'>
                            <!-- Logo -->
                            <div class='col-md-12 col-sm-12 text-center columns'>
                                <a href='#'><img src='http://newsite.gergalberries.com/img/demo/content/logo-footer.png' alt='Receta'></a>
                            </div>
                            <!-- /Logo -->
                            <!-- Copyright -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='copyright'>
                                    <p>Méjico 5245, (1603) Villa Martelli, Buenos Aires. Argentina<br>Tel.: +54 11 4838 1381 / +54 11 4709 5489. Fax: +54 11 4838 1381</br>Horario de atención lunes a viernes de 9 a 16hs </p>
                                </div>
                            </div>
                            <!-- /Copyright -->
                            <!-- Footer Menu -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='menu-footer'>
                                    <ul class='list-inline'>
                                        <li><a href='index.php'>Home</a></li>
                                        <li><a href='http://newsite.gergalberries.com/listado-productos.php?categoria=0'>Productos</a></li>
                                    
                                        <li><a href='http://newsite.gergalberries.com/instalaciones.php'>Instalaciones</a><li>                                                                        
                                        <li><a href='http://newsite.gergalberries.com/contacto.php'>Contacto</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Footer Menu -->
                            <!-- Copyright -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='copyright'>
                                    <p>© Gergal S.A. - Todos los derechos reservados</p>
                                </div>
                            </div>
                            <!-- /Copyright -->
                            <!-- Footer Social -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='social-footer'>
                                    <ul class='list-inline social-list'>
                                        <li><a href='https://www.facebook.com/Gergal-Berries-103417629786681/' class='ToolTip' title='Facebook'><span class='fa fa-facebook'></span></a></li>
                                        <li><a href='https://www.instagram.com/gergalberries/' class='ToolTip' title='Instagram'><span class='fa fa-instagram'></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Footer Social -->
                        </div>
                    </div>
                </div>
                <!-- /Footer Bottom Container -->
            </div>
        </body>
        </html>";
    

    
		require_once "clases/class.phpmailer.php";
		$mail = new PHPMailer();
		$mail->IsMail();
		$mail->IsHTML(true);    
		$mail->CharSet  = "utf-8";
		$mail->From     = 'pedidosgergal@gmail.com';
		$mail->FromName = 'pedidosgergal@gmail.com';
		$mail->WordWrap = 50;    
		$mail->Subject  =  "Generar nueva Clave de acceso";
		$mail->Body     =  $msg;
		$mail->AddAddress($_POST['recover-email']);
		$mail->AddReplyTo('pedidosgergal@gmail.com');
		$emailSent = $mail->Send();

    die;
}




