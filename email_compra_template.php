<!DOCTYPE html>
<html lang="es" >
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Meta -->
	<meta name="description" content="Gergal" />
	<meta name="keywords" content="Gergal" />
	<meta name="author" content="Gergal">
	<meta name="robots" content="index, follow" />
	<meta name="revisit-after" content="3 days" />
	
	
	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet">
	
	<!-- Styles -->
    	<!-- Bootstrap core CSS -->
  		<link rel="stylesheet" href="http://newsite.gergalberries.com/css/bootstrap.css">        

		<!-- Bootstrap RTL 
  		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css"> -->

		<!-- Font Awesome 4.1.0 -->
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

		<!-- Avendor Icons Font -->
		<link href="http://newsite.gergalberries.com/css/avendor-font-styles.css" rel="stylesheet">

		
		<!-- Theme CSS -->
  		<link href="http://newsite.gergalberries.com/css/avendor.css" rel="stylesheet">
        
		<!-- Color CSS -->
		<link href="http://newsite.gergalberries.com/css/colors/color-default.css" rel="stylesheet" title="style1">
    
</head>
<div class="main-wrapper">
	
<div class="header-top">
			<!-- Container -->
    <div class="container">
        <div class="row">
            <!-- Left -->
            <div class="col-md-6 col-sm-6 columns">
            	<div class="header-top-left">
                	<ul class="social-top">
                    	<li><a href="https://www.facebook.com/Gergal-Berries-103417629786681/" class="ToolTip" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                        <li><a href="https://www.instagram.com/gergalberries/" class="ToolTip" title="Instagram"><span class="fa fa-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /Left -->
            <!-- Right -->
                <div class="col-md-6 col-sm-6 columns">

                </div>
            <!-- /right -->
                
            </div>
        
        </div>
				<!-- /Container -->

		    </div>   
			<!-- /Header Top Container -->

			<!-- Header Main Container -->
			<div class="header-main">

        <!-- Container -->
        <div class="container">
            <!-- Main Navigation & Logo -->                    
			<div class="main-navigation">
            	<div class="row">
                    <!-- Main Navigation -->
                    <div class="col-md-12 ">
							<!-- Brand and toggle get grouped for better mobile display -->
							
								<div class="logo ">
									<a href="http://newsite.gergalberries.com"><img src="http://newsite.gergalberries.com/img/theme/logo.png" alt="Logo"></a>
								</div>
								<div class="text-center"><h4>¡Gracias por tu compra!</h4></div>

							

						</div>
					<!-- /Main Navigation -->
					</div>              
            </div>
         <!-- /Main Navigation & Logo -->
        </div>
		<!-- /Container -->
		<div class="container text-left mt-4 p-4">
		<h5>Hola! Estás en casa cuidándote y nosotros estamos para ayudarte y alcanzarte lo necesario; para eso por favor respondé este mail para que combinemos la entrega.<br/>Tenés debajo los mapas de reparto, buscá tu zona y decinos! CALCULÁ ENTRE 3 Y 7 DIAS HÁBILES PARA RECIBIR. ¡Llegaremos lo antes posible!</h5>
		<br/><hr/>
		<div class="row text-center">
		<div class="col-md-12 p-3"><img src="http://newsite.gergalberries.com/img/envios-caba.jpeg" width="100%" alt="Mapa de envios C.A.B.A"></div>
		<div class="col-md-12 p-3"><img src="http://newsite.gergalberries.com/img/envios-gba.jpeg" width="100%" alt="Mapa de envios GBA"></div>
		</div>
		<br/><hr/>
		<h5>Podés abonar tu pedido en efectivo al recibir o transferencia bancaria, por favor responder este mail con el comprobante de transferencia.<br/><br/>Datos de transferencia: ALIAS BRONCE.TUNICA.INDIGO Nº 0008161-2/064-2 CBU 0070064120000008161224 CUIT 30-63265369-3</h5>
		<br/><hr/>

		</div>
		
	</div>   
	<!-- /Header Main Container -->
</div>
	
	<div class="footer-wrapper">
	<!-- Footer Bottom Container -->
	<div class="footer-bottom">
    <!-- Container -->
        <div class="container">
        	<div class="row">
                <!-- Logo -->
				<div class="col-md-12 col-sm-12 text-center columns">
                    <a href="#"><img src="http://newsite.gergalberries.com/img/demo/content/logo-footer.png" alt="Receta"></a>
                </div>
                <!-- /Logo -->
                <!-- Copyright -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="copyright">
                 		<p>México 5245, (1603) Villa Martelli, Buenos Aires. Argentina<br>Tel.: +54 11 4838 1381 / +54 11 4709 5489. Fax: +54 11 4838 1381</br>Horario de atención lunes a viernes de 9 a 16hs </p>
                    </div>
                </div>
                <!-- /Copyright -->
                <!-- Footer Menu -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="menu-footer">
                    	<ul class="list-inline">
                        	<li><a href="index.php">Home</a></li>
                            <li><a href="http://newsite.gergalberries.com/listado-productos.php?categoria=0">Productos</a></li>
                           
                            <li><a href="http://newsite.gergalberries.com/instalaciones.php">Instalaciones</a><li>                                                                        
                            <li><a href="http://newsite.gergalberries.com/contacto.php">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /Footer Menu -->
            	<!-- Copyright -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="copyright">
                		<p>© Gergal S.A. - Todos los derechos reservados</p>
                    </div>
                </div>
                <!-- /Copyright -->
                <!-- Footer Social -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="social-footer">
                    	<ul class="list-inline social-list">
                        	<li><a href="https://www.facebook.com/Gergal-Berries-103417629786681/" class="ToolTip" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="https://www.instagram.com/gergalberries/" class="ToolTip" title="Instagram"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /Footer Social -->
			</div>
        </div>
	<!-- /Container -->
	</div>
<!-- /Footer Bottom Container -->
</div>
</div>	
</body>
</html>