<?php


//If the form is submitted
if(isset($_POST)) {
	$email= '';
	$subject = '';
	$message = '';
	

	$contactName = stripslashes(trim($_POST['nombre']));
	
	//If there is no error, send the email
	if(!isset($hasError)) {
		if(trim($_POST['nombre']) === '') {
			$msg=4;
			$nameError = 'Debe ingresar su nombre.';
			$hasError = true;
		} 
		else {
			$contactName = trim($_POST['nombre']);
		}
		
		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === '')  {
			$msg=2;
			$emailError = 'Debe ingresar su dirección de Email.';
			$hasError = true;

			
		} else if (!filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL)) {
			$msg=2;
			$emailError = 'La direccion de email no es valida.';
            $hasError = true;
		} else {
			$email = trim($_POST['email']);
		}

		//Check to make sure subject were entered 
		if(trim($_POST['asunto']) === '') {
			$msg=3;
			$subjectError = 'Debe ingresar un asunto del mensaje.';
			$hasError = true;
		} 
		else {
			if(function_exists('stripslashes')) {
		  		$subject = stripslashes(trim($_POST['asunto']));
		 	} 
		 	else {
		  		$subject = trim($_POST['asunto']);
			}
		}
		
		// if($attachment) {
    
		// 	$name_of_uploaded_file =basename($_FILES['uploaded_file']['name']); 
		// 	//get the file extension of the file 
		// 	$type_of_uploaded_file = 
		// 		substr($name_of_uploaded_file, 
		// 		strrpos($name_of_uploaded_file, '.') + 1); 
		// 	$size_of_uploaded_file = 
		// 		$_FILES["uploaded_file"]["size"]/1024;//size in KBs 
		// 		//Settings     
		// 	$max_allowed_file_size = 1000; // size in KB 
		// 	$allowed_extensions = array("jpg", "jpeg", "gif", "bmp", "png"); 
		// } 
		 
		 
		//Check to make sure comments were entered 
		if(trim($_POST['mensaje']) === '') {
			$messageError = 'Debe ingresar un texto de mensaje.';
			$hasError = true;
		} 
		else {
			if(function_exists('stripslashes')) {
		  		$message = stripslashes(trim($_POST['mensaje']));
		 	} 
		 	else {
		  		$message = trim($_POST['mensaje']);
			}
		}
	}
	if(!isset($hasError)) {
		include ('db/Config.php');
		$date = date('Y-m-d H:i:s');
		$q = "INSERT INTO contactos_form (email,remitente, asunto, mensaje, fecha_creado,fecha_modif) VALUES ('{$email}','{$contactName}','{$subject}','{$message}', '{$date}', '{$date}')";
        $insertMsg = $db->query($q);

		$status = "";
		
		require_once "clases/class.phpmailer.php";
		$mail = new PHPMailer();
		$mail->IsMail();
		$mail->IsHTML(true);    
		$mail->CharSet  = "utf-8";
		$mail->From     = $email;
		$mail->FromName = $contactName;
		$mail->WordWrap = 50;    
		$mail->Subject  =  $contactName;
		$mail->Body     =  "<strong>Nombre Completo:</strong> " . $contactName. ".<br/>";
		$mail->Body     .=  "<strong>Asunto:</strong> " . $subject. ".<br/>";
		$mail->Body     .= "<strong>Mensaje:</strong> " . $message. ".";
    	// $mail->AddAttachment($_FILES['uploaded_file']['tmp_name'], $_FILES['uploaded_file']['name']);      

		$mail->AddAddress('equipogergal@gmail.com');
		$mail->AddReplyTo($email);
		
		$emailSent = $mail->Send();
		if(! $emailSent) { 
			header("Location: error.php?msg={$msg}");	
		}
		else
		{
			header("Location: exito.php?msg=1");
		}
		die();
	}
	else 
	{
		header("Location: error.php?msg={$msg}");
		// $errmsg = (isset($nameError))?$nameError:'';
		// $errmsg .= (isset($emailError))?' '.$emailError:'';
		// $errmsg .= (isset($subjectError))?' '.$subjectError:'';
		// $errmsg .= (isset($messageError))?' '.$messageError:'';
		// die ("ERROR!!!".$errmsg);
	}	

	die();
} 
?>