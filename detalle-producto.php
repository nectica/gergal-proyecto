<?php 
	require_once 'clases/Receta.php';
	require_once 'clases/Producto.php';
	require_once 'clases/CarritoCompras.php';
	$cart = new CarritoCompras	;
	require_once 'main_head.php';
	require_once 'header.php'; 
	$p = new Producto();
	if(isset($_REQUEST['prod']) && !empty($_REQUEST['prod'])){
		//include 'db/Config.php';
		$productID = $_REQUEST['prod'];
		if (isset($_SESSION['tipo_usuario_id'])) {
			if ($_SESSION['tipo_usuario_id']==2) {
				$whereSelector = " WHERE estado_revendedores > 0";							
			} else {
				$whereSelector = " WHERE estado > 0";							
			}
		} else {
			$whereSelector = " WHERE estado > 0";			
		}		
		$sql = "SELECT productos.*,categorias.nombre as nombre_categoria FROM productos inner join categorias on categorias.id = productos.categoria  " . $whereSelector . " AND productos.id = ".$productID;
		$query = $p->db->query($sql);
		$row = $query->fetch_assoc();
		$itemData = array(
			'id' => $row['id'],
			'nombre' => $row['nombre'],
			'descripcion' => $row['descripcion'],
			'image_path'=> $row['image_path'],
			'categoria'=> $row['categoria'],
			'propiedades'=> $row['propiedades'],
			'info_nutricional'=> $row['info_nutricional'],
			'precio_revendedor' => $row['precio_revendedor'],
			'precio_lista'=> $row['precio_lista'],
			'cant_stock'=>$row['cantidad_en_stock']
		);
	}else{
		header("Location: index.php");
	}
?>
<?php 
        
		if(isset($_REQUEST['prod']) && !empty($_REQUEST['prod'])){
			$prodId = $_REQUEST['prod'];
//			$p = new Producto();
			$pCatId = $p->getCatId($prodId);			$pCatId = $row['categoria'];			
			$catNom = $p->getCatNom($pCatId); 			$catNom = $row['nombre_categoria']; 
		}else{
			$catNom = "Productos";
			$pCatId = 10;
		}
		?>
<!-- Main Wrapper Header -->
<div class="main-wrapper-header fancy-header dark-header parallax parallax-<?php echo $pCatId;?>"
    data-stellar-background-ratio="0.4">



    <div class="container">
        <div class="row">
            <div class="col-sm-12 columns">
                <div class="page-title">
                    <h1 class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        <?php echo $catNom ;?></h1>
                </div>

            </div>
        </div>

    </div>

</div>
<!-- /Main Wrapper Header -->

<!-- Main Container -->
<div class="main-wrapper">

    <!-- Container -->
    <div class="container">
        <div class="white-space space-big"></div>

        <div class="row">

            <div class="col-sm-6">

                <!-- Product Carousel Sync -->
                <div id="full-sync" class="owl-carousel">
                    <div class="item">
                        <!-- Overlay Img -->
                        <div class="overlay-wrapper">
                            <img alt="Product 1" src="/gergal-abm/<?php echo $itemData['image_path']?>">

                        </div>
                        <!-- Overlay Img -->
                    </div>



                </div>

                <!-- /Product Carousel Sync -->
                <div class="white-space space-big"></div>
            </div>

            <div class="col-sm-6">
                <div class="product-info">
                    <h3 class="fancy-title"><span><?php echo $itemData['nombre']?></span></h3>
                    <?php if( array_key_exists('tipo_usuario_id',$_SESSION) && $_SESSION['tipo_usuario_id'] == 2){
									echo "<p class='product-price' style='margin-bottom:0px; margin-top:40px; line-height:0px; text-decoration:line-through; color:#000000; font-weight:400; font-size:24px' >$ ".$itemData['precio_lista']."</p><p class='product-price'>$ ". $itemData['precio_revendedor']."</p>";
								} else{
									echo "<p class='product-price'>$ ".$itemData['precio_lista']."</p>";
								} 
								if($itemData['cant_stock'] > 1){
							   ?>
                    <div class="white-space space-xsmall"></div>
                    <div class="inputQty">
                        <span class="qdown qselect fa fa-angle-left"
                            onClick="updateCant('-','cant_<?php echo $itemData['id']?>')"></span>
                        <input id='cant_<?php echo $itemData['id']?>' type="text" maxlength="6" name="oa_quantity"
                            class="input-quantity" value="1" />
                        <span class="qup qselect fa fa-angle-right"
                            onClick="updateCant('+','cant_<?php echo $itemData['id']?>')"></span>
                    </div>
                    <div onclick="callCarroAccion('cant',<?php echo $itemData['id']?>)" class="btn btn-verde">AGREGAR
                    </div>

                    <?}else{?>
                    <div class="white-space space-xsmall"></div>
                    <div class="inputQty">
                        <input id='cant_<?php echo $itemData['id']?>' type="text" maxlength="6" name="oa_quantity"
                            class="input-quantity" value="0" />

                    </div>
                    <div class="btn btn-verde">SIN STOCK</div>
                    <?}?>


                    <div class="white-space space-small"></div>
                    <p><?php echo $itemData['info_nutricional']?></p>
                    <ul class="divider-list list-unstyled">
                        <li>
                            <strong>Categorías:</strong>
                            <a href="#" class="color-verde"><?php echo $catNom //$p->getCatNom($pCatId)?></a>

                        </li>
                    </ul>

                </div>


            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- Tabs -->
                <ul class="tabs-alt nav nav-tabs nav-justified" role="tablist">
                    <li class="active"><a href="#home5" role="tab" data-toggle="tab">Propiedades</a></li>
                    <!-- <li><a href="#profile5" role="tab" data-toggle="tab">Información adicional</a></li>
							<li><a href="#messages5" role="tab" data-toggle="tab">Envíos a domicilio</a></li> -->
                </ul>

                <div class="tabs-alt tab-content">
                    <div class="tab-pane active" id="home5">
                        <p><?php echo $itemData['propiedades']?></p>
                    </div>
                    <div class="tab-pane" id="profile5">
                        <h5>Titulo</h5>
                        <p>Texto</p>
                    </div>
                    <div class="tab-pane" id="messages5">


                        <h5>Titulo</h5>
                        <p>Texto</p>


                    </div>
                </div>
                <!-- /Tabs -->
                <div class="white-space space-medium"></div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <h4 class="fancy-title text-left"><span><strong>Productos relacionados</strong></span></h4>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- Carousel -->
                <div class="carousel-box">
                    <div class="carousel carousel-nav-out carousel-simple" data-carousel-autoplay="false"
                        data-carousel-items="4" data-carousel-nav="true" data-carousel-pagination="false"
                        data-carousel-speed="1000">
                        <?php echo $p->carrousel_items(6);?>
                    </div>
                </div>
                <!-- /Carousel -->
                <div class="white-space space-big"></div>
            </div>
        </div>

        <div class="white-space space-big"></div>
    </div>
    <!-- /Container -->

    <!-- Parallax -->
    <?php include("parallax_gergal.php"); ?>
    <!-- /Parallax -->

</div>
<!-- /Main Container -->

<!-- Footer Container -->
<?php include("footer.php"); ?>
<!-- /Footer Container -->

</div>

<!-- Back To Top -->
<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>
<!-- /Back To Top -->


<!-- login modal -->
<?php include 'loginView.php'?>

<!-- scripts del template -->
<?php include 'theme_scripts.php'?>
<!-- custom scripts -->
<?php include 'custom_scripts.php'?>
<script>
document.title = "Gergal - Detalle de Producto";
</script>
</body>

</html>