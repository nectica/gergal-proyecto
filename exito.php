<?php 

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header.php'; 

	$msg = 'Solicitud Procesada con Exito';

	$msg_follow ="Gracias por contactarte con nosotros"; 

	if(isset($_GET) && array_key_exists('msg',$_GET)){

		switch ($_GET['msg']) {

			case '1':

				$msg = 'Mensaje enviado con Exito.';

				$msg_follow ="Gracias por contactarte con nosotros."; 	

			break;

			default:

				$msg = 'Solicitud Procesada con Exito';

				$msg_follow ="Gracias por contactarte con nosotros"; 

				break;

		}

	}

?>

<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>



        <div class="row">

            <div class="col-md-10 col-md-offset-1">



                <div class="iconbox-wrapper circle bg-color-verde color-white iconbox-3x aligncenter">

                    <i class="icon gfx-check"></i>

                </div>



                <h1 class="text-center"><?php echo $msg ?></h1>

                <p class="lead text-center"><?php echo $msg_follow ?></p>

                <div class="white-space space-xsmall"></div>



                <div class="white-space space-xsmall"></div>

                <div class="text-center"><a href="index.php" class="btn btn-primary btn-lg">Continuar</a></div>

            </div>

        </div>



        <div class="white-space space-big"></div>

    </div>

    <!-- /Container -->







</div>

<!-- /Main Container -->



<!-- Container destacado -->

<!-- Fullsize -->

<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->

<!-- /Container -->



<!-- Footer destacado -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->



<!-- login modal -->

<?php include 'loginView.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts.php'?>

<script>
document.title = "Gergal - Exito";
</script>

</body>

</html>