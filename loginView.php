<div id="login-signup-modal" class="modal fade"  tabindex="-1" role="dialog" data-keyboard="false"  style="z-index:10050;">
  		<div class="modal-dialog" role="document">
    
    	<!-- LOGIN modal content -->
        <?if (!isset($_SESSION['loggedNombre'])) {?>
      <div class="modal-content" id="login-modal-content">
        <div class="modal-header" >
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="login-modal-title"></span>Ingresar</h4>
        </div>
        
        <div class="modal-body">
          <form method="post" name="login-form" id="login-form"  role="form" novalidate="">
            <input type="hidden" name="type" value="login">
            <div class="form-group" id="fg-login-email" >
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                <input name="login-email" id="login-email" type="email" class="form-control input-lg" placeholder="Ingresa tu email" required="">
                </div>                      
            </div>
            
            
            <div class="form-group">
                <div class="input-group" id="fg-login-password">
                <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                <input name="login-password" id="login-password" type="password" class="form-control input-lg" placeholder="Ingresa tu clave" >
                </div>                      
            </div>
              <button type="button" onClick="validateLogin()" class="btn btn-success btn-block btn-lg login-button">INGRESAR</button>
          </form>
        </div>
        
        <div class="modal-footer">
          <p>
          <a id="FPModal" href="#">Olvidaste tu clave?</a> | 
          <a id="signupModal" href="#">Registrate aqui!</a>
          </p>
        </div>
        
      </div>
        <?}?>
        <!-- login modal content -->
        <!-- signup modal content -->
      <div class="modal-content" id="<?if (isset($_SESSION['loggedNombre'])) {echo("login-modal-content");} else {echo("signup-modal-content");}?>">
        <div class="modal-header"id="signup-modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="login-modal-title"><?if (isset($_SESSION['loggedNombre'])) {echo("Modificá tus datos");} else {echo("Registrate ahora!");}?></h4>
          <h6>Campos Requeridos (*) </h6>
        </div>
        <div class="modal-body" id="signup-modal-body">
          <form name="signin-form" id="signin-form" role="form" novalidate="" >
          <input type="hidden" name="type" value="<?if (isset($_SESSION['loggedNombre'])) {echo("edit");} else {echo("signup");}?>">
          <!-- nombre -->
          <div class="form-group" id="fg-signup-nombre">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
              <input name="signup-nombre" id="signup-nombre" _onFocus="resetItem(this.id)" type="text" class="form-control input-lg" placeholder="Nombre (*)" <?if (isset($_SESSION['loggedNombre'])) echo("value=\"".$_SESSION['loggedNombre']."\"");?>>
              <div class="input-group-addon"><span class="glyphicon"></span></div>
              <input name="signup-apellido" id="signup-apellido" _onFocus="resetItem(this.id)" type="text" class="form-control input-lg" placeholder="Apellido (*)" <?if (isset($_SESSION['loggedApellido'])) echo("value=\"".$_SESSION['loggedApellido']."\"");?>>
            </div>                     
          </div>
          <!-- DNI -->
          <div class="form-group" id="fg-signup-dni">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
              <input name="signup-dni" id="signup-dni" type="text" maxlength="8" _onFocus="resetItem(this.id)" class="form-control input-lg" placeholder="Ingresa tu DNI (*)" <?if (isset($_SESSION['loggedDNI'])) echo("value=\"".$_SESSION['loggedDNI']."\"");?> >
            </div>                     
          </div>
          <!-- telefono -->
          <div class="form-group" id="fg-signup-telefono">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></div>
              <input name="signup-telefono" id="signup-telefono" type="text" _onFocus="resetItem(this.id)" class="form-control input-lg" placeholder="Ingresa tu número de teléfono (*)" <?if (isset($_SESSION['loggedTelefono'])) echo("value=\"".$_SESSION['loggedTelefono']."\"");?>>
            </div>                     
          </div>
          
          <!-- email -->
          <div class="form-group" id="fg-signup-email">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
              <input name="signup-email" id="signup-email" type="email" _onFocus="resetItem(this.id)" class="form-control input-lg" placeholder="Ingresa tu email (*)" <?if (isset($_SESSION['loggedEmail'])) echo("value=\"".$_SESSION['loggedEmail']."\"");?>>
            </div>                     
          </div>
          <!-- domicilio Calle -->
          <div class="form-group" id="fg-signup-direccion">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
              <input name="signup-calle" id="signup-calle"  type="text" class="form-control input-lg" placeholder="Calle (*)" <?if (isset($_SESSION['loggedCalle'])) echo("value=\"".$_SESSION['loggedCalle']."\"");?>>
              <div class="input-group-addon"><span class="glyphicon"></span></div>
              <input name="signup-numero" id="signup-numero"  type="text" maxlength="8" class="form-control input-lg" style="width:14rem;" placeholder="Numero (*)" <?if (isset($_SESSION['loggedNumero'])) echo("value=\"".$_SESSION['loggedNumero']."\"");?>>
            </div>                     
          </div>
          <input type="hidden" name="latitud" id="latitud" value=0 />
          <input type="hidden" name="longitud" id="longitud" value=0 />   
         <!-- piso depto /  numero  -->
         <div class="form-group" id="fg-signup-direccion-2">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
              <input name="signup-direccion-2" id="signup-direccion-2" type="text" class="form-control input-lg" placeholder="Piso y Dpto." <?if (isset($_SESSION['loggedDireccion2'])) echo("value=\"".$_SESSION['loggedDireccion2']."\"");?>>
            </div>                     
          </div>

         <div class="form-group" id="fg-signup-direccion-3">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
              <input name="signup-localidad" id="signup-localidad" type="text" class="form-control input-lg" placeholder="Localidad (*)" <?if (isset($_SESSION['loggedLocalidad'])) echo("value=\"".$_SESSION['loggedLocalidad']."\"");?>>
              <div class="input-group-addon"><span class="glyphicon"></span></div>
              <input name="signup-barrio" id="signup-barrio" type="text" class="form-control input-lg" placeholder="Barrio (*)" <?if (isset($_SESSION['loggedBarrio'])) echo("value=\"".$_SESSION['loggedBarrio']."\"");?>>
            </div>                     
          </div>
         
        <?//if (!isset($_SESSION['loggedNombre'])) {?>
          <!-- password -->
          <div class="form-group" id="fg-signup-password">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
              <input name="signup-password" id="signup-password" type="password" class="form-control input-lg" placeholder="<?if (isset($_SESSION['loggedNombre'])) {echo("Podes modificar tu clave opcionalmente");} else {echo("Ingresa una clave de acceso 5 caracteres o mas (*)");}?>" >
            </div>                      
          </div>
          <!-- confirmar Password -->
          <div class="form-group" id="fg-signup-confirmar-password">
            <div class="input-group">
              <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
              <input name="signup-confirmar-password" id="signup-confirmar-password" type="password" class="form-control input-lg" placeholder="Confirma tu clave (*)">
            </div>                      
          </div>   
		<?//}?>
          <button type="button" onClick="<?if (isset($_SESSION['loggedNombre'])) {echo("validateEdit()");} else {echo("validateSignup()");}?>" class="btn btn-success btn-block btn-lg login-button"><?if (isset($_SESSION['loggedNombre'])) {echo("ACTUALIZA TU CUENTA");} else {echo("CREAR CUENTA!");}?></button>
          </form>
        </div>
        <div class="modal-footer" id="signup-modal-footer" >
        <?if (!isset($_SESSION['loggedNombre'])) {?>
          <p>Ya tenes cuenta? <a id="loginModal" href="#">Ingresa aqui!</a></p>
		<?}?>
        </div>
        
      </div>
        <!-- signup modal content -->
        
        <!-- forgot password content -->
         <div class="modal-content" id="forgot-password-modal-content">
        
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="login-modal-title"> Recuperar Clave!</h4>
      </div>
        
        <div class="modal-body">
          <form method="post" id="Forgot-Password-Form" role="form" novalidate=""  >
          <input type="hidden" name="type" value="recover">
            <div class="form-group" id="fg-recover-email">
                <div class="input-group">
                <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                <input name="recover-email" id="recover-email" type="email" class="form-control input-lg" placeholder="Ingresa tu email">
                </div>                     
            </div>
                        
            <button type="button" onClick="validateRecover()" class="btn btn-success btn-block btn-lg login-button">
              <span class="glyphicon glyphicon-send"></span> ENVIAR
            </button>
          </form>
        </div>
        
        <div class="modal-footer">
          <p>Tenes tu clave? <a id="loginModal1" href="#">Ingresa aqui!</a></p>
        </div>
        
      </div>        
        <!-- forgot password content -->

		
    
    	<!-- /.modal-content -->
  		</div><!-- /.modal-dialog -->
		</div>