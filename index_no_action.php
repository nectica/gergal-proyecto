<?php 

	

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header_np.php'; 

	

	

?>

<div class="container">

    <div class="row"></div>
</div>

<!-- Slider Container -->

<div class="slider-wrapper">



</div>

<!-- /Slider Container -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>



        <div class="row">

            <div class="col-md-12 columns">

                <h1 class="fancy-title script-font text-center color-verde"><span>Hacia lo
                        <strong>Natural</strong></span></h1>

            </div>

            <div class="col-md-10 col-md-offset-1">

                <p class="lead text-center">Desde 1983 nos dedicamos a la producción y comercialización de berries
                    (frutos rojos) frescos y congelados en la Argentina y el mundo. Somos pioneros en la producción en
                    la provincia de Buenos Aires de frambuesas y moras. Siguiendo nuestro lema de “Hacia lo natural”,
                    fuimos incorporando otras frutas y verduras frescas y congeladas y jugos naturales elaborados con
                    nuestra propia fruta en nuestra propia planta de producción. Logrando así una amplia gama de
                    productos sanos y naturales para abastecer diferentes mercados y clientes.<br>

                    <!-- <strong>¡Sumate a nuestro equipo!</strong> </p> -->

                <div class="white-space space-small"></div>

            </div>

        </div>



        <!-- REVENDEDOR -->

        <div class="row">

            <div class="col-sm-12">

                <div class="text-center">

                    <!-- <a href="revendedores.php" class="btn btn-primary btn-lg">Quiero ser revendedor</a> -->

                </div>

            </div>



        </div>

        <div class="white-space space-medium"></div>

        <!-- /REVENDEDOR -->

    </div>

    <!-- /Container -->



    <!-- Fullsize -->

    <div class="fullsize bg-color-light">

        <div class="white-space space-medium"></div>

        <div class="container">

            <div class="row">



                <!-- Content -->

                <div class="col-md-12">



                    <div class="row">

                        <div id="destacados" class="col-md-12">

                            <!-- <h4 class="fancy-title text-left"><span><strong>Productos destacados</strong></span></h4> -->



                        </div>

                    </div>

                    <!-- 3 filas de 4 productos  -->

                    <div class="row">

                        <?php 

							// PARAMS DE LISTADO DE PRODUCTOS

							// $row=3,$cols=4,$offset=0,$limit=4,$categorias=[]

							// $prod = new Producto();

							// echo $prod->listado()['rows'];

						?>



                    </div>

                    <!-- Pagination -->

                    <!-- <div class="row">                        

                            <div class="col-sm-12">

                            	<div class="text-center">

			                		<a href="listado-productos.php" class="btn btn-verde btn-md">Ver todos los productos</a>

                                </div>

                            </div>                                                  

                        </div> -->

                    <!-- /Pagination -->



                    <div class="white-space space-medium"></div>

                </div>

                <!-- /Content -->



            </div>

        </div>

    </div>

    <!-- /Fullsize -->

    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>

        <!--                 

            	<div class="row">

  					<div class="col-md-12 columns">

                    	<h1 class="fancy-title script-font text-center color-verde"><span>¿Qué Estás Buscando?</span></h1>

                    </div>

                    <div class="col-md-10 col-md-offset-1">

                    	<p class="lead text-center">Actualmente distribuimos nuestros productos en toda la República Argentina y exportamos a Estados Unidos, Canadá, Uruguay, Chile, Europa, Asia y Oceanía.</p>

                        <div class="white-space space-medium"></div>

                    </div>

                    

                    

				</div>               

	           			 -->



    </div>

    <!-- /Container -->

    <!-- Parallax -->
    <?php include("parallax_gergal.php"); ?>
    <!-- /Parallax -->

    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>

        <div class="row">

            <div class="col-md-12">

                <!-- <h4 class="fancy-title text-left"><span><strong>Novedades</strong></span></h4> -->



            </div>

        </div>



        <!-- <div class="row">

                	<div class="col-md-12">

					

						<div class="carousel-box" >

							<div class="carousel carousel-nav-out carousel-simple" data-carousel-autoplay="false" data-carousel-items="4" data-carousel-nav="true" data-carousel-pagination="false"  data-carousel-speed="1000">

								<?php //echo (new Producto())->carrousel_items(6);?>

								

							</div>

                        </div>

					

                        <div class="white-space space-big"></div>

                    </div>

                </div> -->

    </div>

    <!-- /Container -->





    <!-- Container Recetas desactivado-->





    <!-- <div class="fullsize bg-color-light">

            <div class="row">

                    <div class="col-md-12">

                        <div class="white-space space-medium"></div>

                        	<h1 class="script-font text-center color-rojo">Recetas</h1>

                            

                        </div>

                    </div>

            	<div id="recetas" class="white-space space-medium"></div>

            

                <div  class="container bg-color-white">

                	<div class="white-space space-small"></div>

                	

					<div class="row">

                    	<div class="col-md-12">

							-->



    <!-- Carousel desactivado

						

						<div class="carousel-box" >

								<div class="carousel carousel-simple" data-carousel-autoplay="9000" data-carousel-items="3" data-carousel-nav="false" data-carousel-pagination="true"  data-carousel-speed="1000">

								<?php 

									//echo (new Receta())->get_carousel_boxes(0,6);

								?>

								

                                                                        

								</div>

                        	</div>

                        -->

    <!--	

					</div>

                    </div>

                    <div class="white-space space-big"></div>

                </div> 

                </div> 

            /cierra el Container de recetas (desactivado) -->













</div>

<!-- /Main Container -->



<!-- Footer Container -->

<?php include("footer_np.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->



<!-- login modal -->

<?php include 'loginView.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts.php'?>

<script>
document.title = "Gergal - Inicio";
</script>

</body>

</html>