<div class="footer-wrapper">
	<!-- Footer Bottom Container -->
	<div class="footer-bottom">
    <!-- Container -->
        <div class="container">
        	<div class="row">
                <!-- Logo -->
				<div class="col-md-12 col-sm-12 text-center columns">
                    <a href="#"><img src="img/demo/content/logo-footer.png" alt="Receta"></a>
                </div>
                <!-- /Logo -->
                <!-- Copyright -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="copyright">
                 		<p>Méjico 5245, (1603) Villa Martelli, Buenos Aires. Argentina<br>Tel.: +54 11 4838 1381 / +54 11 4709 5489. Fax: +54 11 4838 1381</br>Horario de atención lunes a viernes de 9 a 16 hs </p>
                    </div>
                </div>
                <!-- /Copyright -->
                <!-- Footer Menu -->
				<!-- <div class="col-md-12 col-sm-12 columns">
                	<div class="menu-footer">
                    	<ul class="list-inline">
                        	<li><a href="index.php">Home</a></li>
                            <li><a href="listado-productos.php?categoria=0">Productos</a></li>
                           
                            <li><a href="instalaciones.php">Instalaciones</a><li>                                                                        
                            <li><a href="contacto.php">Contacto</a></li>
                            <li><a href="legales.php">Terminos y condiciones </a></li>
                        </ul>
                    </div>
                </div> -->
                <!-- /Footer Menu -->
            	<!-- Copyright -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="copyright">
                		<p>© Gergal S.A. - Todos los derechos reservados</p>
                    </div>
                </div>
                <!-- /Copyright -->
                <!-- Footer Social -->
				<div class="col-md-12 col-sm-12 columns">
                	<div class="social-footer">
                    	<ul class="list-inline social-list">
                        	<li><a href="https://www.facebook.com/Gergal-Berries-103417629786681/" class="ToolTip" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="https://www.instagram.com/gergalberries/" class="ToolTip" title="Instagram"><span class="fa fa-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /Footer Social -->
			</div>
        </div>
	<!-- /Container -->
	</div>
<!-- /Footer Bottom Container -->
</div>