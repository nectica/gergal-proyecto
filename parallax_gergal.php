<!-- Parallax -->
<div class="parallax parallax-background9" data-stellar-background-ratio="0.4">

    <div class="white-space space-big"></div>
    <div class="white-space space-big"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="color-white text-center animation fadeInDown"><strong>¿Por qué Gergal?</strong></h3>

                <div class="white-space space-medium"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <h2 class="text-center color-white animation zoomIn"><img src="img/demo/content/ico-home3.png"></h2>
                <h3 class="text-center color-white script-font"><strong>100% natural</strong></h3>
                <div class="white-space space-big"></div>
            </div>
            <div class="col-md-3 col-sm-12">
                <h2 class="text-center color-white animation zoomIn"><img src="img/demo/content/ico-home2.png"></h2>

                <h3 class="text-center color-white script-font"><strong>limpio, cortado, listo para cocinar</strong>
                </h3>
                <div class="white-space space-big"></div>
            </div>
            <div class="col-md-3 col-sm-12">
                <h2 class="text-center color-white animation zoomIn"><img src="img/demo/content/ico-home1.png"></h2>

                <h3 class="text-center color-white script-font"><strong>Te lo llevamos a tu casa</strong></h3>
                <div class="white-space space-big"></div>
            </div>
            <div class="col-md-3 col-sm-12">
                <a href="contacto.php">
                    <h2 class="text-center color-white animation zoomIn"><img src="img/demo/content/ico-home4.png"></h2>

                    <h3 class="text-center color-white script-font"><strong>¿Sos del interior? Contactanos</strong></h3>
                </a>
                <div class="white-space space-big"></div>
            </div>

        </div>
    </div>
</div>
<!-- /Parallax -->