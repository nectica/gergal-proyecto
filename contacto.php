<?php 

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header.php'; 

?>

		<!-- Main Wrapper Header -->

        <div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras" data-stellar-background-ratio="0.4">

			
 			<div class="container">

            

				<div class="row">

                	<div class="col-sm-12 columns">

                		<div class="page-title">

                    		<h1 class="script-font" style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">Contacto</h1>                    

                    	</div>

                    	

					</div>

				</div>

                

			</div>

 

        </div>       

		<!-- /Main Wrapper Header -->



		<!-- Main Container -->

		<div class="main-wrapper">



        	<!-- Container -->

            <div class="container">

            	<div class="white-space space-big"></div>

                

            	<div class="row">

                    <div class="col-md-6">

                    	<h3 class="fancy-title"><span><a href="/revendedores.php">Quiero ser Revendedor</a></span></h3>

                        <div class="row">	

                        	<div class="col-sm-6">

                            	<ul class="list-default fa-ul">

                        			<li><span class="fa-li fa fa-phone color-default"></span>Tel.: +54 11 4838 1381<br>

 +54 11 4709 5489 <br>

Fax: +54 11 4838 1381</li>

                            		<li><span class="fa-li fa fa-clock-o color-default"></span><strong>Lunes a Viernes:</strong> 09:00 - 16:00</li>                            

                        		</ul>

                            </div>

                        	<div class="col-sm-6">

                            	<ul class="list-default fa-ul">

                        			<li><span class="fa-li fa fa-map-marker color-default"></span>Méjico 5245, (1603) Villa Martelli, Buenos Aires. Argentina </li>

                            		<li><span class="fa-li fa fa-envelope color-default"></span>equipogergal@gmail.com</li>                         

                        		</ul>

                            </div>

                        </div>

                        <div class="white-space space-small"></div>

                    </div>

                    <div class="col-md-6">

                    	<h3 class="fancy-title"><span>Envianos tu mensaje</span></h3>

						

                        <!-- Form -->

						<form method="post" role="form" novalidate=""  action="contacto-accion.php" class="form-horizontal" >

							<div class="form-group">

								<div class="col-sm-6">

									<input type="name" name="nombre" class="form-control" id="inputName" placeholder="Nombre">

								</div>

								<div class="col-sm-6">

									<input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email">

								</div>    

								<div class="col-sm-12">

									<input type="message" name="asunto" class="form-control" id="inputEmail3" placeholder="Asunto">

								</div> 

								<div class="col-sm-12">

									<textarea  name="mensaje" class="form-control" rows="5" placeholder="Mensaje"></textarea>

								</div> 

								<div class="col-sm-12">

									<button type="submit" class="btn btn-primary btn-block">Enviar mensaje</button>

								</div>

							</div>

						</form>                        

                        <!-- /Form --> 

                                               

                        <div class="white-space space-small"></div>

                    </div>

				</div>               

			

            	<div class="white-space space-medium"></div>	

            </div>

			<!-- /Container -->
               

		</div>

		<!-- /Main Container --> 

        

        <!-- Container destacado -->

            <!-- Fullsize -->

            <!-- Parallax -->                            
			 <?php include("parallax_gergal.php"); ?>
			<!-- /Parallax -->

            <!-- /Container -->      



		<!-- Footer destacado -->

		<?php include("footer.php"); ?>

		<!-- /Footer Container -->



	</div>	



	<!-- Back To Top -->

	<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>

	<!-- /Back To Top -->



 

	<!-- login modal -->

	<?php include 'loginView.php'?>



	<!-- scripts del template -->

	<?php include 'theme_scripts.php'?>

	<!-- custom scripts -->

	<?php include 'custom_scripts.php'?>

	<script>

		document.title = "Gergal - Contacto" ;

	</script>

