<?php 
	require_once 'clases/Receta.php';
	require_once 'clases/Producto.php';
	require_once 'clases/CarritoCompras.php';
	$cart = new CarritoCompras	;
	require_once 'main_head.php';
	require_once 'header.php'; 

	$msg = 'No se pudo procesar la solicitud';
	$msg_follow ="Intentá de nuevo o comunicate con nosotros."; 
	if(isset($_GET) && array_key_exists('msg',$_GET)){
		switch ($_GET['msg']) {
			case '1':
				$msg = 'Email o clave no validos';
				$msg_follow ="revisa los datos ingresados e intenta nuevamente."; 
				break;
			case '2':
				$msg = 'Email no valido';
				$msg_follow ="La direccion de email no es valida."; 
			break;
			case '3':
				$msg = 'Email sin Asunto';
				$msg_follow ="Debe ingresar un asunto del mensaje."; 		
			break;	
			case '4':
				$msg = 'Email sin Nombre de contacto';
				$msg_follow ="Debe ingresar su nombre."; 		
			break;	
			case '5':				$msg = 'El carrito de compras esta vacio';				$msg_follow ="Debes seleccionar productos."; 					break;				case '6':				$msg = 'Esa direccion de correo ya se encuentra registrada';				$msg_follow ="Debes utilizar otra direccion de email"; 					break;	
			default:
				$msg = 'No se pudo procesar la solicitud';
				$msg_follow ="Intentá de nuevo o comunicate con nosotros."; 
			break;

		}
	}
	
?>

<!-- Main Container -->
<div class="main-wrapper">

    <!-- Container -->
    <div class="container">
        <div class="white-space space-big"></div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="iconbox-wrapper circle bg-color-default color-white iconbox-3x aligncenter">
                    <i class="icon gfx-alert"></i>
                </div>

                <h1 class="text-center"><?php echo $msg ?></h1>
                <p class="lead text-center"><?php echo $msg_follow ?></p>
                <div class="white-space space-xsmall"></div>

                <div class="white-space space-xsmall"></div>
                <div class="text-center"><a href="index.php" class="btn btn-primary btn-lg">Continuar</a></div>
            </div>
        </div>

        <div class="white-space space-big"></div>
    </div>
    <!-- /Container -->



</div>
<!-- /Main Container -->

<!-- Container destacado -->
<!-- Fullsize -->

<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->

<!-- /Container -->

<!-- Footer destacado -->
<?php include("footer.php"); ?>
<!-- /Footer Container -->

</div>

<!-- Back To Top -->
<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>
<!-- /Back To Top -->

<!-- login modal -->
<?php include 'loginView.php'?>

<!-- scripts del template -->
<?php include 'theme_scripts.php'?>
<!-- custom scripts -->
<?php include 'custom_scripts.php'?>
<script>
document.title = "Gergal - Error";
</script>
</body>

</html>