<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

// initialize shopping cart class

include 'clases/CarritoCompras.php';
$cart = new CarritoCompras;
include 'db/Config.php';
if (empty($_REQUEST['action'])) {
    header("Location: error.php");
    die;
}

// AGREGAR PRODUCTO
if ($_REQUEST['action'] == 'addToCart') {
    if (empty($_REQUEST['cant']) || empty($_REQUEST['id'])) {
        echo "error no cant to add";
        die;
    }
    $productID = $_REQUEST['id'];
    $query = $db->query("SELECT * FROM productos WHERE id = " . $productID);

    $row = $query->fetch_assoc();
    if ($row['cantidad_en_stock'] < $_REQUEST['cant']) {
        echo json_encode(['status' => 'error', 'msg' => "No hay stock"], JSON_THROW_ON_ERROR);
        die;
    }
    $itemData = array(
        'id' => $row['id'],
        'name' => $row['nombre'],
        'image_path' => $row['image_path'],
        'price' => floatval($row['precio_lista']),
        'qty' => $_REQUEST['cant'],
        'precio_revendedor' => $row['precio_revendedor']
    );
    $insertItem = $cart->insert($itemData);
    echo json_encode(['total' => $cart->total(), 'items' => $cart->total_items(), 'producto' => $row['cantidad_en_stock']]);
    die;
    // $redirectLoc = $insertItem?'carritoView.php':'index.php';
    // header("Location: ".$redirectLoc);
}
// actualizar cantidades de productos
if ($_REQUEST['action'] == 'updateCartItem') {
    if ($_REQUEST['cant'] == 0) {
        $updateItem = $cart->remove($_REQUEST['id']);
        echo 'reload';
        die;
    } else {

        $itemData = array(
            'rowid' => $_REQUEST['id'],
            'qty' => $_REQUEST['cant']
        );

        $updateItem = $cart->update($itemData);
    }
    echo $updateItem ? 'ok' : 'err';
    die;
}
// borrar producto
if ($_REQUEST['action'] == 'removeCartItem') {
    $deleteItem = $cart->remove($_REQUEST['id']);
    header("Location: carritoView.php");
    die;
}

// checkout armar pedido
// 
if ($_REQUEST['action'] == 'placeOrder') {
    // NO CART ITEMS OR NOT LOGGED OR NO FORMA DE PAGO EXIT!!

    if ($cart->total_items() == 0 || empty($_SESSION['loggedInId']) || empty($_REQUEST['forma_pago'])) {
        header("Location: error.php");
        die;
    }



    $error = false;
    $sin_stock = [];
    //itero primero, para chequear todo stock
    foreach ($cart->contents() as $item) {
        if (is_array($item)) {
            $query = $db->query("SELECT cantidad_en_stock FROM productos WHERE id = " . $item['id']);
            $row = $query->fetch_assoc();
            if ($row['cantidad_en_stock'] < $item['qty']) {
                $error = true;
                $sin_stock[$item['id']] = $row['cantidad_en_stock'];
            }
        }
    }
    if ($error) {
        $_SESSION['sin_stock'] = $sin_stock;
        header("Location: carritoView.php");
        die;
    }
    /* fin de chequeo de de stock, si continua es por que hay stock 
 */
    // DATOS DEL CLIENTE
    $clidata = $db->query("SELECT * from usuarios where id = {$_SESSION['loggedInId']} ")->fetch_assoc();

    //  INSERT DEL PEDIDO  
    $date = date('Y-m-d H:i:s');
    $precio_total = ($_SESSION['tipo_usuario_id'] == 2) ? $cart->total_revendedor() : $cart->total();
    $q = "INSERT INTO pedidos (cli_id, rev_id, precio_total,forma_pago, fecha_creado, fecha_modif) VALUES ({$_SESSION['loggedInId']},{$_REQUEST['revId']}, {$precio_total},'{$_REQUEST['forma_pago']}', '{$date}', '{$date}')";
    $insertOrder = $db->query($q);
    if (!$insertOrder) {
        // EXIT CON ERROR
        header("Location: error.php");
        die;
    }
    $orderID = $db->insert_id;
    $sql = '';

    // codigo viejo
    foreach ($cart->contents() as $item) {
        if (is_array($item)) {
            $precio = ($_SESSION['tipo_usuario_id'] == 2) ? $item['precio_revendedor'] : $item['price'];
            $sql .= "INSERT INTO pedidos_items (pedidos_id, producto_id,producto_nombre,producto_precio_unitario, cant) VALUES ({$orderID}, {$item['id']},'{$item['name']}',{$precio}, {$item['qty']});";
        }
        // DEDUCE ITEM DEL STOCK  
        $deduceItemFromStock = $db->query("UPDATE `gergal`.`productos` SET `cantidad_en_stock` = `cantidad_en_stock` -  {$item['qty']}  WHERE `productos`.`id` = {$item['id']} ");
    }
    // insert order items into database
    $insertOrderItems = $db->multi_query($sql);


    $_SESSION['detalleDelPedido'] = ['orderId' => $orderID, 'cartItems' => $cart->contents(), 'monto_total' => $precio_total];
    // ENVIAR EMAIL DE CONFIRMACION DE PEDIDO
    // SI ES REVENDEDOR VA A EQUIPOGERGAL@GMAIL.COM
    // SI ES PARTICULAR VA A PEDIDOSGERGAL@GMAIL.COM
    $backEndEmail = ($_SESSION['tipo_usuario_id'] == 2) ? 'equipogergal@gmail.com' : 'pedidosgergal@gmail.com';
    // $backEndEmail = ($_SESSION['tipo_usuario_id'] == 2)?'jarvi02@gmail.com':'jpuebla.ar@gmail.com';
    $tipomsg = ($_SESSION['tipo_usuario_id'] == 2) ? 'message_revendedor' : 'message_particular';
    $tipousuario = ($_SESSION['tipo_usuario_id'] == 2) ? 'Revendedor' : 'Particular';
    $message_particular =
        "<!DOCTYPE html>
        <html lang='es' >
        <head>
        <meta http-equiv='Content-type' content='text/html; charset=utf-8' />
            <meta charset='utf-8'>	
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
        
            <!-- Meta -->
            <meta name='description' content='Gergal' />
            <meta name='keywords' content='Gergal' />
            <meta name='author' content='Gergal'>
            <meta name='robots' content='index, follow' />
            <meta name='revisit-after' content='3 days' />
            
            
            <!-- Google Fonts -->
            <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
            <link rel='preconnect' href='https://fonts.gstatic.com'>
            <link href='https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap' rel='stylesheet'>
            
            <!-- Styles -->
                <!-- Bootstrap core CSS -->
                <link rel='stylesheet' href='http://gergalberries.com/css/bootstrap.css'>        
        
                <!-- Bootstrap RTL 
                <link rel='stylesheet' href='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css'> -->
        
                <!-- Font Awesome 4.1.0 -->
                <link href='http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' rel='stylesheet'>
        
                <!-- Avendor Icons Font -->
                <link href='http://gergalberries.com/css/avendor-font-styles.css' rel='stylesheet'>
        
                
                <!-- Theme CSS -->
                <link href='http://gergalberries.com/css/avendor.css' rel='stylesheet'>
                
                <!-- Color CSS -->
                <link href='http://gergalberries.com/css/colors/color-default.css' rel='stylesheet' title='style1'>
            
        </head>
        <body>    
            <div class='main-wrapper'>
                <div class='header-top'>
                    <div class='container'>
                        <div class='row'>
                            <!-- Left -->
                            <div class='col-md-6 col-sm-6 columns'>
                                <div class='header-top-left'>
                                    <ul class='social-top'>
                                        <li><a href='https://www.facebook.com/Gergal-Berries-103417629786681/' class='ToolTip' title='Facebook'><span class='fa fa-facebook'></span></a></li>
                                        <li><a href='https://www.instagram.com/gergalberries/' class='ToolTip' title='Instagram'><span class='fa fa-instagram'></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Left -->
                        </div>
                    </div>
                    <!-- /Container -->
            
                </div>   
                <!-- /Header Top Container -->
                <div class='container'>
                        <!-- Main Navigation & Logo -->                    
                        <div class='main-navigation'>
                            <div class='row'>
                                <!-- Main Navigation -->
                                <div class='col-md-12 '>
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                            <div class='logo '>
                                                <a href='http://gergalberries.com'><img src='http://gergalberries.com/img/theme/logo.png' alt='Logo'></a>
                                            </div>
                                            <div class='text-center'><h4>¡Gracias por tu compra!</h4></div>
                                </div>
                                <!-- /Main Navigation -->
                            </div>              
                        </div>
                        <!-- /Main Navigation & Logo -->
                </div>
                <!-- /Container -->
                <!-- cuerpo del mensaje -->
                <div class='container text-left mt-4 p-4'>
                        <h5>Hola! {$clidata['nombre']} Estás en casa cuidándote y nosotros estamos para ayudarte y alcanzarte lo necesario; para eso por favor respondé este mail para que combinemos la entrega.<br/>Tenés debajo los mapas de reparto, buscá tu zona y decinos! CALCULÁ ENTRE 3 Y 7 DIAS HÁBILES PARA RECIBIR. ¡Llegaremos lo antes posible!</h5>
                        <br/><hr/>
                        <div class='col-md-12 p-3'><img src='http://gergalberries.com/img/envios-caba.jpeg' width='100%' alt='Mapa de envios C.A.B.A'>
                        </div>
                        <div class='col-md-12 p-3'><img src='http://gergalberries.com/img/envios-gba.jpeg' width='100%' alt='Mapa de envios GBA'>
                        </div>
                        <br/><hr/>
                        <p class='text-left'><strong>Numero de pedido: {$orderID}</strong></p>
                        <p class='text-left'><strong>Cantidad total de productos: {$cart->total_items()}</strong></p ";
    // se comentan  estas lineas para en su lugar , agregar tabla de items con precios.
    // $message_particular .= "<ul class='list-default divider-list biglist list-unstyled'>";
    /* foreach ($cart->contents() as $itm) {
        if (isset($itm['name']) && isset($itm['qty'])) {
            $message_particular .= "<li><strong>" . $itm['name'] . "</strong> || Cantidad: <strong>" . $itm['qty'] . "</strong></li>";
        }
    }*/
    //$message_particular .= "</ul><hr><p><strong>Total a Pagar: " . number_format(floatval($cart->total()), 2) . "</strong></p>";



    $message_revendedor =
        "<!DOCTYPE html>
        <html lang='es' >
        <head>
        <meta http-equiv='Content-type' content='text/html; charset=utf-8' />
            <meta charset='utf-8'>	
            <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
            <!-- Meta -->
            <meta name='description' content='Gergal' />
            <meta name='keywords' content='Gergal' />
            <meta name='author' content='Gergal'>
            <meta name='robots' content='index, follow' />
            <meta name='revisit-after' content='3 days' />
            <!-- Google Fonts -->
            <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
            <link rel='preconnect' href='https://fonts.gstatic.com'>
            <link href='https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap' rel='stylesheet'>
            
            <!-- Styles -->
                <!-- Bootstrap core CSS -->
                <link rel='stylesheet' href='http://gergalberries.com/css/bootstrap.css'>        
        
                <!-- Bootstrap RTL 
                <link rel='stylesheet' href='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css'> -->
        
                <!-- Font Awesome 4.1.0 -->
                <link href='http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' rel='stylesheet'>
        
                <!-- Avendor Icons Font -->
                <link href='http://gergalberries.com/css/avendor-font-styles.css' rel='stylesheet'>
        
                
                <!-- Theme CSS -->
                <link href='http://gergalberries.com/css/avendor.css' rel='stylesheet'>
                
                <!-- Color CSS -->
                <link href='http://gergalberries.com/css/colors/color-default.css' rel='stylesheet' title='style1'>
            
        </head>
        <body>    
            <div class='main-wrapper'>
                <div class='header-top'>
                    <div class='container'>
                        <div class='row'>
                            <!-- Left -->
                            <div class='col-md-6 col-sm-6 columns'>
                                <div class='header-top-left'>
                                    <ul class='social-top'>
                                        <li><a href='https://www.facebook.com/Gergal-Berries-103417629786681/' class='ToolTip' title='Facebook'><span class='fa fa-facebook'></span></a></li>
                                        <li><a href='https://www.instagram.com/gergalberries/' class='ToolTip' title='Instagram'><span class='fa fa-instagram'></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Left -->
                        </div>
                    </div>
                    <!-- /Container -->
            
                </div>   
                <!-- /Header Top Container -->
                <div class='container'>
                        <!-- Main Navigation & Logo -->                    
                        <div class='main-navigation'>
                            <div class='row'>
                                <!-- Main Navigation -->
                                <div class='col-md-12 '>
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                            <div class='logo '>
                                                <a href='http://gergalberries.com'><img src='http://gergalberries.com/img/theme/logo.png' alt='Logo'></a>
                                            </div>
                                            <div class='text-center'><h4>¡Gracias por tu compra!</h4></div>
                                </div>
                                <!-- /Main Navigation -->
                            </div>              
                        </div>
                        <!-- /Main Navigation & Logo -->
                </div>
                <!-- /Container -->
                <!-- cuerpo del mensaje -->
                <div class='container text-left mt-4 p-4'>
                        <h5>Hola! {$clidata['nombre']} Por favor respondé este mail para que combinemos la entrega.<br/>Tenés debajo los mapas de reparto, buscá tu zona y decinos! CALCULÁ ENTRE 3 Y 7 DIAS HÁBILES PARA RECIBIR. ¡Llegaremos lo antes posible!</h5>
                        <br/><hr/>
                        <div class='col-md-12 p-3'><img src='http://gergalberries.com/img/envios-caba.jpeg' width='100%' alt='Mapa de envios C.A.B.A'>
                        </div>
                        <div class='col-md-12 p-3'><img src='http://gergalberries.com/img/envios-gba.jpeg' width='100%' alt='Mapa de envios GBA'>
                        </div>
                        <br/><hr/>
                        <p class='text-left'><strong>Numero de pedido: {$orderID}</strong></p>
                        <p class='text-left'><strong>Cantidad total de productos: {$cart->total_items()}</strong></p ";
    //$message_revendedor .= "<ul class='list-default divider-list biglist list-unstyled'>";
    /*  foreach ($cart->contents() as $itm) {
        if (isset($itm['name']) && isset($itm['qty'])) {
            $message_revendedor .= "<li> <strong>" . $itm['name'] . "</strong>&nbsp;|| &nbsp; Cantidad: <strong>" . $itm['qty'] . "</strong></li>";
        }
    }*/
    // $message_revendedor .= "</ul><hr><p><strong>Total a Pagar: " . number_format(floatval($cart->total_revendedor()), 2) . "</strong></p>";
    // $message_revendedor .= "<br/><hr/></div>";

    $msg_backend = "<!DOCTYPE html><html lang='es' ><head><meta http-equiv='Content-type' content='text/html; charset=utf-8' />
        <meta charset='utf-8'>	
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
        <!-- Meta -->
        <meta name='description' content='Gergal' />
        <meta name='keywords' content='Gergal' />
        <meta name='author' content='Gergal'>
        <meta name='robots' content='index, follow' />
        <meta name='revisit-after' content='3 days' />
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel='preconnect' href='https://fonts.gstatic.com'>
        <link href='https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap' rel='stylesheet'>
        
        <!-- Styles -->
            <!-- Bootstrap core CSS -->
            <link rel='stylesheet' href='http://gergalberries.com/css/bootstrap.css'>        
    
            <!-- Bootstrap RTL 
            <link rel='stylesheet' href='http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css'> -->
    
            <!-- Font Awesome 4.1.0 -->
            <link href='http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' rel='stylesheet'>
    
            <!-- Avendor Icons Font -->
            <link href='http://gergalberries.com/css/avendor-font-styles.css' rel='stylesheet'>
    
            
            <!-- Theme CSS -->
            <link href='http://gergalberries.com/css/avendor.css' rel='stylesheet'>
            
            <!-- Color CSS -->
            <link href='http://gergalberries.com/css/colors/color-default.css' rel='stylesheet' title='style1'>
        
            </head>
            <body>    
            <div class='main-wrapper'>
                <!-- /Header Top Container -->
                <div class='container'>
                        <!-- Main Navigation & Logo -->                    
                        <div class='main-navigation'>
                            <div class='row'>
                                <!-- Main Navigation -->
                                <div class='col-md-12 '>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class='logo '>
                                        <a href='http://gergalberries.com'><img src='http://gergalberries.com/img/theme/logo.png' alt='Logo'></a>
                                    </div>
                                </div>
                                <!-- /Main Navigation -->
                            </div>              
                        </div>
                        <!-- /Main Navigation & Logo -->
                </div>
                <!-- /Container -->
                <!-- cuerpo del mensaje -->
                <div class='container text-left mt-4 p-4'>
                        <h5>Recibiste una compra de {$clidata['nombre']}  {$clidata['apellido']}</h5>
                        <hr/>
                        <h4><strong>Información de contacto</h4>
                        <p class='text-left'><strong>Tipo de usuario: </strong> {$tipousuario}</p>
                          <p class='text-left'><strong>Email: </strong> {$clidata['email']}</p>
                        <p class='text-left'><strong>Nombre completo: </strong>{$clidata['nombre']}  {$clidata['apellido']}</p>
                        <p class='text-left'><strong>Telefono: </strong> {$clidata['telefono']}</p>
                        <p class='text-left'><strong>Direccion: </strong> {$clidata['nombre_calle']} {$clidata['calle_nro']}</p>
                        <p class='text-left'><strong>Piso / Dpto: </strong> {$clidata['direccion2']} </p>
                        <p class='text-left'><strong>Barrio: </strong> {$clidata['barrio']} </p>
                        <p class='text-left'><strong>Localidad: </strong> {$clidata['localidad']}</p>
                        <hr/>
                        <h4>Información del pedido</h4>
                        <p class='text-left'><strong>Numero de pedido: </strong><a href='http://gergalberries.com/gergal-abm/public/pedidos/{$orderID}'>#{$orderID}</a></p>
                        <p class='text-left'><strong>Cantidad total de productos:</strong> {$cart->total_items()}</p>";

    $msg_backend .= "<div class='container'><table class='table table-bordered'><thead><tr><th>Producto</th><th>Cantidad</th><th>Precio</th><th>Sub total</th></tr></thead><tbody>";
    $message_particular .= "<div class='container'><table class='table table-bordered'><thead><tr><th>Producto</th><th>Cantidad</th><th>Precio</th><th>Sub total</th></tr></thead><tbody>";
    $message_revendedor .= "<div class='container'><table class='table table-bordered'><thead><tr><th>Producto</th><th>Cantidad</th><th>Precio</th><th>Sub total</th></tr></thead><tbody>";

    if ($tipousuario == 'Revendedor') {
        foreach ($cart->contents() as $itm) {
            if (isset($itm['name']) && isset($itm['qty'])) {
                $sbttl = intval($itm['qty']) * intval($itm['precio_revendedor']);
                $fsbt = number_format(floatval($sbttl), 2);
                $fprc = number_format(floatval($itm['precio_revendedor']), 2);
                $msg_backend .= "<tr><td>{$itm['name']}</td><td>{$itm['qty']}</td><td>{$fprc}</td><td>{$sbttl}</td></tr>";
                $message_revendedor .= "<tr><td>{$itm['name']}</td><td>{$itm['qty']}</td><td>{$fprc}</td><td>{$sbttl}</td></tr>";
                // $msg_backend .= "<li><strong>".."</strong> || Cantidad: <strong>".$itm['qty']."</strong></li>";
            }
        }
        $msg_backend .= "<tr><td colspan=3 class='text-right'><h4>Monto total:</h4></td><td class='text-right'><h4>" . number_format(floatval($cart->total_revendedor()), 2) . "</h4></td></tr>";
    } else {
        foreach ($cart->contents() as $itm) {
            if (isset($itm['name']) && isset($itm['qty'])) {
                $sbttl = intval($itm['qty']) * intval($itm['price']);
                $fsbt = number_format(floatval($sbttl), 2);
                $fprc = number_format(floatval($itm['price']), 2);
                $msg_backend .= "<tr><td>{$itm['name']}</td><td>{$itm['qty']}</td><td>{$fprc}</td><td>{$fsbt}</td></tr>";
                $message_particular .= "<tr><td>{$itm['name']}</td><td>{$itm['qty']}</td><td>{$fprc}</td><td>{$fsbt}</td></tr>";
                // $msg_backend .= "<li><strong>".."</strong> || Cantidad: <strong>".$itm['qty']."</strong></li>";
            }
        }
        $msg_backend .= "<tr><td colspan=3 class='text-right'><h4>Monto total</h4></td><td class='text-right'><h4>" . number_format(floatval($cart->total()), 2) . "</h4></td></tr>";
    }
    $msg_backend .= "</tbody></table></div>";
    $msg_backend .= "<hr/><p class='text-left'><strong>Metodo de pago:</strong>{$_REQUEST['forma_pago']}</p>";

    $msg_backend .= "<br/><hr/></div>
                <div class='footer-wrapper'>
                <!-- Footer Bottom Container -->
                <div class='footer-bottom'>
                    <div class='container'>
                        <div class='row'>
                            <!-- Logo -->
                            <div class='col-md-12 col-sm-12 text-center columns'>
                                <a href='#'><img src='http://gergalberries.com/img/demo/content/logo-footer.png' alt='Receta'></a>
                            </div>
                            <!-- /Logo -->
                            <!-- Copyright -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='copyright'>
                                    <p>Méjico 5245, (1603) Villa Martelli, Buenos Aires. Argentina<br>Tel.: +54 11 4838 1381 / +54 11 4709 5489. Fax: +54 11 4838 1381</br>Horario de atención lunes a viernes de 9 a 16hs </p>
                                </div>
                            </div>
                            <!-- /Copyright -->
                            <!-- Footer Menu -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='menu-footer'>
                                    <ul class='list-inline'>
                                        <li><a href='index.php'>Home</a></li>
                                        <li><a href='http://gergalberries.com/listado-productos.php?categoria=0'>Productos</a></li>
                                    
                                        <li><a href='http://gergalberries.com/instalaciones.php'>Instalaciones</a><li>                                                                        
                                        <li><a href='http://gergalberries.com/contacto.php'>Contacto</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Footer Menu -->
                            <!-- Copyright -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='copyright'>
                                    <p>© Gergal S.A. - Todos los derechos reservados</p>
                                </div>
                            </div>
                            <!-- /Copyright -->
                            <!-- Footer Social -->
                            <div class='col-md-12 col-sm-12 columns'>
                                <div class='social-footer'>
                                    <ul class='list-inline social-list'>
                                        <li><a href='https://www.facebook.com/Gergal-Berries-103417629786681/' class='ToolTip' title='Facebook'><span class='fa fa-facebook'></span></a></li>
                                        <li><a href='https://www.instagram.com/gergalberries/' class='ToolTip' title='Instagram'><span class='fa fa-instagram'></span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Footer Social -->
                        </div>
                    </div>
                </div>
                <!-- /Footer Bottom Container -->
            </div>
        </body>
        </html>";
    $message_particular .= "<hr><p><strong>Total a Pagar: " . number_format(floatval($cart->total()), 2) . "</strong></p>";
    $message_particular .= "<h5>Podés abonar tu pedido en efectivo al recibir o transferencia bancaria, por favor responder este mail con el comprobante de transferencia.<br/><br/>Datos de transferencia: ALIAS BRONCE.TUNICA.INDIGO Nº 0008161-2/064-2 CBU 0070064120000008161224 CUIT 30-63265369-3</h5>
        <br/><hr/>
        </div>";
    //footer
    $message_particular .= "
                    <div class='footer-wrapper'>
                    <!-- Footer Bottom Container -->
                    <div class='footer-bottom'>
                        <div class='container'>
                            <div class='row'>
                                <!-- Logo -->
                                <div class='col-md-12 col-sm-12 text-center columns'>
                                    <a href='#'><img src='http://gergalberries.com/img/demo/content/logo-footer.png' alt='Receta'></a>
                                </div>
                                <!-- /Logo -->
                                <!-- Copyright -->
                                <div class='col-md-12 col-sm-12 columns'>
                                    <div class='copyright'>
                                        <p>Méjico 5245, (1603) Villa Martelli, Buenos Aires. Argentina<br>Tel.: +54 11 4838 1381 / +54 11 4709 5489. Fax: +54 11 4838 1381</br>Horario de atención lunes a viernes de 9 a 16 hs </p>
                                    </div>
                                </div>
                                <!-- /Copyright -->
                                <!-- Footer Menu -->
                                <div class='col-md-12 col-sm-12 columns'>
                                    <div class='menu-footer'>
                                        <ul class='list-inline'>
                                            <li><a href='index.php'>Home</a></li>
                                            <li><a href='http://gergalberries.com/listado-productos.php?categoria=0'>Productos</a></li>
                                        
                                            <li><a href='http://gergalberries.com/instalaciones.php'>Instalaciones</a><li>                                                                        
                                            <li><a href='http://gergalberries.com/contacto.php'>Contacto</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Footer Menu -->
                                <!-- Copyright -->
                                <div class='col-md-12 col-sm-12 columns'>
                                    <div class='copyright'>
                                        <p>© Gergal S.A. - Todos los derechos reservados</p>
                                    </div>
                                </div>
                                <!-- /Copyright -->
                                <!-- Footer Social -->
                                <div class='col-md-12 col-sm-12 columns'>
                                    <div class='social-footer'>
                                        <ul class='list-inline social-list'>
                                            <li><a href='https://www.facebook.com/Gergal-Berries-103417629786681/' class='ToolTip' title='Facebook'><span class='fa fa-facebook'></span></a></li>
                                            <li><a href='https://www.instagram.com/gergalberries/' class='ToolTip' title='Instagram'><span class='fa fa-instagram'></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /Footer Social -->
                            </div>
                        </div>
                    </div>
                    <!-- /Footer Bottom Container -->
                </div>
            </body>
            </html>";
    $message_revendedor .= "<hr><p><strong>Total a Pagar: " . number_format(floatval($cart->total_revendedor()), 2) . "</strong></p>";
    // footer de revendedor
    $message_revendedor .= "<div class='footer-wrapper'>
                        <!-- Footer Bottom Container -->
                        <div class='footer-bottom'>
                            <div class='container'>
                                <div class='row'>
                                    <!-- Logo -->
                                    <div class='col-md-12 col-sm-12 text-center columns'>
                                        <a href='#'><img src='http://gergalberries.com/img/demo/content/logo-footer.png' alt='Receta'></a>
                                    </div>
                                    <!-- /Logo -->
                                    <!-- Copyright -->
                                    <div class='col-md-12 col-sm-12 columns'>
                                        <div class='copyright'>
                                            <p>Méjico 5245, (1603) Villa Martelli, Buenos Aires. Argentina<br>Tel.: +54 11 4838 1381 / +54 11 4709 5489. Fax: +54 11 4838 1381</br>Horario de atención lunes a viernes de 9 a 16hs </p>
                                        </div>
                                    </div>
                                    <!-- /Copyright -->
                                    <!-- Footer Menu -->
                                    <div class='col-md-12 col-sm-12 columns'>
                                        <div class='menu-footer'>
                                            <ul class='list-inline'>
                                                <li><a href='index.php'>Home</a></li>
                                                <li><a href='http://gergalberries.com/listado-productos.php?categoria=0'>Productos</a></li>
                                            
                                                <li><a href='http://gergalberries.com/instalaciones.php'>Instalaciones</a><li>                                                                        
                                                <li><a href='http://gergalberries.com/contacto.php'>Contacto</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /Footer Menu -->
                                    <!-- Copyright -->
                                    <div class='col-md-12 col-sm-12 columns'>
                                        <div class='copyright'>
                                            <p>© Gergal S.A. - Todos los derechos reservados</p>
                                        </div>
                                    </div>
                                    <!-- /Copyright -->
                                    <!-- Footer Social -->
                                    <div class='col-md-12 col-sm-12 columns'>
                                        <div class='social-footer'>
                                            <ul class='list-inline social-list'>
                                                <li><a href='https://www.facebook.com/Gergal-Berries-103417629786681/' class='ToolTip' title='Facebook'><span class='fa fa-facebook'></span></a></li>
                                                <li><a href='https://www.instagram.com/gergalberries/' class='ToolTip' title='Instagram'><span class='fa fa-instagram'></span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /Footer Social -->
                                </div>
                            </div>
                        </div>
                        <!-- /Footer Bottom Container -->
                    </div>
                </body>
            </html>";
    // EVIO DE EMAILS

    file_put_contents('rafa.htm', $msg_backend, FILE_APPEND);
    file_put_contents('msg_cliente.html', $$tipomsg, FILE_APPEND);

    require_once "clases/class.phpmailer.php";
    try {
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->IsHTML(true);
        $mail->CharSet  = "utf-8";
        $mail->From     = $backEndEmail;
        $mail->FromName = 'Gergal S.A.';
        $mail->WordWrap = 50;
        $mail->Subject  =  "Gergal S.A Pedido Nro." . $_SESSION['detalleDelPedido']['orderId'];
        $mail->Body     = $$tipomsg;
        $mail->AddAddress($clidata['email']);
        $mail->AddReplyTo($backEndEmail);
        //? $message_particular : $message_revendedor
        $mail->Send();
    } catch (phpmailerException $e) {
        file_put_contents('log-email.log', 'error cliente' . $e->errorMessage() . PHP_EOL, FILE_APPEND);
        file_put_contents('log-email.log', 'error tipo  ' . $tipomsg . PHP_EOL, FILE_APPEND);
        file_put_contents('log-email.log', 'error cliente cuerpo  ' . $msg_backend . PHP_EOL, FILE_APPEND);
    }
    // --------------------------

    // email al backend

    try {
        $mail_backend = new PHPMailer();
        $mail_backend->IsMail();
        $mail_backend->IsHTML(true);
        $mail_backend->CharSet  = "utf-8";
        $mail_backend->From     = $backEndEmail;
        $mail_backend->FromName = 'Gergal S.A.';
        $mail_backend->WordWrap = 50;
        $mail_backend->Subject  =  "Gergal S.A Pedido Nro. " . $_SESSION['detalleDelPedido']['orderId'];

        $mail_backend->AddAddress($backEndEmail);
        //$mail_backend->addCC('Fordoqui@nectica.com');
        $mail_backend->AddReplyTo($clidata['email']);


        $mail_backend->Body = $msg_backend;
        $mail_backend->Send();
    } catch (phpmailerException $e) {
        file_put_contents('log-email.log', 'error emai backend' . $e->errorMessage() . PHP_EOL, FILE_APPEND);
        file_put_contents('log-email.log', 'error  tipo backend' . $tipomsg . PHP_EOL, FILE_APPEND);
        file_put_contents('log-email.log', 'error cliente cuerpo  ' . $msg_backend . PHP_EOL, FILE_APPEND);
    }

    $cart->destroy();
    header("Location: gracias.php");
}
if ($_REQUEST['action'] == 'emptyCart') {
    $deleteItem = $cart->destroy();

    header("Location: carritoView.php");
    die;
}
