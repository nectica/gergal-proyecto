<?php 

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header.php'; 

?>

<!-- Main Wrapper Header -->

<div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras"
    data-stellar-background-ratio="0.4">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 columns">

                <div class="page-title">

                    <h1 class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        Checkout</h1>

                </div>



            </div>

        </div>



    </div>



</div>

<!-- Main Container -->

<div class="main-wrapper">

    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>

        <div class="row">

            <div class="col-md-12">

                <h3 class="fancy-title"><span>¡Muchas gracias!</span></h3>

                Tu pedido fue procesado con éxito. Vas a recibir un mail con el detalle de tu compra.

                <div class="white-space space-medium"></div>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <h3 class="fancy-title"><span>Detalle de la compra</span></h3>

                <table class="table cart-table table-hover">

                    <thead>

                        <tr>

                            <th>Cantidad</th>

                            <th>Producto</th>

                            <!-- <th class="text-right" >Subtotal</th> -->

                        </tr>

                    </thead>

                    <tbody>

                        <?php

							if(!empty($_SESSION['detalleDelPedido'])){

								$gtot = 0;

								foreach($_SESSION['detalleDelPedido']['cartItems'] as $item){

									if(is_array($item)){

										$subtot = $item['price'] * $item["qty"];

										$gtot += $subtot;

										

										echo "<tr><td>{$item["qty"]}</td><td><a href='detalle-producto.php?prod={$item['id']}' class='color-verde'>{$item['name']}</a></td></tr>";

									}

									

								}

								// <td class='text-right' colspan='6'>{$subtot}</td> *** deprecated

								if(is_array($item)){

									echo "</tbody><tfoot><tr><td class='text-right' colspan='6'>Total: <span class='lead'><strong>{$_SESSION['detalleDelPedido']['monto_total']}</strong></span></td></tr></tfoot></table>";

								}

								

							}

							?>

            </div>

        </div>

        <div class="row">

            <div class="col-md-12">

                <a href="index.php" class="btn btn-primary pull-right">Ir al sitio <i
                        class="fa fa-angle-right fa-margin-left"></i></a>

            </div>

        </div>

        <div class="white-space space-big"></div>

    </div>

    <!-- /Container -->

</div>

<!-- /Main Container -->

<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->

</div>

<!-- /Main Container -->

<!-- Footer Container -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->

</div>

<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->





<!-- login modal -->

<?php include 'loginView.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts.php'?>

<script>
document.title = "Gergal - Checkout";
</script>

</body>

</html>