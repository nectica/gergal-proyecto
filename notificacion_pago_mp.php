<?php
function logfile($str){
    $file='log.txt';
    $myfile = fopen(dirname(__FILE__)."/log.txt", "a") or die("Unable to open file!");
    fwrite($myfile, "\r\n");
    fwrite($myfile, $str);
    fclose($myfile);
}
$str_notificacion = file_get_contents("php://input");
logfile($str_notificacion);

if (!($notification=json_decode($str_notificacion))){
	header("HTTP/1.1 500 Error interno 1");
	exit();
}
require('db/Config.php');
$date = date('Y-m-d H:i:s');
$q = "INSERT INTO notificacion_mp (payment_id,fecha_creado,fecha_modificado) VALUES ('{$notification->data->id}','{$date}','{$date}')";
$insertMsg = $db->query($q);
if($insertMsg){
    logfile('insertOK;');
}else{
    logfile('fallo insert');
}
header("HTTP/1.1 200 OK");

/*
{"resource":"https://api.mercadolibre.com/merchant_orders/2136932862","topic":"merchant_order"}
{"action":"payment.created","api_version":"v1","data":{"id":"1232325080"},"date_created":"2020-12-23T13:38:36Z","id":6833150731,"live_mode":false,"type":"payment","user_id":"692348675"}

notificacion_mp (topic,payment_id,pedidos_id,status,status_detail,transaction_amount,payer_first_name,payer_last_name,payer_email,id_number,id_type,fecha_creado,fecha_modificado)

*/

?>