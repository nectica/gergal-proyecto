<?php 

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header.php'; 

?>

        <!-- Main Wrapper Header -->

        <div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras" data-stellar-background-ratio="0.4">

			<div class="container">

				<div class="row">

                	<div class="col-sm-12 columns">

                		<div class="page-title">

                    		<h1 class="script-font" style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">Instalaciones</h1>                    

                    	</div>

                    	

					</div>

				</div>

			</div>

        </div>       

		<!-- /Main Wrapper Header -->



		<!-- Main Container -->

		<div class="main-wrapper">



        	<!-- Container -->

            <div class="container">

            	<div class="white-space space-big"></div>

                

            	<div class="row">

				  

                  <div class="col-md-10 col-md-offset-1">

                    	<p class="lead text-center"><span class="color-verde">Contamos con dos plantaciones de frambuesas y moras: 8 hectáreas en Arrecifes al norte de la provincia de Buenos Aires y otra de 4 hectáreas en Carmen de Patagones al sur de la provincia de Buenos Aires.</span>

                          <br>

                          <br>

                          Además de su producción propia, Gergal cuenta con una red de productores de berries distribuidos de forma estratégica a lo largo y ancho de la Argentina para lograr así mayor volumen, y poder obtener berries frescos durante la mayor cantidad de meses posibles.

La empresa cuenta también con una planta de proceso con cámaras frigoríficas, un túnel de IQF, línea de clasificación y lavado de arándanos y con una planta procesadora y embotelladora de jugos de frutas, ambas en Villa Martelli provincia de Buenos</strong> </p>

                    <div class="white-space space-small"></div>

                    </div>

				</div> 

               

                

          </div>

			<!-- /Container -->                  

		</div>

		<!-- /Main Container --> 

        

        <div class="container">

            <div class="portfolio clearfix">

						<!-- Portfolio Grid -->

						<div class="portfolio-grid  portfolio-4-cols">


							<!-- Portfolio Element -->

                            <div class="element isotope-item print" data-content="#">

								<div class="element-inner animation fadeInRight">


                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-04.jpg" width="1200" height="900" alt="Instalaciones 1">                                  

                                    	<div class="overlay-wrapper-content">

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-04.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        	   
                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->

                            <!-- Portfolio Element -->

                            <div class="element isotope-item print" data-content="#">

								<div class="element-inner animation fadeInRight">


                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-05.jpg" width="1200" height="900" alt="instalaciones 2">                                  

                                    	<div class="overlay-wrapper-content">

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/demo/portfolio/instalaciones-05.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        	                                  	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->

							<!-- Portfolio Element -->

                            <div class="element isotope-item print" data-content="#">

								<div class="element-inner animation fadeInRight">



                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-06.jpg" width="1200" height="900" alt="instalaciones 3">                                  

                                    	<div class="overlay-wrapper-content">

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-06.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        	                                  	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->



							<!-- Portfolio Element -->

                            <div class="element isotope-item print" data-content="#">

								<div class="element-inner animation fadeInRight">



                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-07.jpg" width="1200" height="900" alt="instalaciones 4">                                  

                                    	<div class="overlay-wrapper-content">

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-07.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        	                                  	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->

                            
							<!-- Portfolio Element -->

                            <div class="element isotope-item print" data-content="#">

								<div class="element-inner animation fadeInRight">



                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-01.jpg" width="1200" height="900" alt="instalaciones 5">                                  

                                    	<div class="overlay-wrapper-content">

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-01.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        	                                  	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->



							<!-- Portfolio Element -->

                            <div class="element isotope-item print" data-content="#">

								<div class="element-inner animation fadeInRight">



                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-02.jpg" width="1200" height="900" alt="instalaciones 6">                                  

                                    	<div class="overlay-wrapper-content">

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-02.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        	                                  	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->

							<!-- Portfolio Element -->

                            <div class="element isotope-item branding" data-content="#">

								<div class="element-inner animation fadeInRight">



                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-03.jpg" width="1200" height="900" alt="instalaciones 7">                                  

                                    	<div class="overlay-wrapper-content">

                                    		

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-03.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        		                                 	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->


							<!-- Portfolio Element -->

                            <div class="element isotope-item branding" data-content="#">

								<div class="element-inner animation fadeInRight">



                                	<div class="overlay-wrapper">

										<img src="img/instalaciones/instalaciones-08.jpg" width="1200" height="900" alt="instalaciones 8">                                  

                                    	<div class="overlay-wrapper-content">

                                    		

                                        	<div class="overlay-details">

                                        		<a class="color-white" href="img/instalaciones/instalaciones-08.jpg" data-lightbox="image"><span class="livicon" data-n="plus"  data-color="#ffffff" data-hovercolor="#ffffff" data-op="1" data-onparent="true"></span></a>

                                        		                                 	

                                    		</div>

                                        	<div class="overlay-bg"></div>

                                    	</div>

                                    </div>										



								</div>

							</div>

							<!-- /Portfolio Element -->


						</div>

                        <!-- /Portfolio Grid -->

                        					

                    </div>

                    <div class="white-space space-big"></div>

            </div>

    
        <!-- Container destacado -->

            <!-- Fullsize -->

            <!-- Parallax -->                            
			 <?php include("parallax_gergal.php"); ?>
			<!-- /Parallax -->

            <!-- /Container -->      



		<!-- Footer destacado -->

		<?php include("footer.php"); ?>

		<!-- /Footer Container -->



	</div>	



	<!-- Back To Top -->

	<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>

	<!-- /Back To Top -->





	<!-- login modal -->

	<?php include 'loginView.php'?>



	<!-- scripts del template -->

	<?php include 'theme_scripts.php'?>

	<!-- custom scripts -->

	<?php include 'custom_scripts.php'?>

	<script>

		document.title = "Gergal - Instalaciones" ;

	</script>

  </body>

</html>

