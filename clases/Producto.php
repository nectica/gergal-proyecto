<?php
class Producto {
	public $db=0;
	
    public function __construct(){
        include ('db/Config.php');
        $this->db = $db;
    }

    /**
     * listado: retorna el contenido de la base desde $offset hasta $limit
     * @param    int offset , int limit, array categorias
     * @return    array
     */
    

    public function listado($row=3,$cols=4,$offset=0,$limit=12,$categoria=0,$search='',$order='',$destacado=-1){
        $classCols = ($cols == 4)?3:4; 
        $r = '';
        $minStockCant = 1;
        //deprecated ***   WHERE cantidad_en_stock > " . $minStockCant. "
		if (isset($_SESSION['tipo_usuario_id'])) {
			if ($_SESSION['tipo_usuario_id']==2) {
				$whereSelector = " WHERE estado_revendedores > 0";							
			} else {
				$whereSelector = " WHERE estado > 0";							
			}
		} else {
			$whereSelector = " WHERE estado > 0";			
		}
        if ($destacado==0) {
			$whereSelector .= " and destacado = 0 ";
		}
        if ($destacado==1) {
			$whereSelector .= " and destacado = 1 ";
		}
        $orderSelect = ' ORDER BY id ASC ' ;
        if($categoria > 0){
            $whereSelector .= " AND categoria = '{$categoria}' ";    
        }
        if($search !== ''){
            $whereSelector .= " AND productos.nombre LIKE '%{$search}%' ";    
        }
        $qtp = $this->db->query("SELECT COUNT(id) as totalProds FROM productos {$whereSelector} ");
        if($order != ''){
            switch ($order) {
                case 'ventas':
                    $orderSelect = ' ORDER BY case when `cantidad_en_stock`<=0 then 1 else 0 end, orden ';
                    break;
                case 'novedad':
                    $orderSelect = ' ORDER BY fecha_creado DESC ';
                    break;
                case 'precio-asc':
                    $orderSelect = ' ORDER BY case when `cantidad_en_stock`<=0 then 1 else 0 end, precio_lista ASC ';
                    break;
                case 'precio-desc':
                    $orderSelect = ' ORDER BY case when `cantidad_en_stock`<=0 then 1 else 0 end, precio_lista DESC ';
                    break;
            }
        }
        if($order !=='' && $order == 'ventas' && 1==0){ //Deshabilitado por ahora
            $q = "SELECT productos.id,nombre,descripcion,image_path,hover_image_path,categoria as categoria_id,(SELECT nombre FROM categorias WHERE id = categoria) as categoria,propiedades,info_nutricional,precio_lista,precio_revendedor,productos.cantidad_en_stock,s.totalCount AS ventas_count  FROM productos 
            LEFT JOIN(
                SELECT  pedidos_items.id, SUM(pedidos_items.cant) totalCount 
                FROM    pedidos_items GROUP BY pedidos_items.id
                )  s ON s.id = productos.id
            {$whereSelector} {$orderSelect} LIMIT {$offset},{$limit}";
        }else{
            $q = "SELECT productos.id,productos.nombre,productos.descripcion,productos.cantidad_en_stock,productos.image_path,productos.hover_image_path,productos.categoria as categoria_id,
			c.nombre as categoria,productos.propiedades,productos.info_nutricional,productos.precio_lista,precio_revendedor 
			FROM productos inner join categorias c on productos.categoria = c.id {$whereSelector} {$orderSelect} LIMIT {$offset},{$limit}";
        
        }
        $query = $this->db->query($q);
        if($qtp->num_rows > 0 && $query->num_rows > 0){ 
            $curr_col=1;
            $curr_row=1;
            $curr_count=$offset;
            while($row = $query->fetch_assoc()){
                $curr_count ++;
                if($curr_col > $cols){
                    $curr_row ++;
                    $curr_col = 1;
                    $r .= "</div><div class='row'>"; 
                }
                $tit = $this->setStringMaxLength($row['nombre'],60);
                // $tit = $row['nombre'];
                $r .= "
                <div class='col-md-{$classCols}'><div class='shop-product'>
                <a href='detalle-producto.php?prod={$row['id']}'>
                <div class='overlay-wrapper'>
                <img class='size-wrapper-prod' src='/gergal-abm/{$row['image_path']}' alt='{$tit}'>
                </div>
                <div class='shop-product-info'><h5 class='product-name' style='min-height:35px;max-height:35px'>{$tit}</h5></a><p class='product-category'><a href='listado-productos.php?categoria={$row['categoria_id']}'>{$row['categoria']}</a></p>";
                // PRECIO DE REVENDEDORES
                if( array_key_exists('tipo_usuario_id',$_SESSION) && $_SESSION['tipo_usuario_id'] == 2){
                    $r .= "<p style='margin-bottom:16px; margin-top:8px; line-height:0px; text-decoration:line-through; color:#000000; font-weight:400; font-size:14px'>$ {$row['precio_lista']}</p><p class='product-price'>$ {$row['precio_revendedor']}</p>";
                }
                // precio de lista
                else{
                    $r .="<p class='product-price'>$ {$row['precio_lista']}</p>";
                } 
                // ****
                
                if($row['cantidad_en_stock'] < 1){
                    $r .= "</div>
                    <div class='product-links'><input type='number' min=0 max=999 id='cant_{$row['id']}' value=0 disabled style='width:50px'>
                    <div class='btn btn-verde btn-xs'>SIN STOCK</div>
                    </div>";
                }else{
                    $r .= "</div>
                    <div class='product-links'><input type='number' min=0 max={$row['cantidad_en_stock']} id='cant_{$row['id']}' value=1 style='width:50px'>
                    <div class='btn btn-verde btn-xs' onclick=callCarroAccion('cant',{$row['id']}) >AGREGAR</div>
                    </div>";
                }
                
                $r .= "</div>
                <div class='white-space space-small'></div></div>";
                $curr_col ++;
            } 
            return ['totalProds'=>$qtp->fetch_assoc()['totalProds'],'desde'=>$offset+1,'hasta'=>$curr_count,'rows'=>$r];
        }else{         
            return ['totalProds'=>0,'rows'=>"No se encontraron productos para la categoria "]; ;
        }
    }
    
    /**
     * image hover no esta en uso
     * // <img class='img-hover size-wrapper-prod' src='/gergal-abm/{$row['hover_image_path']}' alt='{$tit}'>
     */


    /**
     * carrousel_items: retorna items para el formato carrousel
     * @param    int cant ,  array categorias
     * @return    array de elementos html
     */
    public function carrousel_items($cant,$categoria=null){
//        require_once ('db/Config.php');
        $r = '';
        $minStockCant = 1;
		if (isset($_SESSION['tipo_usuario_id'])) {
			if ($_SESSION['tipo_usuario_id']==2) {
				$whereSelector = " WHERE estado_revendedores > 0";							
			} else {
				$whereSelector = " WHERE estado > 0";							
			}
		} else {
			$whereSelector = " WHERE estado > 0";			
		}		
        $whereSelector .= " AND cantidad_en_stock > " . $minStockCant;
        
        if($categoria !== null){
             $whereSelector .= " AND categoria LIKE '{$categoria}'";
        }
        $query = $this->db->query("SELECT 
            productos.id,productos.nombre,descripcion,productos.cantidad_en_stock,image_path,hover_image_path,categoria as categoria_id,
			categorias.nombre as categoria,propiedades,info_nutricional,precio_lista,precio_revendedor    
            FROM productos inner join categorias on categorias.id = productos.categoria {$whereSelector} LIMIT {$cant}");
        if($query->num_rows > 0){ 
            while($row = $query->fetch_assoc()){
                $tit = $this->setStringMaxLength($row['nombre'],25);
                $r .= "
                <div class='carousel-item'>
                    <div class='shop-product'>
                        <a href='detalle-producto.php?prod={$row['id']}'>
                        <div class='overlay-wrapper'>
                            <img class='size-wrapper-prod' src='/gergal-abm/{$row['image_path']}' alt='{$tit}'>
                            
                        </div>
                        <div class='shop-product-info'>
                            <a href='detalle-producto.php?prod={$row['id']}'><h5 class='product-name'>{$tit}</h5></a>
                            <p class='product-category'><a href='listado-productos.php?categoria={$row['categoria_id']}'>{$row['categoria']}</a></p>
                            <p class='product-price'>$ {$row['precio_lista']}</p>
                        </div>";
                if($row['cantidad_en_stock'] < 1){
                    $r .= "</div>
                    <div class='product-links'><input type='number' min=0 max=999 id='cant_{$row['id']}' value=0 disabled style='width:50px'>
                    <div class='btn btn-verde btn-xs'>SIN STOCK</div>
                    </div>";
                }else{
                    $r .= "<div class='product-links'><input type='number'  min=0 max=999 id='crlCant_{$row['id']}' value=1 style='width:50px'>
                    <div class='btn btn-verde btn-xs' onclick=callCarroAccion('crlCant','{$row['id']}') >AGREGAR</div>
                        </div>
                    </div>";
                }
                $r.="<div class='white-space space-small'></div>
                </div>";
            } 
            return $r;
        }else{         
            return  "No se encontraron productos para la categoria ";
        }
    }
	
	
    public function novedades_items($cant,$categoria=null){
        //require_once ('db/Config.php');
        $r = '';
        $minStockCant = 1;
		if (isset($_SESSION['tipo_usuario_id'])) {
			if ($_SESSION['tipo_usuario_id']==2) {
				$whereSelector = " WHERE estado_revendedores > 0";							
			} else {
				$whereSelector = " WHERE estado > 0";							
			}
		} else {
			$whereSelector = " WHERE estado > 0";			
		}		
        $whereSelector .= " AND novedades = 1 AND cantidad_en_stock > " . $minStockCant;
        
        if($categoria !== null){
             $whereSelector .= " AND categoria LIKE '{$categoria}'";
        }
        $query = $this->db->query("SELECT 
            productos.id,productos.nombre,descripcion,productos.cantidad_en_stock,image_path,hover_image_path,categoria as categoria_id,c.nombre as categoria,
			propiedades,info_nutricional,precio_lista,precio_revendedor    
            FROM productos inner join categorias c on c.id = productos.categoria {$whereSelector} ORDER BY orden LIMIT {$cant}");
        if($query->num_rows > 0){ 
            while($row = $query->fetch_assoc()){
                $tit = $this->setStringMaxLength($row['nombre'],25);
                $r .= "
                <div class='carousel-item'>
                    <div class='shop-product'>
                        <a href='detalle-producto.php?prod={$row['id']}'>
                        <div class='overlay-wrapper'>
                            <img class='size-wrapper-prod' src='/gergal-abm/{$row['image_path']}' alt='{$tit}'>
                            
                        </div>
                        <div class='shop-product-info'>
                            <a href='detalle-producto.php?prod={$row['id']}'><h5 class='product-name'>{$tit}</h5></a>
                            <p class='product-category'><a href='listado-productos.php?categoria={$row['categoria_id']}'>{$row['categoria']}</a></p>
                            <p class='product-price'>$ {$row['precio_lista']}</p>
                        </div>";
                if($row['cantidad_en_stock'] < 1){
                    $r .= "</div>
                    <div class='product-links'><input type='number' min=0 max=999 id='cant_{$row['id']}' value=0 disabled style='width:50px'>
                    <div class='btn btn-verde btn-xs'>SIN STOCK</div>
                    </div>";
                }else{
                    $r .= "<div class='product-links'><input type='number'  min=0 max=999 id='crlCant_{$row['id']}' value=1 style='width:50px'>
                    <div class='btn btn-verde btn-xs' onclick=callCarroAccion('crlCant','{$row['id']}') >AGREGAR</div>
                        </div>
                    </div>";
                }
                $r.="<div class='white-space space-small'></div>
                </div>";
            } 
            return $r;
        }else{         
            return  "No se encontraron novedades";
        }
    }	
// // <img class='img-hover size-wrapper-prod' src='/gergal-abm/{$row['hover_image_path']}' alt='{$tit}'> *** HOVER DE IMAGEN DE PRODUCTO NO SE USA  ****

    /**
     * detalle: retorna todas las categorias
     * @return    array data
     */
    public function getCategorias(){
//        include ('db/Config.php');
        $res = [];
        $query = $this->db->query("SELECT count(1) as q, c.id, c.nombre FROM categorias c inner join productos p on c.id = p.categoria group by  c.id, c.nombre ORDER BY c.nombre");
        while($row = $query->fetch_assoc()){
//            $cCat = $this->db->query("SELECT count(id) FROM productos WHERE categoria = {$row['id']}");
            //$cCat = $this->db->query("SELECT count(id) FROM productos WHERE categoria = {$row['id']}");
            // $cSubCat =  $db->query("SELECT count(id) FROM productos WHERE sub_categoria = {$row['id']}");
            //$countCat = ($cCat->num_rows > 0)?$cCat->fetch_row()[0]:0;
            // $countSubCat = ($cSubCat->num_rows > 0)?$cSubCat->fetch_row()[0]:0;
            $res[]=['id'=>$row['id'],'nombre'=>$row['nombre'],'count'=>$row['q']];
        }
        return $res;
    } 

    public function getCatId($id){
        //include ('db/Config.php');
        $q = $this->db->query("SELECT categoria FROM productos WHERE id = {$id}");
        if($q->num_rows > 0 && $row = $q->fetch_assoc()){
            return $row['categoria'];
        }else{
            return 'Productos';
        }

    }

    public function getCatNom($id){
        //include ('db/Config.php');
        $q = $this->db->query("SELECT nombre FROM categorias WHERE id = {$id}");
        if($q->num_rows > 0 &&  $row = $q->fetch_assoc()){
            return $row['nombre'];
        }else{
            return 'Listado de Productos';
        } 
    }

    public function getCategoriasScreenObjects(){
        //include ('db/Config.php');
        $res = "<div class='row'>";
        $query = $this->db->query("SELECT * FROM categorias ORDER BY id ASC");
        $objsPerRow = 4;
        $c=1;
        while($row = $query->fetch_assoc()){
            $res .= "<div class='col-md-4'><div class='shop-product'>
                <a href='listado-productos.php?categoria={$row['id']}'>
                <!-- Overlay Img -->
                <div class='overlay-wrapper'>
                    <img class='size-wrapper-catg' src='/gergal-abm/{$row['imagen']}' alt='{$row['nombre']}'>
                </div>
                <!-- Overlay Img -->
                <div class='shop-product-info'>
                     <p class='product-category'></p><h5 class='product-name'>{$row['nombre']}</h5><p></p>
                </div>
               </div></a>
               <div class='white-space space-small'></div>
               </div>";
               $c++;
               if($c % $objsPerRow == 0){
                    $c = 1;
                    $res .= "</div><div class='row'>";
               }
        }
        $res .= "</div>";
        return $res;

    }

    /**
     * detalle: retorna los datos del detalle del producto
     * @param    int id del producto 
     * @return    array data
     */
    public function detalle($prodId){
        //include ('db/Config.php');
        highlight_string("<?php data = " . var_export($prodId, true) . ";?>");
        exit;
    }


    /**
     * detalle: retorna los 10 mas vendidos
     * 
     * @return    array data
     */
    function getMasVendidos(){
        //include ('db/Config.php');
        $res = [];
		if (isset($_SESSION['tipo_usuario_id'])) {
			if ($_SESSION['tipo_usuario_id']==2) {
				$whereSelector = " WHERE estado_revendedores > 0";							
			} else {
				$whereSelector = " WHERE estado > 0";							
			}
		} else {
			$whereSelector = " WHERE estado > 0";			
		}		
		$sql = "SELECT pedidos_items.producto_id,pedidos_items.producto_nombre,pedidos_items.producto_precio_unitario, SUM(pedidos_items.cant) AS TotalVentas,
		 precio_revendedor AS producto_precio_revendedor,image_path 
         FROM pedidos_items inner join productos on productos.id = pedidos_items.producto_id " . $whereSelector . " GROUP BY pedidos_items.producto_id ORDER BY SUM(pedidos_items.cant) DESC LIMIT 0 , 5";
         $query = $this->db->query($sql);
        if(!empty($query)){
            while($row = $query->fetch_assoc()){
                $n = $this->setStringMaxLength($row['producto_nombre'],30);
                $res[]= [
                    'producto_id'=>$row['producto_id'],
                    'producto_nombre'=>$n,
                    'producto_precio_unitario'=>$row['producto_precio_unitario'],
                    'producto_precio_revendedor'=>$row['producto_precio_revendedor'],
                    'image_path'=>$row['image_path'] //$this->getProdImagePath($row['producto_id'])					
                ]; 
            }
        }
        return $res;
    }

    function getProdImagePath($prodId){
        //include ('db/Config.php');
        $image = '';
        $q = $this->db->query("SELECT image_path FROM `productos` WHERE id   = {$prodId} LIMIT 1 ");
        $row = $q->fetch_assoc();
        if(isset($row)){
            $image = $row['image_path'];
        }
        return $image;
    }  
    
    function setStringMaxLength($str,$ml){
        $r = '';
        // $log = 'len:'.strlen($str).'result'.(strlen($str) <= $ml);
        // file_put_contents('log/errors.txt', $log, FILE_APPEND);
        
        
        if(strlen($str) <= $ml){$r = $str;}
        else{$r = substr($str,0,($ml-3)).'...';}
        return $r;      
    }
 
}