<?php
session_start();
class Login{
    public function __construct(){}

    public function login($data){
        include('db/Config.php');
        $q = "SELECT * FROM usuarios WHERE email = '{$data["login-email"]}'";
        $query = $db->query($q);
        $row = $query->fetch_assoc();
        if(empty($row)){
            $log = 'no encontro email..:'.$data["login-email"];
            file_put_contents('log/errors.txt', $log, FILE_APPEND);
            return false;
        }else{
            $inputPass = $data['login-password'];
            $dbPass = $row['password'];
            if(password_verify($inputPass, $dbPass )){
                $log = 'logueo ok:'.$data["login-email"];
                file_put_contents('log/errors.txt', $log, FILE_APPEND);
                $_SESSION['loggedIn'] = $row['nombre'].' '.$row['apellido'];
                $_SESSION['loggedInId'] = $row['id'];

                $_SESSION['loggedNombre'] = $row['nombre'];
                $_SESSION['loggedApellido'] = $row['apellido'];
                $_SESSION['loggedDNI'] = $row['dni'];
                $_SESSION['loggedTelefono'] = $row['telefono'];
                $_SESSION['loggedEmail'] = $row['email'];
                $_SESSION['loggedCalle'] = $row['nombre_calle'];
                $_SESSION['loggedNumero'] = $row['calle_nro'];
                $_SESSION['loggedDireccion2'] = $row['direccion2'];
                $_SESSION['loggedLocalidad'] = $row['localidad'];
                $_SESSION['loggedBarrio'] = $row['barrio'];


                $_SESSION['tipo_usuario_id'] = $row['tipo_usuario_id'];
            return true;    
                
            }else{
                $log = 'log FAiled:';
                file_put_contents('log/errors.txt', $log, FILE_APPEND);
                return false;
            }
            

        }
    }
	
    public function reload($data){
        include('db/Config.php');
        $q = "SELECT * FROM usuarios WHERE email = '{$data["login-email"]}'";
        $query = $db->query($q);
        $row = $query->fetch_assoc();
        if(empty($row)){
            $log = 'no encontro email..:'.$data["login-email"];
            file_put_contents('log/errors.txt', $log, FILE_APPEND);
            return false;
        }else{
			$_SESSION['loggedIn'] = $row['nombre'].' '.$row['apellido'];
			$_SESSION['loggedInId'] = $row['id'];
			$_SESSION['loggedNombre'] = $row['nombre'];
			$_SESSION['loggedApellido'] = $row['apellido'];
			$_SESSION['loggedDNI'] = $row['dni'];
			$_SESSION['loggedTelefono'] = $row['telefono'];
			$_SESSION['loggedEmail'] = $row['email'];
			$_SESSION['loggedCalle'] = $row['nombre_calle'];
			$_SESSION['loggedNumero'] = $row['calle_nro'];
			$_SESSION['loggedDireccion2'] = $row['direccion2'];
			$_SESSION['loggedLocalidad'] = $row['localidad'];
			$_SESSION['loggedBarrio'] = $row['barrio'];
			$_SESSION['tipo_usuario_id'] = $row['tipo_usuario_id'];
            return true;    
        }
    }	

    public function check_email($email){
        include ('db/Config.php');
        $query = $db->query("SELECT * FROM usuarios WHERE email = '{$email}'");
        if($query->num_rows > 0){
            return 'found';
        }else{
            return 'not_found';
        }
    }
    public function signup($data){
        include ('db/Config.php');
        
        if(!empty($data) && is_array($data)){
            $columns = '';
            $values  = '';
            $i = 0;
            foreach($data as $key=>$val){
                $pre = ($i > 0)?', ':'';
                $columns .= $pre.$key;
                $values  .= $pre."'".$val."'";
                $i++;
            }
            $query = "INSERT INTO usuarios (".$columns.") VALUES (".$values.")";
            $log  = "Data: ".$query.
            "-------------------------".PHP_EOL;
                    
            
			//Save string to log, use FILE_APPEND to append.
			file_put_contents('jp_logfile.log', $log, FILE_APPEND);

            if ($insert = $db->query($query)) {
				return true;
			} else {
				return false;
			}
            
    

            
            return $insert;
        }else{
            return false;
        }
    }
	public function edit($data){
        include ('db/Config.php');
        
        if(!empty($data) && is_array($data)){
            $columns = "fecha_modif='" . date("Y-m-d H:i:s") . "'";
            foreach($data as $key=>$val){
                $pre = ', ';
                $columns .= $pre . $key . "=" . "'".$val."'";
            }
            $query = "update usuarios set " . $columns . " where id = " . $_SESSION['loggedInId'];

            $log  = "Data: ".$query.
            "-------------------------".PHP_EOL;
                    
            
			//Save string to log, use FILE_APPEND to append.
			file_put_contents('jp_logfile.log', $log, FILE_APPEND);

            if ($insert = $db->query($query)) {
				return true;
			} else {
				return false;
			}
            
    

            
            return $insert;
        }else{
            return false;
        }
    }
    public function recover($email){
        $query = $db->query("SELECT * FROM clientes WHERE email = ".$email);
        if($query->num_rows > 0){
            return 'se envia email con el password';
        }else{
            return 'email no registrado';
        }
    } 
}