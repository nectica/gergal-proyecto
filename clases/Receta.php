<?php
class Receta {
    public function __construct(){
        
    }

    /**
     * listado: retorna el contenido de la base desde $offset hasta $limit
     * @param    int offset , int limit
     * @return    array
     */
    
    public function get_carousel_boxes($offset=0,$limit=12){
        include ('db/Config.php');
        $r = '';
        $query = $db->query("SELECT * FROM recetas ORDER BY id ASC LIMIT {$offset},{$limit}");
        if($query->num_rows > 0){ 
            while($crow = $query->fetch_assoc()){
                $bajada = substr(($crow['ingredientes'].' '.$crow['preparacion']),0,320)."...";
                $r .= "
                <div class='carousel-item'>
                    <a href='detalle-recetas.php?id={$crow['id']}'><img src='{$crow['image_path']}' alt='Receta'></a>
                    <div class='panel panel-default'>
                        <div class='panel-body bg-color-white' style='min-height:252px;max-height:252px;    overflow:hidden;'>
                            <h5><a href='detalle-recetas.php?id={$crow['id']}' class='color-verde'>{$crow['titulo']}</a></h5>
                                <p>$bajada</p>
                        </div>
                    </div>
                </div>";
            } 
            return $r;
        }else{         
            return  "No se encontraron productos para la categoria ";
        }
    }
    

    public function get_listado_boxes($offset=0,$limit=12){
        include ('db/Config.php');
        $r = '';
        $query = $db->query("SELECT * FROM recetas ORDER BY id ASC LIMIT {$offset},{$limit}");
        if($query->num_rows > 0){ 
            while($row = $query->fetch_assoc()){
                $bajada = substr(($row['ingredientes'].' '.$row['preparacion']),0,320)."...";
                $r .= "
                <div class='carousel-item'>
                    <a href='receta.php?id{$row['id']}'><img src='{$row['image_path']}' alt='Receta'></a>
                    <div class='panel panel-default'>
                        <div class='panel-body bg-color-white'>
                            <h5><a href='#' class='color-verde'>{$row['titulo']}</a></h5>
                                <p>$bajada</p>
                        </div>
                    </div>
                </div>";
            } 
            return $r;
        }else{         
            return  "No se encontraron productos para la categoria ";
        }
    }

    public function getRecetas($offset=0,$limit=3){
        include ('db/Config.php');
        $cr = $db->query("SELECT count(id) as tot FROM recetas ");
        $res = [];
        $query = $db->query("SELECT * FROM recetas ORDER BY id ASC LIMIT {$offset},{$limit} ");
        if($query->num_rows > 0){ 
            while($row = $query->fetch_assoc()){
                $res[]=$row;  
            }
        }
        if($cr->num_rows > 0 ){
            $r = $cr->fetch_assoc();
            $pages = ($r['tot']>$limit)?($r['tot']/$limit):1;
            return ['pages'=>$pages,'boxes'=>$res];
        }else{
            return false;
        }
        
        
    }

    public function getReceta($id){
        include ('db/Config.php');
        $r = '';
        $query = $db->query("SELECT * FROM recetas WHERE id = {$id}");
        if($query->num_rows > 0){ 
            return $query->fetch_assoc();
        }else{
            return false;
        }
    }

}
