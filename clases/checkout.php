<?php
class Checkout{
    public $dta = [];
    
    public function __construct($uId){
        include 'db/Config.php';
        $q = "SELECT * FROM usuarios WHERE id = {$uId} ";
        $query = $db->query($q);
        if($query->num_rows > 0){
            $this->dta['user'] = $query->fetch_assoc();
        }  
    }


    public function getRevendedoresCercanos(){
        $maxDistanciaKilometros = 10;
        $res = [];
        include 'db/Config.php';
        $q = "SELECT * FROM usuarios WHERE tipo_usuario_id = (SELECT id FROM tipo_usuario WHERE descripcion = 'revendedor') ";
        $query = $db->query($q);
        while($row = $query->fetch_assoc()){
            $latA = floatval($this->dta['user']['latitud']);
            $lngA = floatval($this->dta['user']['longitud']);
            $latB = floatval($row['latitud']);
            $lngB = floatval($row['longitud']);
            if($this->distance($latA,$lngA,$latB,$lngB) <= $maxDistanciaKilometros){
                $res[] = [
                    'id'=> $row['id'],
                    'nombre' => $row['nombre'] ." ". $row['apellido'],
                    'email' => $row['email'],
                    'telefono'=>$row['telefono'],
                ];
            }
        }
        return $res;
    }

    private function distance($latA, $lngA,$latB, $lngB) {
        $R = 6371000;
        $radiansLAT_A = deg2rad($latA);
        $radiansLAT_B = deg2rad($latB);
        $variationLAT = deg2rad($latB - $latA);
        $variationLNG = deg2rad($lngB - $lngA);

        $a = sin($variationLAT/2) * sin($variationLAT/2) 
            + cos($radiansLAT_A) * cos($radiansLAT_B) * sin($variationLNG/2) * sin($variationLNG/2);

        $c = 2 * atan2(sqrt($a), sqrt(1-$a));

        $d = $R * $c;

        return $d /1000;
    }  

}