<!-- Geocoder Api -->
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBD9PqkcjYtEfSuqLCP_gSOxx-aP4XpD4Q"></script>-->


<script>
	function updateCartItem(obj, id) {
		$.get("carroAccion.php", {
			action: "updateCartItem",
			id: id,
			cant: obj.value
		}, function(data) {
			console.log('receive', data)
			if (data == 'reload') {
				location.reload();
			}
			else if (data !== 'ok') {
				alert('falló la conexion con el servidor, intente nuevamente.')
			}
		});
	}


	function callCarroAccion(orig, currId) {
		let item = orig + '_' + currId;
		let inputCantidad = document.querySelector("#" + item);
		let currCant = inputCantidad.value;
		//let currCant = $("#"+item).val();
		console.log(currCant, inputCantidad.max, );
		if (currCant < 1) {

			alert("Seleccione un numero correcto.");
			return;
		} else if (parseInt(currCant) > parseInt(inputCantidad.max)) {
			alert("No hay stock suficiente para esa cantidad, seleccione una inferior a " + inputCantidad.max + ' .');
			return;

		}
		$.get("carroAccion.php", {
			action: "addToCart",
			id: currId,
			cant: currCant
		}, function(data) {
			const res = JSON.parse(data);
			console.log('respuesta de carro accion', data,  res)
			if (res.hasOwnProperty('status') && res.status == 'error') {
				alert(res.msg);
				return;
			}

			$('#headerCarroCantItems').html(res.items);
			$('#headerCarroTotal').html(res.total.toFixed(2));
		});

		// window.location.href=`carroAccion.php?action=addToCart&id=${id}&cant=${cant};return false;`;
		// console.log('showwww')
		// toast show
		var alertToast = $(".alert-container");
		alertToast.addClass('z-index', 10000);
		alertToast.slideDown();
		window.setTimeout(function() {
			alertToast.slideUp();
		}, 2500);
	}
	// buscador en NavBar
	function search() {
		const s = $('#search_txt').val().replace(/[^A-Za-z0-9-ñÑáéíóúÁÉÍÓÚ]+/g, "");
		window.location.assign(`listado-productos.php?search=${s}`)
	}
	// orden de productos en listado 
	function selectOrder() {
		const order = $('#orderSelection').val();
		const currPage = <?php echo (isset($_REQUEST['page'])) ? $_REQUEST['page'] : 1; ?>;
		const currCat = <?php echo (isset($cat)) ? $cat : 0; ?>;
		const currSearch = '';
		window.location.href = `listado-productos.php?categoria=${currCat}&search=${currSearch}&page=${currPage}&order=${order}`;
	}

	// validacion de login
	function validateRecover() {
		const email = $('#recover-email').val();
		const elem = 'fg-recover';
		form = $('#Forgot-Password-Form')
		if (validateEmail(email, elem) == 1) {
			console.log('sending...', form.serialize())
			$.ajax({
				type: "POST",
				url: 'loginAccion.php',
				data: form.serialize(),

				success: function(data) {
					 // respuesta del servidor
					 json = JSON.parse(data);
					console.log('res', data);
					if (json.hasOwnProperty('status') && json.status == 'error') {
						alert(json.msg);
						return;
					} else {
						alert("Se envió un email con instrucciones para resetear la clave de acceso");

						/* window.location.href = "index.php"; */
					}

					/* ; */
				}
			});
			return false
		} else {
			alert("El email ingresado no pertenece a un usuario registrado.");
			return false
		}
	}

	function resetLatLong() {
		$('#latitud').val(0);
		$('#longitud').val(0);
	}

	function resetItem(i) {
		if (i == 'signup-direccion') {
			resetLatLong();
			$('#fg-' + i).removeClass('has-error').removeClass('has-success');
		} else {
			$('#' + i).val('');
			$('#fg-' + i).removeClass('has-error').removeClass('has-success');
		}
	}

	function checkDni() {
		const dni = $('#signup-dni').val();
		console.log('dni chek', (parseInt(dni) < 1000000 || isNaN(parseInt(dni))));
		if (parseInt(dni) < 1000000 || isNaN(parseInt(dni))) {
			validateNotEmpty($('#signup-dni').val(), 'fg-signup-dni');
			$('#fg-signup-dni').removeClass('has-success').addClass('has-error');
			// $('#signup-dni').val('Nro DNI No Valido');
			// $('#signup-dni').focus();
			//alert('Nro de DNI no valido');
			return 0
		} else {
			$('#fg-signup-dni').removeClass('has-error').addClass('has-success');
			return 1;
		}

	}

	function validateEdit() {
		let vt = 0;
		let vtTotal = 8;
		vt += validateText($('#signup-nombre').val(), 'fg-signup-nombre');
		vt += validateText($('#signup-apellido').val(), 'fg-signup-apellido');
		vt += validateDNI($('#signup-dni').val(), 'fg-signup-dni');
		vt += validateAddress($('#signup-calle').val(), $('#signup-numero').val(), 'fg-signup-direccion');
		vt += validateLocalidad($('#signup-localidad').val(), 'fg-signup-direccion-3');
		vt += validateText($('#signup-barrio').val(), 'fg-signup-direccion-3');
		vt += validateText($('#signup-telefono').val(), 'fg-signup-telefono');
		vt += validateEmail($('#signup-email').val(), 'fg-signup-email');
		if ($('#signup-password').val() != '') {
			vt += validatePass($('#signup-password').val(), 'fg-signup-password');
			vt += validateConfirm($('#signup-password').val(), $('#signup-confirmar-password').val(), 'fg-signup-confirmar-password');
			vtTotal = vtTotal + 2;
		}
		if (vt == vtTotal) {
			form = $('#signin-form')
			// console.log('sending',form.serialize());
			$.ajax({
				type: "POST",
				url: 'loginAccion.php',
				data: form.serialize(),
				success: function(data) {
					if (data == 'error1') {
						msg = `<div class="iconbox-wrapper circle bg-color-rojo color-white iconbox-1x aligncenter">
											<i class="icon gfx-alert"></i>
										</div>	
										<h5 class="text-center">Ya existe una cuenta con esa direccion de correo</h5>
										<div class="white-space space-xsmall"></div>
										`;
					} else {
						msg = `<div class="iconbox-wrapper circle bg-color-verde color-white iconbox-1x aligncenter">
											<i class="icon gfx-check"></i>
										</div>	
										<h5 class="text-center">Solicitud de actualizacion procesada exitosamente.</h5>
										<div class="white-space space-xsmall"></div>
										`;

					}
					const footer = `<a href="#" onclick="window.location.reload(true);">Continuar</a>`;
					$('#signup-modal-body').html(msg);
					$('#signup-modal-header').html('');
					$('#signup-modal-footer').html(footer);

				}
			});

		} else {
			// console.log('NOT sending');
			alert('Debe completar los campos requeridos')
			return false;
		}
		return false;
	}

	function validateSignup() {
		let vt = 0;
		vt += validateText($('#signup-nombre').val(), 'fg-signup-nombre');
		vt += validateText($('#signup-apellido').val(), 'fg-signup-apellido');
		vt += validateDNI($('#signup-dni').val(), 'fg-signup-dni');
		vt += validateAddress($('#signup-calle').val(), $('#signup-numero').val(), 'fg-signup-direccion');
		vt += validateLocalidad($('#signup-localidad').val(), 'fg-signup-direccion-3');
		vt += validateText($('#signup-barrio').val(), 'fg-signup-direccion-3');
		vt += validateText($('#signup-telefono').val(), 'fg-signup-telefono');
		vt += validateEmail($('#signup-email').val(), 'fg-signup-email');
		vt += validatePass($('#signup-password').val(), 'fg-signup-password');
		vt += validateConfirm($('#signup-password').val(), $('#signup-confirmar-password').val(), 'fg-signup-confirmar-password');
		if (vt == 10) {
			form = $('#signin-form')
			// console.log('sending',form.serialize());
			$.ajax({
				type: "POST",
				url: 'loginAccion.php',
				data: form.serialize(),
				success: function(data) {
					if (data == 'error1') {
						msg = `<div class="iconbox-wrapper circle bg-color-rojo color-white iconbox-1x aligncenter">
											<i class="icon gfx-alert"></i>
										</div>	
										<h5 class="text-center">Ya existe una cuenta con esa direccion de correo</h5>
										<div class="white-space space-xsmall"></div>
										`;
					} else {
						msg = `<div class="iconbox-wrapper circle bg-color-verde color-white iconbox-1x aligncenter">
											<i class="icon gfx-check"></i>
										</div>	
										<h5 class="text-center">Solicitud de registro procesada exitosamente.</h5>
										<div class="white-space space-xsmall"></div>
										`;

					}
					const footer = `<a href="#" onclick="window.location.reload(true);">Continuar</a>`;
					$('#signup-modal-body').html(msg);
					$('#signup-modal-header').html('');
					$('#signup-modal-footer').html(footer);

				}
			});

		} else {
			// console.log('NOT sending');
			alert('Debe completar los campos requeridos')
			return false;
		}
		return false;
	}

	function validateLogin() {
		const email = $('#login-email').val();
		const pass = $('#login-password').val();
		form = $('#login-form')
		if (validateEmail(email, 'fg-login-email') == 1 && validatePass(pass, 'fg-login-password') == 1) {
			$.ajax({
				type: "POST",
				url: 'loginAccion.php',
				data: form.serialize(),
				success: function(data) {
					console.log('res', data); // respuesta del servidor
					window.location.assign(data);
				}
			});
		} else {
			console.log('NOT sending');
			alert('Usuario o clave no valido');
			return false
		}
	}

	function validateEmail(email, formGroup) {
		const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		const x = re.test(String(email).toLowerCase());
		if (x) {
			$('#' + formGroup).removeClass('has-error').addClass('has-success');
			return 1
		} else {
			$('#' + formGroup).removeClass('has-success').addClass('has-error');
			return false
		}
	}

	function validatePass(pass, formGroup) {
		const re = /^[~`!@#$%^&*()_+-=a-z0-9]+$/i;
		const x = re.test(String(pass));
		if (x) {
			$('#' + formGroup).removeClass('has-error').addClass('has-success');
			return 1
		} else {
			$('#' + formGroup).removeClass('has-success').addClass('has-error');
			return false
		}
		return false
	}

	function validateAddress(a, b, c) {
		const re = /^[A-Za-z0-9-_, .áéíúóü]+$/g;
		const re1 = /^\d+$/;
		//			const x = re.test(String(a));
		//			const y = re1.test(String(b));
		const x = (a.length > 0);
		const y = (b.length > 0);
		if (x && y) {
			$('#' + c).removeClass('has-error').addClass('has-success');
			return 1
		} else {
			$('#' + c).removeClass('has-success').addClass('has-error');
			return 0
		}
		return false
	}

	function validateDNI(a, c) {
		const re = /^\d+$/;
		const x = re.test(String(a));
		if (x) {
			$('#' + c).removeClass('has-error').addClass('has-success');
			return 1
		} else {
			$('#' + c).removeClass('has-success').addClass('has-error');
			return 0
		}
		return false
	}

	function validateAddressGoogle() {
		const lt = $('#latitud').val();
		const lg = $('#longitud').val();
		if (lt == 0 || lg == 0) {
			alert('debe seleccionar calle, numero y localidad entre las opciones desplegadas. ')
			// $('#signup-direccion').val('');
			$('#fg-signup-direccion').removeClass('has-success').addClass('has-error');
			return 0;
		} else {
			$('#fg-signup-direccion').removeClass('has-error').addClass('has-success');
			return 1;
		}
	}

	function validateText(v, formGroup) {
		const re = /^[A-Za-z0-9-_, .áéíúóü]+$/g;
		//			const x = re.test(String(v));
		const x = (v.length > 0);
		if (x) {
			$('#' + formGroup).removeClass('has-error').addClass('has-success');
			return 1
		} else {
			$('#' + formGroup).removeClass('has-success').addClass('has-error');
			return 0
		}
		return false
	}

	function validateLocalidad(v, formGroup) {
		const re = /^[A-Za-z0-9-_, .]+$/g;
		//			const x = re.test(String(v));
		const x = (v.length > 0);
		if (x) {
			$('#' + formGroup).removeClass('has-error').addClass('has-success');
			return 1
		} else {
			$('#' + formGroup).removeClass('has-success').addClass('has-error');
			return 0
		}
		return false
	}

	function validateConfirm(a, b, fg) {
		if (a == b) {
			$('#' + fg).removeClass('has-error').addClass('has-success');
			return true;
		} else {
			$('#' + fg).removeClass('has-success').addClass('has-error');
			return false;
		}
		return false;
	}

	function validateNotEmpty(v, formGroup) {
		if (v.length > 0) {
			$('#' + formGroup).removeClass('has-error').addClass('has-success');
			return true
		} else {
			$('#' + formGroup).removeClass('has-success').addClass('has-error');
			return false
		}
		return false
	}

	// end validacion de login

	function seleccionaRevendedor() {
		if ($('input[name=revendedor]:checked').val() === undefined) {
			alert('Debes seleccionar un Revendedor');
			return false;
		}
		console.log('radio selected', $('input[name=revendedor]:checked').val())
		enviarPedidoConRevendedor($('input[name=revendedor]:checked').val())
	}

	function enviarPedidoConRevendedor(revId) {
		window.location.href = `carroAccion.php?action=placeOrder&revId=${revId}`;
	}

	function enviarPedidoSinRevendedor() {
		const input = document.querySelector('[name=forma_pago]:checked');
		if (input) {
			window.location.href = `carroAccion.php?action=placeOrder&revId=${0}&forma_pago=${input.value}`;
		} else {
			alert('Seleccione un metodo de pago');
		}
	}

	function enviosCaba() {

		$('#modalTitle').html('Envios C.A.B.A')
		$('#modal-content').html("<img src='img/envios-caba.jpeg' width='100%' alt='Mapa de envios C.A.B.A'>")
		$('#login-signup-modal').modal({
			keyboard: false
		})
	}

	function enviosGba() {
		// $('#ModalCenter').modal()
		$('#modalTitle').html('Envios Gran Buenos Aires')
		$('#modal-content').html("<img src='img/envios-gba.jpeg' width='100%' alt='Mapa de envios GBA'>")
		$('#ModalCenter').modal({
			keyboard: false
		})
	}

	// Init --- 
	$(document).ready(function() {
		if (window.innerWidth <= 800) {
			$('.size-wrapper_prod').removeClass('size-wrapper_prod');
			$('.size-wrapper_catg').removeClass('size-wrapper_catg');
		}

		<?php
		// $log = 'session:'.json_encode($_SESSION);
		// file_put_contents('log/errors.txt', $log, FILE_APPEND);

		?>
		// ******** elementos del header
		<?php
		if (isset($cart) && isset($_SESSION)) {
		?>
			$('#headerCarroCantItems').html(<?php echo (isset($cart)) ? $cart->total_items() : 0; ?>)
			$('#headerCarroTotal').html(<?php echo (Array_key_exists('tipo_usuario_id', $_SESSION) && $_SESSION['tipo_usuario_id'] == 2) ? $cart->total_revendedor() : $cart->total(); ?>)
		<?php
		}
		?>



		$('#search_txt').keypress(function(e) {
			var keycode = (e.keyCode ? e.keyCode : e.which);
			if (keycode == '13') {
				search();
				e.preventDefault();
				return false;
			}
		});
		//*** Modal de Login Mi Cuenta  
		$('#login-signup-modal').on('shown.bs.modal', function(e) {
			// EVITA EL SCROLLING
			document.body.style.overflow = "hidden";
			document.body.style.height = "100%";
		})
		$('#login-signup-modal').on('hidden.bs.modal', function(e) {
			// DEVUELVE EL SCROLLING
			document.body.style.overflow = "auto";
			document.body.style.height = "auto";
		})
		$('#signupModal').click(function() {
			$('#login-modal-content').fadeOut('fast', function() {
				$('#signup-modal-content').fadeIn('fast');
			});
		});
		$('#loginModal').click(function() {
			$('#signup-modal-content').fadeOut('fast', function() {
				$('#login-modal-content').fadeIn('fast');
			});
		});
		$('#FPModal').click(function() {
			$('#login-modal-content').fadeOut('fast', function() {
				$('#forgot-password-modal-content').fadeIn('fast');
			});
		});
		$('#loginModal1').click(function() {
			$('#forgot-password-modal-content').fadeOut('fast', function() {
				$('#login-modal-content').fadeIn('fast');
			});
		});
		$('#signup-modal-content').hide()
		$('#forgot-password-modal-content').hide()

		/*			var input = document.getElementById('signup-direccion');
					var autocomplete = new google.maps.places.Autocomplete(input, {
						types: ['geocode'],
						componentRestrictions: {
							country: "ARG"
						}
					});
					
					google.maps.event.addListener(autocomplete, 'place_changed', function () {
						var near_place = autocomplete.getPlace();
						document.getElementById('latitud').value = near_place.geometry.location.lat();
						document.getElementById('longitud').value = near_place.geometry.location.lng();
					});
		*/

		$('.main-wrapper').append("<div class='alert-container'><div class='custom-alert'>Se agrego al carrito de compras...</div></div>");
		$('.alert-container').hide();




		// TEST DE NOTIFICACION DE PAGOS
		// $.ajax({
		// 	type: "POST",
		// 	url: 'notificacion_pago_mp.php',
		// 	data: JSON.stringify({"action":"payment.created","api_version":"v1","data":{"id":"1232346204"},"date_created":"2020-12-24T13:56:32Z","id":6837579497,"live_mode":false,"type":"payment","user_id":"692348675"}), 
		// 	success: function(data)
		// 	{
		//     	console.log('res',data); // respuesta del servidor
		//         window.location.assign(data);
		// 	}
		// });

	});
	// $( window ).resize(function() {
	// 	if (window.innerWidth <= 800) {
	// 		$('img').removeClass('size-wrapper');
	// 	}else{
	// 		$('img').addClass('size-wrapper');
	// 	}
	// });
</script>