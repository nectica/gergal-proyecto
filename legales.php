<?php 

	require_once 'main_head.php';

	require_once 'header.php'; 

?>

<!-- Main Wrapper Header -->

<div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras"
    data-stellar-background-ratio="0.4">







    <div class="container">



        <div class="row">

            <div class="col-sm-12 columns">

                <div class="page-title">

                    <h1 class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        Términos condiciones</h1>

                </div>



            </div>

        </div>



    </div>



</div>

<!-- /Main Wrapper Header -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>

        <div class="row">

            <div class="col-md-12">

                <p>Se detallan a continuación los términos y condiciones de uso de Gergal S.A. Por cualquier consulta o
                    duda, no dude en contactarnos</p>

                <h5 class="color-black">INTRODUCCION</h5>

                <p>Bienvenido a Gergal, sitio provisto por GERGAL S.A. como un servicio más a nuestros clientes.

                    Por favor, lea las siguientes pautas básicas que rigen el uso del website de Gergal (en adelante,
                    las "Pautas").</p>

                <h5 class="fancy-title">IMPORTANTE</h5>

                <p>El uso de [*] (el "Website") implica la aceptación incondicional e irrevocable de que toda la
                    información y actividades realizadas mediante el Website se encuentran regidas por las Pautas.
                    Asimismo, las Pautas podrán ser cambiadas por GERGAL S.A., en cualquier momento y a exclusivo
                    criterio de ésta, razón por la cual usted deberá revisar las Pautas cada vez que visite el Website.
                    GERGAL S.A. se reserva el derecho de modificar o interrumpir el servicio ofrecido, ya sea en forma
                    permanente o transitoria, sin aviso previo y/o consentimiento de usuarios, en cualquier momento y a
                    exclusivo criterio de ésta.</p>

                <h5 class="fancy-title">USO DEL WEBSITE</h5>

                <p>Mediante el uso del Website el Usuario acepta las Pautas, y declara bajo juramento tener 18 años de
                    edad o más. Si usted es menor de 18 años de edad deberá abstenerse de usar el Website. Los padres,
                    tutores o responsables de los menores de 18 años que utilicen el Website son plena y exclusivamente
                    responsables por el uso del Website por parte de éstos, incluyendo, y sin limitar, cualquier cargo o
                    costo en que se pueda incurrir como consecuencia de tal uso. Si usted no está de acuerdo en todos y
                    cada uno de los términos de las Pautas, absténgase de usar el Website.

                </p>

                <h5 class="fancy-title">POLITICA DE PRIVACIDAD</h5>

                <p>En cumplimiento del artículo 6° de la ley 25.326 (Ley de Protección de Datos Personales) usted (el
                    “Usuario”) toma conocimiento que los datos facilitados a GERGAL S.A. para el uso del Website, son
                    necesarios para establecer la relación contractual conforme se dispone en estos Términos y
                    Condiciones, y serán tratados únicamente para las operaciones relativas al cumplimiento de las
                    Pautas, encuestas, suscripciones, ofertas de servicios, y cualquier otra acción comercial, en total
                    cumplimiento a las normativas aplicables.

                    El Usuario toma conocimiento de la existencia de un Banco de Datos donde se almacena su información
                    personal, en sede de GERGAL S.A., única responsable del mismo, y en ningún caso los datos
                    facilitados serán entregados a terceros, siendo dicha información totalmente confidencial, entre
                    Usuario y [*], salvo que la información le sea requerida a GERGAL S.A. por una resolución judicial o
                    por Autoridad Gubernamental Competente. Toda la información que usted proporciona deberá ser
                    verdadera, exacta y completa.

                    El Usuario es único y exclusivo responsable de la información que brinda mediante el uso del
                    Website. El tal sentido, el Usuario será único y exclusivo responsable respecto de las consecuencias
                    que genere cualquier inexactitud o falsedad de la información brindada.

                    El usuario toma conocimiento de la facultad de ejercer el derecho de acceso a sus datos personales
                    en forma gratuita a intervalos no inferiores a seis meses, salvo que acredite un interés legítimo al
                    efecto, y asimismo que tiene derecho, de ser procedente, a rectificar y/o suprimir dichos datos. La
                    Dirección Nacional de Protección de Datos Personales, es el Órgano de Control de la Ley 25.326, y
                    tiene la atribución de atenderlas denuncias y reclamos que se interpongan con relación al
                    incumplimiento de las normas sobre protección de datos personales (arts. 14, 15 y 16 de la Ley N°
                    25.326, y Disposición N° 10/08 de la D NPDP).</p>

                <h5 class="fancy-title">COMPRA DE PRODUCTOS</h5>

                <p>La compra de productos ofrecidos en el Website se encuentra sujeta a disponibilidad de stock, así
                    como también a condiciones de logística y distribución.

                    En caso de no poder hacerse la entrega del o los productos solicitados debido a la falta de
                    disponibilidad de stock se dará por anulada la compra. Si el cliente desea adquirir un nuevo
                    producto, la compra estará regida por las condiciones comerciales correspondientes al día de la
                    nueva compra. Todas las compras tienen las Condiciones comerciales del día que se realiza la misma,
                    no la fecha de entrega (exceptuando promociones Bancarias donde puede haber excepciones).

                    En caso de que no se pueda procesar el pago online por motivos vinculados a la entidad emisora de la
                    tarjeta de crédito, no se considerará aceptada la compra, y [*] notificará por mail al usuario sobre
                    la imposibilidad de llevar a cabo dicha operación a fin de que se contacte con la entidad emisora de
                    su tarjeta. 

                    Las fotos de los productos son meramente Ilustrativas. Los productos o servicios, características
                    técnicas, precios, disponibilidad, y ofertas contenidas en este sitio pueden variar sin previo aviso
                    y no necesariamente coincidirán con los productos, servicios y precios ofrecidos en los locales.</p>

                <h5 class="fancy-title">PRECIOS</h5>

                <p>Todos los precios incluyen IVA.

                    Los precios publicados correspondientes a los artículos, ofertas y promociones bancarias, serán
                    válidos y se facturarán conforme las condiciones comerciales vigentes al momento en que el usuario
                    confirme la compra respectiva en la Website. Los productos seleccionados antes de las 00hs que
                    permanezcan aún dentro del Carrito sin ser pagados pasarán a tener las condiciones comerciales
                    (precios y descuentos) que rigen en el nuevo día que comienza. En tal sentido, se establece que día
                    de compra corresponde a la fecha que se confirma y cierra el pedido en la Website.

                    En el caso que el medio de pago elegido cuente con un descuento bancario el mismo se aplicara en
                    primer término y luego se complementara el mismo con las ofertas y/o promociones que estén vigentes
                    en el momento de la compra. Luego la entidad bancaria hará el reintegro correspondiente a su
                    tarjeta.</p>

                <h5 class="fancy-title">CONDICIONES DE PAGO</h5>

                <p>Las ofertas y promociones publicadas son válidas para compras por internet abonadas dentro del
                    período de vigencia de las mismas.

                    Usted podrá abonar su compra, siempre dentro del marco seguro proporcionado por el portal. Allí se
                    aceptará como único medio de pago válido por transferencia bancaria o efectivo.

                    Una vez realizado el pedido e ingresado el medio de pago seleccionado, se tomará como fecha de cobro
                    la elegida por el cliente al momento de terminar la compra. Ante la imposibilidad de concretar la
                    operación, y consecuentemente no poder efectuar el pago en la fecha elegida, GERGAL S.A se
                    comunicará con el Cliente - por el medio que estime conveniente, quedando la elección del mismo a su
                    solo criterio, (la “Comunicación”) - a fin de informarle que la operación no ha podido ser
                    procesada.

                    El Cliente reconoce y acepta que en caso de producirse la Comunicación, la operación no se reputará
                    concretada. En dicho caso, si el Cliente desea realizar un nuevo pedido, deberá proceder a generar
                    el mismo nuevamente seleccionando un medio de pago habilitado a tal efecto.

                    GERGAL S.A no asume ninguna responsabilidad por la no concreción de la operación, por impedimentos
                    relacionados con los medios de pagos.

                    Para las compras siguientes, si el cliente decide que otra persona en su nombre reciba el pedido en
                    su domicilio, el cliente deberá dejar explícito quién recibirá el pedido (adulto mayor de 18 años).
                    La persona que recibe el pedido deberá hacerlo con su correspondiente identificación a fin de
                    verificar su identidad (DNI)

                    Ante cualquier duda puede solicitar ayuda llamando al 48381381 de lunes a viernes de 9 a 16hs o por
                    e-mail a info@gergalberries.com

                </p>

                <h5 class="fancy-title">ENTREGAS</h5>

                <p>El cliente podrá conocer el costo que deberá abonar por el envío de su pedido, antes de cerrarlo.
                    Para mayor información sobre zonas de cobertura y/o tipos de servicio disponibles el cliente podrá
                    realizar su consulta llamando al 48381381 de Lunes a Viernes de 9 a 16 hs, o por e-mail
                    info@gergalberries.com

                    El pedido no podrá ser entregado a menores de edad.

                    Además, para su seguridad, en la primera compra se requiere la presencia del titular con el
                    correspondiente DNI. 

                    Es INDISPENSABLE el documento al momento de la entrega ya que el cadete tiene la obligación de
                    solicitarlo. En caso contrario, no podrá realizarse la entrega del pedido.

                    El Usuario debe verificar que el Producto entregado esté completo. La firma del remito y/o la del
                    comprobante de pago implicará declaración de conformidad con la mercadería entregada, sin perjuicio
                    de los derechos que le pudieren corresponder en caso de que la misma resultara por algún motivo
                    defectuosa.

                </p>

                <h5 class="fancy-title">REVOCACIÓN</h5>

                <p>Revocación de la aceptación: Según la Resolución 424/2020 de la Secretaría de Comercio Interior, y de
                    conformidad con los arts. 1110 del Código Civil y Comercial, y 34 de la Ley 24.240, el consumidor
                    por medios electrónicos tiene derecho a revocar la aceptación de su producto dentro de los diez (10)
                    días de formulada la compra o entrega del producto. Este derecho queda exceptuado en el caso de
                    productos perecederos, de conformidad con el artículo 1116 del Código Civil y Comercial, y su
                    aplicación queda sujeta a las posibilidades fácticas de revocación de cada pedido.

                    El cliente debe poner el bien a disposición del vendedor y los gastos de devolución serán conforme
                    lo previsto por la Ley 24.240 y sus modificaciones. La revocación debe ser notificada a
                    info@gergalberries.com por escrito o medios electrónicos o similares, o mediante la devolución de la
                    cosa dentro del plazo de diez días computados desde la fecha de compra o entrega del producto. Esta
                    metodología se complementa en el website, mediante el botón de arrepentimiento.

                </p> 

                <h5 class="fancy-title">INDEMNIDAD</h5>

                <p>El Usuario se obliga en forma expresa e irrevocable a mantener absolutamente indemne a GERGAL S.A,
                    respecto a cualquier reclamo y/o demanda y/o sanción extrajudicial, administrativo o judicial, de
                    cualquier tercero, derivada del uso del Website, o en caso de verificarse la existencia de multas
                    y/o deudas de cualquier tipo generadas por el Usuario o la actividad del mismo realizada en o
                    mediante el Website.

                    AYUDA

                    Por Teléfono al: 48381381 de Lunes a Viernes de 9 a 16hs hs.

                    Por e-mail: info@gergalberries.com

                </p>

                <h5 class="fancy-title">DERECHOS DE MARCA, PUBLICIDAD Y PROPIEDAD INTELECTUAL</h5>

                <p>Todos los elementos, incluidas las imágenes, textos, ilustraciones, íconos, logo e isotipos,
                    fotografías, programas, animaciones, cualquier música, melodía, video clip y cualquier otro elemento
                    que forma parte del Website sólo tiene como destino la comercialización de productos por parte de
                    GERGAL S.A., y se encuentra prohibida cualquier reproducción, modificación o distribución de los
                    mismos.

                    Todos los derechos reservados www.gergalberries.com y www.gergalsa.com son marcas registradas de
                    GERGAL S.A.

                </p>



            </div>

        </div>



        <div class="white-space space-medium"></div>

    </div>

    <!-- /Container -->


</div>

<!-- /Main Container -->



<!-- Container destacado -->

<!-- Fullsize -->

<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->

<!-- /Container -->



<!-- Footer destacado -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->





<!-- login modal -->

<?php include 'loginView.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts.php'?>

<script>
document.title = "Gergal - Contacto";
</script>