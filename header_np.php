<div class="header-wrapper">
	<!-- Header Top Container -->
		<div class="header-top">
			<!-- Container -->
    <div class="container">
    			<!-- /Container -->

		    </div>   
			<!-- /Header Top Container -->

			<!-- Header Main Container -->
			<div class="header-main">

        <!-- Container -->
        <div class="container">
            <!-- Main Navigation & Logo -->                    
			<div class="main-navigation">
            	<div class="row">
                    <!-- Main Navigation -->
                    <div class="col-md-12 columns">
							<nav class="navbar navbar-default gfx-mega nav-left" role="navigation">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-toggle" data-toggle="collapse" data-target="#gfx-collapse"></a>
								<div class="logo">
									<a href="index.php"><img src="img/theme/logo.png" alt="Logo"></a>
								</div>
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="gfx-collapse">
										<ul id='list_navbar' class="nav navbar-nav gfx-nav">
											<!-- <li id='li_home'><a href="index.php">Home</a></li> -->
                            	    <!-- <li id='li_productos'><a href="productos.php">Productos</a></li> -->
                                   
                                    <!-- <li id='li_recetas'><a href="listado-recetas.php" >Recetas</a></li> -->
											<!-- <li id='li_instalaciones'><a href="instalaciones.php">Instalaciones</a></li> -->
                                    <!-- <li id='li_contacto'><a href="contacto.php" >Contacto</a></li> -->
                                    <!-- <li id='li_busqueda' ><div class="input-group" style="width:340px; padding-top:26px">
											<input id="search_txt" class="form-control" type="text" placeholder="Buscar productos..."><span class="input-group-btn"><button onClick=search() class="btn btn-primary" type="button"><i class="fa fa-search"></i></button></span>
											</div>
                        			</li> -->
                        		</ul>
								</div><!-- /.navbar-collapse -->
							</nav>
						</div>
					<!-- /Main Navigation -->
					</div>              
            </div>
         <!-- /Main Navigation & Logo -->
        </div>
		<!-- /Container -->
	</div>   
	<!-- /Header Main Container -->
</div>
