<!DOCTYPE html>
<html lang="es" >
<head>
<meta name="facebook-domain-verification" content="gkm0qlw86d44b61o17h8qipsr79ix9" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5S778QP');</script>
<!-- End Google Tag Manager -->

<meta name="google-site-verification" content="1OpTM222B0QsPVcajhTVeaKXaQcQCrKA_662wUGV1Go" />	
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta charset="utf-8">	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Meta -->
	<meta name="description" content="Gergal" />
	<meta name="keywords" content="Gergal" />
	<meta name="author" content="Gergal">
	<meta name="robots" content="index, follow" />
	<meta name="revisit-after" content="3 days" />
	
	
	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Amatic+SC:wght@400;700&display=swap" rel="stylesheet">

	
	<!-- Styles -->
    	<!-- alert -->
		<link rel="stylesheet" href="css/custom_alert.css">        
		<!-- Bootstrap core CSS -->
  		<link rel="stylesheet" href="css/bootstrap.css">        

		<!-- Bootstrap RTL 
  		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css"> -->

		<!-- Font Awesome 4.1.0 -->
		<link href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

		<!-- Avendor Icons Font -->
		<link href="css/avendor-font-styles.css" rel="stylesheet">

		<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<link rel="stylesheet" type="text/css" href="css/revolutionslider/settings.css" media="screen" />
    	
    	<!-- Animate CSS -->       
    	<link href="css/themecss/animate.css" rel="stylesheet">

    	<!-- Lightbox CSS -->       
    	<link href="css/themecss/lightbox.css" rel="stylesheet">
              
    	<!-- OWL Carousel -->       
    	<link href="css/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="css/owl-carousel/owl.transitions.css" rel="stylesheet">

		<!-- Theme CSS -->
  		<link href="css/avendor.css" rel="stylesheet">
        
		<!-- Color CSS -->
		<link href="css/colors/color-default.css" rel="stylesheet" title="style1">
   
		
		<!-- login Modal CSS -->
		<link href="css/login-modal.css" rel="stylesheet" >
   

	    <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    	<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
    	<link rel="shortcut icon" href="ico/favicon.png">
    
</head>
<body class="bg-pattern1" data-spy="scroll" data-target=".navbar">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5S778QP"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="preloader"></div>
<!-- Page Main Wrapper -->
<div class="page-wrapper" id="page-top">
