<?php 
	require_once 'clases/Receta.php';
	require_once 'clases/Producto.php';
	require_once 'clases/CarritoCompras.php';
	$cart = new CarritoCompras	;
	require_once 'main_head.php';
	require_once 'header.php'; 	$prod = new Producto(); 
?>
<!-- Main Wrapper Header -->
<div class="main-wrapper-header fancy-header dark-header parallax parallax-<?php echo (isset($_REQUEST['categoria']) && !empty($_REQUEST['categoria'])? $_REQUEST['categoria']:'productos'); ?>"
    data-stellar-background-ratio="0.4">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 columns">
                <div class="page-title">
                    <h1 id="titulo" class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        <?php echo $prod->getCatNom((isset($_REQUEST['categoria']) && !empty($_REQUEST['categoria']))? $_REQUEST['categoria']:0); ?>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Main Wrapper Header -->
<!-- Main Container -->
<div class="main-wrapper">
    <!-- Container -->
    <div class="container content-with-sidebar">
        <div class="row">
            <!-- Content -->
            <div class="col-sm-8 col-sm-push-4 col-md-9 col-md-push-3 columns">
                <div class="white-space space-big"></div>
                <!-- filters -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="shop-filters">
                            <form>
                                <select id="orderSelection" class="form-control ordering" onChange='selectOrder()'>
                                    <option value="">Ordenar Por</option>
                                    <option value="ventas">Ordenar por mas vendidos</option>
                                    <option value="novedad">Ordenar por novedad</option>
                                    <option value="precio-asc">Ordenar por precio: menor a mayor</option>
                                    <option value="precio-desc">Ordenar por precio: mayor a menor</option>
                                </select>
                                <label id='results' class="show-results"></label>
                            </form>
                        </div>
                        <div class="white-space space-small"></div>
                    </div>
                </div>
                <!-- listado productos -->
                <div class="row" id='prods_container'>
                    <?php 
							$filas= 2;
							$cols = 3;
							$prodsPerPage = 9;
							$offset = 0;
							$currPage=1;
							$cat=0;
							$sanitizedSearch = '';
							$order = 'ventas';
							// HANDLER DE ORDER SELECTOR
							if(isset($_REQUEST['order']) && $_REQUEST['order'] != ''){
								$order = $_REQUEST['order'];
							}

							// HANDLER DE PAGINADO
							if(isset($_REQUEST['page']) && $_REQUEST['page'] > 0){
								$offset = $prodsPerPage * (intval($_REQUEST['page'])-1);
								$currPage = intval($_REQUEST['page']); 	
							}
							
							// RESPUESTA PARA SEARCH BOX
							if(isset($_REQUEST['search']) && !empty($_REQUEST['search'])){
								$sanitizedSearch = preg_replace("/[^a-zA-Z0-9-ñÑáéíóúÁÉÍÓÚ]/", "", $_REQUEST['search']);
								//$prod = new Producto(); 
								echo $prod->listado($filas,$cols,$offset,$prodsPerPage,$cat,$sanitizedSearch,$order)['rows'];
							}else{
								// LISTADO DE PRODUCTOS POR CATEGORIA
								// PARAMS  $row=3,$cols=4,$offset=0,$limit=4,$categorias=[],search=''
 								$cat = (isset($_REQUEST['categoria']) && !empty($_REQUEST['categoria']))? $_REQUEST['categoria']:0;
								//$prod = new Producto(); 
								echo $prod->listado($filas,$cols,$offset,$prodsPerPage,$cat,$sanitizedSearch,$order)['rows'];
							}
							
						?>
                </div>
                <!-- Pagination -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <ul class="pagination">
                                <?php 
											$listadoProds = $prod->listado($filas,$cols,$offset,$prodsPerPage,$cat,$sanitizedSearch);
											$desde = (array_key_exists('desde',$listadoProds))?$listadoProds['desde']:0;
											$hasta = (array_key_exists('hasta',$listadoProds))?$listadoProds['hasta']:0;
											$totalProds = (array_key_exists('totalProds',$listadoProds))?$listadoProds['totalProds']:0;
											if($totalProds > 1 && $totalProds > $prodsPerPage ){
												$cantPages = $totalProds / $prodsPerPage;
												$last = $currPage - 1;
												if($last > 0){
													echo "<li><a href={$_SERVER['PHP_SELF']}?categoria={$cat}&search={$sanitizedSearch}&page={$last}&order={$order}><i class='fa fa-angle-left'></i></a></li>";
												}
												echo "<li id='pbot_1'><a href={$_SERVER['PHP_SELF']}?categoria={$cat}&search={$sanitizedSearch}&page=1&order={$order}>1</a></li>";
												for ($i=1; $i < $cantPages ; $i++) { 
													$p = $i+1;
													echo "<li id='pbot_{$p}'><a href={$_SERVER['PHP_SELF']}?categoria={$cat}&search={$sanitizedSearch}&page={$p}&order={$order}>$p</a></li>";
												}
												$next = $currPage+1;
												if($next < $cantPages){
													echo "<li><a href={$_SERVER['PHP_SELF']}?categoria={$cat}&search={$sanitizedSearch}&page={$next}&order={$order}><i class='fa fa-angle-right'></i></a></li>";
												}
												
											}
										?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /Pagination -->

                <div class="white-space space-medium"></div>
            </div>
            <!-- /Content -->

            <!-- Sidebar -->
            <div class="col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9 columns sidebar sidebar-left">
                <div class="white-space space-big"></div>
                <div class="sidebar-content">
                    <div class="sidebar-widget">
                        <h4 class="title-widget fancy-title"><span>Categorías</span></h4>
                        <ul class="shop-product-categories">
                            <?php
										$c = $prod->getCategorias();
										if(count($c) > 0){
											foreach($c as $item){
									?>
                            <li class="active"><a
                                    href='listado-productos.php?categoria=<?php echo $item['id']?>'><?php echo $item['nombre']?></a><span
                                    class="count">( <?php echo $item['count']?> )</span></li>
                            <?php } } ?>
                        </ul>
                    </div>
                    <div class="sidebar-widget">
                        <h4 class="title-widget fancy-title"><span>Los más vendidos</span></h4>
                        <ul class="recent-post">
                            <?php
										$mv = $prod->getMasVendidos();
										if(count($mv) > 0){
											foreach($mv as $mvItem){
									?>
                            <li>
                                <a href='detalle-producto.php?prod=<?php echo $mvItem['producto_id'] ?>'><img
                                        src='<?php echo '/gergal-abm/'.$mvItem['image_path']?>'
                                        alt=""><?php echo $mvItem['producto_nombre']?></a>
                                <?php if(array_key_exists('tipo_usuario_id',$_SESSION) && $_SESSION['tipo_usuario_id'] == 2){
										echo "<p style='margin-bottom:6px; margin-top:6px; line-height:0px; text-decoration:line-through; color:#666666; font-weight:400; font-size:13px'>$ {$mvItem['producto_precio_unitario']} </p>
										 <p class='product-price'>$ {$mvItem['producto_precio_revendedor']}</p></li>";
										}
										else{
											echo "<p class='product-price'>$ {$mvItem['producto_precio_unitario']}</p>";
										}		
									?>

                            </li>
                            <?php } } ?>
                        </ul>
                    </div>
                </div>
                <div class="white-space space-big"></div>
            </div>
            <!-- /Sidebar -->

        </div>
    </div>
    <!-- /Container -->


    <!-- Parallax -->
    <?php include("parallax_gergal.php"); ?>
    <!-- /Parallax -->




    <div class="container">
        <div class="white-space space-big"></div>
        <div class="row">
            <div class="col-md-12">
                <h4 class="fancy-title text-left"><span><strong>Novedades</strong></span></h4>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- Carousel -->
                <div class="carousel-box">
                    <div class="carousel carousel-nav-out carousel-simple" data-carousel-autoplay="false"
                        data-carousel-items="4" data-carousel-nav="true" data-carousel-pagination="false"
                        data-carousel-speed="1000">
                        <?php echo $prod->carrousel_items(6);?>
                    </div>
                </div>
                <!-- /Carousel -->
                <div class="white-space space-big"></div>
            </div>
        </div>

    </div>






</div>
<!-- /Main Container -->

<!-- Footer Container -->
<?php include("footer.php"); ?>
<!-- /Footer Container -->

</div>

<!-- Back To Top -->
<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>
<!-- /Back To Top -->


<!-- login modal -->
<?php include 'loginView.php'?>

<!-- scripts del template -->
<?php include 'theme_scripts.php'?>
<!-- custom scripts -->
<?php include 'custom_scripts.php'?>
<script>
document.title = "Gergal - Listado de productos";
</script>
</body>

</html>