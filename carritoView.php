<?php

require_once 'clases/Receta.php';

require_once 'clases/Producto.php';

require_once 'clases/CarritoCompras.php';

$cart = new CarritoCompras;

require_once 'main_head.php';

require_once 'header.php';

?>

<!-- Main Wrapper Header -->

<div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras" data-stellar-background-ratio="0.4">

    <div class="container">



        <div class="row">

            <div class="col-sm-12 columns">

                <div class="page-title">

                    <h1 class="script-font" style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        Carrito de Compras</h1>

                </div>



            </div>

        </div>



    </div>



</div>

<!-- /Main Wrapper Header -->

<!-- /Main Wrapper Header -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>



        <div class="row">

            <div class="col-md-9 table-responsive" style="padding-right: 8px; padding-left: 8px; border: none;">

                <table class="table cart-table table-hover">

                    <thead>

                        <tr>
                            <th>Producto</th>

                            <th>Precio</th>

                            <th>Cantidad</th>

                            <th>Total</th>

                            <th></th>

                            <th></th>
                            <?php
                            if (isset($_SESSION['sin_stock']) && count($_SESSION['sin_stock']) >  0) { ?>
                                <th>stock disponible </th>

                            <?php }
                            ?>

                        </tr>

                    </thead>

                    <tbody>

                        <?php

                        if ($cart->total_items() > 0) {

                            //get cart items from session

                            $cartItems = $cart->contents();

                            foreach ($cartItems as $item) {

                                if (is_array($item)) {



                        ?>

                                    <tr class="<?php echo isset($_SESSION['sin_stock'][$item['id']])  ? 'bg-color-warning' : '' ?>">



                                        <td style="white-space: pre-wrap;"><a href="detalle-producto.php?prod=<?php echo $item['id']; ?>" class="color-verde"><?php echo $item["name"]; ?></a></td>

                                        <td>$ <?php echo $item["price"]; ?></td>

                                        <td><input class="qty input-lg" style="width:60px; padding: 10px 10px; font-size: 15px;" type="number" min='0' max=999 value="<?php echo $item["qty"]; ?>" name="quantity" onchange="updateCartItem(this, '<?php echo $item['rowid']; ?>')"></td>

                                        <td>$<?php echo $item["subtotal"]; ?></td>

                                        <td class="td-remove"><a href="carroAccion.php?action=removeCartItem&id=<?php echo $item["rowid"]; ?>">
                                                <div class="icon-wrapper icon-border-round fa-lg" onclick="return confirm('Desea elminimar el producto ?')"><i class="fa fa-trash-o"></i></div>
                                            </a></td>

                                        <td class="td-cart-image"><img src="/gergal-abm/<?php echo $item["image_path"]; ?>" alt="Product"></td>
                                        <?php
                                        if (isset($_SESSION['sin_stock']) && count($_SESSION['sin_stock']) >  0) { ?>
                                            <td><?php echo isset($_SESSION['sin_stock'][$item['id']])  ? $_SESSION['sin_stock'][$item['id']] : 'Disponible' ?> </td>

                                        <?php }
                                        ?>

                                    </tr>

                            <?php }
                            }
                        } else { ?>

                            <tr>
                                <td colspan="5">
                                    <p>El carrito de compras esta vacio...</p>
                                </td>

                            <?php } ?>

                    </tbody>

                    <tfoot>

                        <tr>

                            <td colspan="10" style="white-space: pre-wrap;"><a href="index.php" class="btn btn-primary" style="margin:2px;">Seguir
                                    comprando <i class="fa fa-angle-right fa-margin-left"></i></a> <a href="carritoView.php" class="btn btn-primary " style="margin:2px;">Actualizar
                                    carrito <i class="fa fa-angle-right fa-margin-left"></i></a> <a href="carroAccion.php?action=emptyCart" class="btn btn-primary" style="margin:2px;">Vaciar carrito <i class="fa fa-angle-right fa-margin-left"></i></a></td>

                        </tr>

                    </tfoot>

                </table>

            </div>



            <div class="col-md-3">

                <h4 class="title-widget fancy-title"><span>Detalle de compra</span></h4>

                <table class="table">

                    <tbody>

                        <tr>

                            <td><strong>Precio de lista</strong></td>

                            <td class="text-right"><?php echo '$ ' . $cart->total(); ?></td>

                        </tr>

                        <?php if (array_key_exists('tipo_usuario_id', $_SESSION) &&  $_SESSION['tipo_usuario_id'] == 2) {

                            $pr = $cart->total_revendedor();

                            echo "<tr><td><strong>Precio revendedor</strong></td><td class='text-right'>$ {$pr}</td></tr>";
                        }





                        ?>

                    </tbody>

                    <tfoot>

                        <tr>

                            <td><strong>Envío</strong></td>

                            <td class="text-right">Gratis</td>

                        </tr>

                        <tr>

                            <td><strong>Total Pedido</strong></td>

                            <td>
                                <h4 class="text-right"><?php

                                                        $ct = $cart->total();

                                                        if (array_key_exists('tipo_usuario_id', $_SESSION) &&  $_SESSION['tipo_usuario_id'] == 2) {

                                                            $ct = $cart->total_revendedor();
                                                        }

                                                        echo '$' . $ct; ?></h4>
                            </td>

                        </tr>

                    </tfoot>

                </table>

                <a <?php



                    $tipousuario = (array_key_exists('tipo_usuario_id', $_SESSION) &&  $_SESSION['tipo_usuario_id'] == 2) ? 'Revendedor' : 'Particular';

                    $mcArray = $cart->get_minimo_compra();

                    $mc = intval($mcArray[$tipousuario]);



                    if (isset($_SESSION['loggedInId'])) {



                        if (intval($ct) > $mc) {

                            $checkout_btn_state = '';
                        } else {

                            $checkout_btn_state = 'disabled';
                        }

                        echo "href='checkout.php'";
                    } else {



                        if (intval($ct) > $mc) {

                            $checkout_btn_state = 'xx';
                        } else {

                            $checkout_btn_state = 'disabled';
                        }

                        echo "data-toggle='modal' data-target='#login-signup-modal'";
                    }

                    ?> id='checkout_btn' class="btn btn-xs btn-primary <?php echo $checkout_btn_state ?>">Checkout <i class="fa fa-angle-right fa-margin-left"></i></a> <a href="index.php" class="btn btn-xs btn-primary">Seguir comprando <i class="fa fa-angle-right fa-margin-left"></i></a>

                <?php if (intval($ct) < $mc) {

                    echo "<div class='col'>El minimo de compra es $" . $mc . "</div>";
                }

                ?>

            </div>

        </div>



        <div class="white-space space-big"></div>

    </div>

    <!-- /Container -->



</div>

<!-- /Main Container -->



<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->



</div>

<!-- /Main Container -->



<!-- Footer Container -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->



<!-- login modal -->

<?php include 'loginView.php' ?>



<!-- scripts del template -->

<?php include 'theme_scripts.php' ?>

<!-- custom scripts -->

<?php include 'custom_scripts.php' ?>

<script>
    document.title = "Gergal - Carrito de Compras";
    window.onload = function(){
        $alert_stock = <?= isset($_SESSION['sin_stock']) ?>;
        if ($alert_stock) {
            alert('Alerta por favor cambie y/o elimine los productos señalados, no hay stock suficiente para completar su orden con algun(os) producto(s).');
        }
    }
</script>

</body>

</html>