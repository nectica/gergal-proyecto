<?php 
	require_once 'clases/Receta.php';
	require_once 'clases/Producto.php';
	require_once 'clases/CarritoCompras.php';
	$cart = new CarritoCompras	;
	require_once 'main_head.php';
	require_once 'header.php'; 
	$p = new Producto;?>
<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras" data-stellar-background-ratio="0.4">
			<div class="container">
            	<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h1 id="titulo" class="script-font" style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">PRODUCTOS</h1>                    
                    	</div>
                    </div>
				</div>
            </div>
        </div>      
		<!-- /Main Wrapper Header -->
		<!-- Main Container -->
		<div class="main-wrapper">
			<div class="fullsize">
            	<div class="white-space space-medium"></div>
    	        	<div class="container">
					<div class="white-space space-big"></div>
                
            	<div class="row">
  					<div class="col-md-12 columns">
                    	<h1 class="fancy-title script-font text-center color-verde"><span><strong>Frutas  y Vegetales</strong> congelados Gergal </span></h1>
                    </div>
                    <div class="col-md-10 col-md-offset-1">
                    	<p class="lead text-center"><strong>Frutas y vegetales de primera calidad cosechadas en su punto óptimo de maduración.
						Una vez cosechadas, se lavan,  clasifican, y se congelan usando tecnología IQF (Individual Quick Frozen). Esta tecnología permite que la fruta y la verdura se congele muy rápidamente después de la cosecha conservando así propiedades y nutrientes.</strong><br>
						<br>
						Usar frutas y vegetales congelados Gergal te garantiza productos de primera calidad todo el año ahorrando tiempo y dinero ya que dependiendo del alimento, no tenes que ni cortar,ni lavar, ni pelar y como es muy fácil separar en porciones, usas lo que necesitas sin desperdico.<br><br>
						Las frutas y vegetales congelados IQF tienen la ventaja de que al congelarse rápidamente después de la cosecha, conservan la mayor parte de los nutrientes y vitaminas haciendo que tanto su color como su sabor sean mas ricos que muchos de los productos frescos ya que se congelan en plantas de proceso cercanas a los campos de producción,  por lo que no pierden sus propiedades y vitaminas en largos viajes hasta los puntos de distribucion y exhibición en góndola como los productos frescos
						<br><br>
						<strong class="color-verde">• Frutas y vegetales 100% naturales sin ningún agregado de conservantes ni colorantes
						<br>
						• Lavadas, listas para usar.
						<br>
						• Peladas y cortadas (dependiendo el producto)
						<br>
						• Duran hasta 24 meses manteniendo la cadena de frío
						<br>
						• Frutas y verduras todo el año.</strong> <br>
						<br>
						<strong>Cómo usarlos</strong><br>
						Las frutas IQF están listas para usar, podés descongelar en heladera o llevarlos directo a cocinar en budines, licuados, tortas, granolas o solas ¡Como más te guste! Los vegetales congelados IQF están listos para usar, limpios y cortados, no hace falta descongelarlos previamente, van directo a cocinarse, al vapor, hervidos o saltados, ¡preparalos como más te guste!<br>
						</p>
                        <div class="white-space space-small"></div>
                    </div>
				</div> 
                
				<div class="white-space space-medium"></div>   
                <hr>
                <div class="row">
                	<div class="col-md-6 col-sm-12">
						<h3 class="text-center color-verde script-font"><strong>Jugos Naturales</strong></h3>
                        <h5 class="text-center"><p>Hacemos jugos naturales en nuestra propia planta de elaboración utilizando las mismas frutas y vegetales que distribuimos. Contamos con dos líneas de jugos, en botella de vidrio 100% naturales y en botella de plástico. Todos con mucha fruta y verdura para que te mantengas fuerte y saludable y puedas incorporar nutrientes esenciales a donde vayas ¡Elegí el que más te guste!</p>
						</h5>
						<div class="white-space space-big"></div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                    	<h3 class="text-center color-verde script-font"><strong>Lafem tienda saludable</strong></h3>
                        <h5 class="text-center"><p>¡Nuestra línea de dietética para sumar a tu carrito! Para completar tu compra podés pasar por la tienda saludable y llevarte esos productos que mantendrán a vos y tu familia fuerte y saludable. Opciones de platos listos que te resuelven y todo lo necesario para que llevemos a tu casa una compra variada y práctica.</p>
						</h5>
						
                    </div>
                </div>
                <hr>
			</div>
		</div>
		<!-- categorias Productos -->
		<div class="fullsize">
            <div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-12 columns">
                    		<h2 class="fancy-title script-font text-center color-verde"><span>Categorías </span></h2>
                    	</div>
						<div class="row">
							<?php 
								//$p = new Producto;
								echo $p->getCategoriasScreenObjects();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- preguntas frecuentes -->
        <div class="main-wrapper">
			<div class="fullsize">
				<!-- <div class="white-space space-big"></div> -->
            		<div class="row">
						<div class="col-md-12 columns">
							<h1 class="fancy-title script-font text-center color-verde"><span>Preguntas Frecuentes</span></h1>
						</div>
						<div class="col-md-10 col-md-offset-1">
							<div class="accordion panel-group" id="accordion2">
								<div class="panel panel-default">
									<div class="panel-heading bg-color-white">
										<h5 class="panel-title"><a class="accordion-toggle collapsed-icon" data-toggle="collapse" href="#collapse1"><span class="icon gfx-question-1 iconleft"></span>¿Cómo hago la compra? ¿Hay un mínimo?</a></h5>
									</div>
									<div id="collapse1" class="panel-collapse collapse in">
										<div class="panel-body">Se hace de forma online y te enviamos los productos. El mínimo de compra es $2500</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading bg-color-white">
										<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse2"><span class="icon gfx-question-1 iconleft"></span>¿Cuál es la zona de cobertura? ¿Tiene costo de envío?</a></h5>
									</div>
									<div id="collapse2" class="panel-collapse collapse">
										<div class="panel-body">Distribuimos en CABA y Gran Buenos Aires, llegando a La Plata y no tiene costo de envío.</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading bg-color-white">
										<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse3"><span class="icon gfx-question-1 iconleft"></span>¿Cuánto tiempo tardan en llegar los productos a mi casa?</a></h5>
									</div>
									<div id="collapse3" class="panel-collapse collapse">
										<div class="panel-body">Una vez hecho el pedido por la página tenés que responder el mail de confirmación de compra combinando la entrega. Se estima que llegaremos entre 4 y 6 días hábiles. 
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading bg-color-white">
										<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse4"><span class="icon gfx-question-1 iconleft"></span>¿Cómo se distribuyen los productos?</a></h5>
									</div>
									<div id="collapse4" class="panel-collapse collapse">
										<div class="panel-body">Van en camiones con cámara de frío, especiales para congelados.
										</div>
									</div>
								</div>

								<div class="panel panel-default">
									<div class="panel-heading bg-color-white">
										<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse6"><span class="icon gfx-question-1 iconleft"></span>¿Puedo comprar por menos de $2500?</a></h5>
									</div>
									<div id="collapse6" class="panel-collapse collapse">
										<div class="panel-body">No es posible.</div>
									</div>
								</div>

												<div class="panel panel-default">
													<div class="panel-heading bg-color-white">
														<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse7"><span class="icon gfx-question-1 iconleft"></span>¿Cuáles son los métodos de pago?</a></h5>
													</div>
													<div id="collapse7" class="panel-collapse collapse">
														<div class="panel-body">Se paga en efectivo al recibir los productos o por transferencia bancaria. Una vez hecho el pedido por la página por favor responder el mail de confirmación de compra combinando la entrega y en caso de abonar por transferencia bancaria enviar adjunto el comprobante.</div>
													</div>
												</div>
	
												<div class="panel panel-default">
													<div class="panel-heading bg-color-white">
														<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse8"><span class="icon gfx-question-1 iconleft"></span>¿Cómo tengo que hacer para distribuir sus productos en el interior?</a></h5>
													</div>
													<div id="collapse8" class="panel-collapse collapse">
														<div class="panel-body">¡Queremos que nuestros productos se distribuyan por todo el país! Para esto tenés que estar dado de alta en algún transporte de congelados a tu zona. Con este dato te pedimos que mandes un mail a <a href="/contacto.php">equipogergal@gmail.com</a> diciendo de donde sos y cual es el transporte a tu zona. Desde ese mail te mandan toda la info para avanzar. </div>
													</div>
												</div>

												
												

												<div class="panel panel-default">
													<div class="panel-heading bg-color-white">
														<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse9"><span class="icon gfx-question-1 iconleft"></span>¿Qué vencimiento tienen los productos?</a></h5>
													</div>
													<div id="collapse9" class="panel-collapse collapse">
														<div class="panel-body">Las frutas y verduras congeladas duran hasta 2 años y el vencimiento lo ves en la etiqueta. Los jugos entre 3 y 6 meses. </div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="panel-heading bg-color-white">
														<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse10"><span class="icon gfx-question-1 iconleft"></span>¿Cómo hago una compra mayorista?</a></h5>
													</div>
													<div id="collapse10" class="panel-collapse collapse">
														<div class="panel-body">La compra mayorista es a partir de 15kgs y viene en bolsas de 2.5kgs. Para info de precios y condiciones de venta enviar un mail a <a href='/contacto.php'>equipogergal@gmail.com</a> aclarando de que zona sos. </div>
													</div>
												</div>
												<div class="panel panel-default">
													<div class="panel-heading bg-color-white">
														<h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed" data-toggle="collapse" href="#collapse11"><span class="icon gfx-question-1 iconleft"></span>¿Cómo hago para ser revendedor/a?</a></h5>
													</div>
													<div id="collapse11" class="panel-collapse collapse">
														<div class="panel-body">Si sos de CABA y Gran Buenos Aires y contas con CUIT o CUIL enviá un mail a <a href="/contacto.php">equipogergal@gmail.com</a> que te mandan toda la info. 
</div>
													</div>
												</div>

											</div>
							
							
							
							<div class="white-space space-medium"></div>
						</div>
				</div>                      
               
            <!-- Parallax -->                            
			 <?php include("parallax_gergal.php"); ?>
			<!-- /Parallax -->
              
                                   
		</div>
				
			</div>
		</div>
		<!-- /Main Container -->      

		<!-- Footer Container -->
		<?php include("footer.php"); ?>
		<!-- /Footer Container -->

	</div>	

	<!-- Back To Top -->
	<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>
	<!-- /Back To Top -->


	<!-- login modal -->
	<?php include 'loginView.php'?>

	<!-- scripts del template -->
	<?php include 'theme_scripts.php'?>
	<!-- custom scripts -->
	<?php include 'custom_scripts.php'?>
	<script>
		document.title = "Gergal - Listado de productos" ;
	</script>
	</body>
</html>