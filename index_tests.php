<?php 

	

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	$cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header.php'; 

	

?>



<div class="container">

    <div class="row"></div>
</div>

<!-- Slider Container -->

<div class="slider-wrapper">

    <!-- START REVOLUTION SLIDER 3.1 rev5 fullwidth mode -->

    <div class="tp-banner-container">

        <div class="tp-banner">

            <ul>



                <!-- SLIDE 1 -->

                <li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000"
                    data-thumb="img/demo/slider/slide1.jpg" data-fstransition="fade" data-fsmasterspeed="1000"
                    data-fsslotamount="7" data-saveperformance="off" data-title="Frutas y Vegetales Congelados">

                    <!-- MAIN IMAGE -->

                    <img src="img/demo/slider/slide1.jpg" alt="" data-bgposition="center center" data-bgfit="cover"
                        data-bgrepeat="no-repeat">

                    <!-- LAYERS -->



                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="200" data-splitin="words" data-elementdelay="0.1" data-start="1250"
                        data-speed="1500" data-easing="Power3.easeOut"
                        data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-splitout="lines" data-endelementdelay="0"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-endspeed="1500" data-endeasing="Power3.easeInOut" data-captionhidden="on"
                        style="font-size:130px;line-height:110px;text-shadow: 2px 2px 2px #333333;">Frutas y Vegetales
                        Congelados

                    </div>

                    <div class="white-space space-small"></div>

                    <!-- link productos frutas -->

                    <div class="tp-caption sfb rs-parallaxlevel-5" data-x="center" data-y="380" data-speed="800"
                        data-start="2600" data-easing="Power4.easeOut" data-endspeed="300"
                        data-endeasing="Power1.easeIn" data-captionhidden="off" style="z-index: 6">

                        <a href="listado-productos.php?categoria=10" class="btn btn-primary btn-lg">Ver productos </a>

                    </div>



                </li>

                <!-- SLIDE  -->





                <!-- SLIDE 2 -->



                <li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000"
                    data-thumb="img/demo/slider/slide2.jpg" data-fstransition="fade" data-fsmasterspeed="1000"
                    data-fsslotamount="7" data-saveperformance="off" data-title="Verduras Congeladas">

                    <!-- MAIN IMAGE -->

                    <img src="img/demo/slider/slide2.jpg" alt="Verduras Congeladas" data-bgposition="center center"
                        data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!-- LAYERS -->



                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="200" data-splitin="words" data-elementdelay="0.1" data-start="1250"
                        data-speed="1500" data-easing="Power3.easeOut"
                        data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-splitout="lines" data-endelementdelay="0"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-endspeed="1500" data-endeasing="Power3.easeInOut" data-captionhidden="on"
                        style="font-size:130px;line-height:110px;text-shadow: 2px 2px 2px #333333;">Verduras Congeladas

                    </div>

                    <div class="white-space space-small"></div>

                    <!-- Link productos verduras -->

                    <div class="tp-caption sfb rs-parallaxlevel-5" data-x="center" data-y="380" data-speed="800"
                        data-start="2600" data-easing="Power4.easeOut" data-endspeed="300"
                        data-endeasing="Power1.easeIn" data-captionhidden="off" style="z-index: 6">

                        <a href="listado-productos.php?categoria=18" class="btn btn-primary btn-lg">Ver productos </a>

                    </div>



                </li>



                <!-- SLIDE  -->

                <!-- SLIDE 3 -->

                <li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000"
                    data-thumb="img/demo/slider/slide3.jpg" data-fstransition="fade" data-fsmasterspeed="1000"
                    data-fsslotamount="7" data-saveperformance="off" data-title="Jugos naturales">

                    <!-- MAIN IMAGE -->

                    <img src="img/demo/slider/slide3.jpg" alt="Jugos naturales" data-bgposition="center center"
                        data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!-- LAYERS -->



                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="200" data-splitin="words" data-elementdelay="0.1" data-start="1250"
                        data-speed="1500" data-easing="Power3.easeOut"
                        data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-splitout="lines" data-endelementdelay="0"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-endspeed="1500" data-endeasing="Power3.easeInOut" data-captionhidden="on"
                        style="font-size:130px;line-height:110px;text-shadow: 2px 2px 2px #333333;">Jugos Naturales

                    </div>

                    <div class="white-space space-small"></div>

                    <!-- Link productos jugos -->

                    <div class="tp-caption sfb rs-parallaxlevel-5" data-x="center" data-y="380" data-speed="800"
                        data-start="2600" data-easing="Power4.easeOut" data-endspeed="300"
                        data-endeasing="Power1.easeIn" data-captionhidden="off" style="z-index: 6">

                        <a href="listado-productos.php?categoria=17" class="btn btn-primary btn-lg">Ver productos </a>

                    </div>



                </li>

                <!-- SLIDE  -->



                <!-- SLIDE 4 -->

                <li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000"
                    data-thumb="img/demo/slider/slide4.jpg" data-fstransition="fade" data-fsmasterspeed="1000"
                    data-fsslotamount="7" data-saveperformance="off" data-title="Tienda Lafem">

                    <!-- MAIN IMAGE -->

                    <img src="img/demo/slider/slide4.jpg" alt="Tienda Lafem" data-bgposition="center center"
                        data-bgfit="cover" data-bgrepeat="no-repeat">

                    <!-- LAYERS -->



                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="200" data-splitin="words" data-elementdelay="0.1" data-start="1250"
                        data-speed="1500" data-easing="Power3.easeOut"
                        data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-splitout="lines" data-endelementdelay="0"
                        data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-endspeed="1500" data-endeasing="Power3.easeInOut" data-captionhidden="on"
                        style="font-size:130px;line-height:110px;text-shadow: 2px 2px 2px #333333;">Lafem, tienda
                        saludable

                    </div>

                    <div class="white-space space-small"></div>

                    <!-- Link productos lafem -->

                    <div class="tp-caption sfb rs-parallaxlevel-5" data-x="center" data-y="380" data-speed="800"
                        data-start="2600" data-easing="Power4.easeOut" data-endspeed="300"
                        data-endeasing="Power1.easeIn" data-captionhidden="off" style="z-index: 6">

                        <a href="listado-productos.php?categoria=11" class="btn btn-primary btn-lg">Ver productos </a>

                    </div>



                </li>

                <!-- SLIDE  -->



                <div class="tp-bannertimer tp-bottom"></div>



            </ul>

        </div>

    </div>



    <!-- END REVOLUTION SLIDER -->

</div>

<!-- /Slider Container -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>



        <div class="row">

            <div class="col-md-12 columns">

                <h1 class="fancy-title script-font text-center color-verde"><span>Hacia lo
                        <strong>Natural</strong></span></h1>

            </div>

            <div class="col-md-10 col-md-offset-1">

                <p class="lead text-center">Desde 1983 nos dedicamos a la producción y comercialización de berries
                    (frutos rojos) frescos y congelados en la Argentina y el mundo. Somos pioneros en la producción en
                    la provincia de Buenos Aires de frambuesas y moras. Siguiendo nuestro lema de “Hacia lo natural”,
                    fuimos incorporando otras frutas y verduras frescas y congeladas y jugos naturales elaborados con
                    nuestra propia fruta en nuestra propia planta de producción. Logrando así una amplia gama de
                    productos sanos y naturales para abastecer diferentes mercados y clientes.<br>

                    <strong>¡Sumate a nuestro equipo!</strong>
                </p>

                <div class="white-space space-small"></div>

            </div>

        </div>



        <!-- REVENDEDOR -->

        <div class="row">

            <div class="col-sm-12">

                <div class="text-center">

                    <a href="revendedores.php" class="btn btn-primary btn-lg">Quiero ser revendedor</a>

                </div>

            </div>



        </div>

        <div class="white-space space-medium"></div>

        <!-- /REVENDEDOR -->

    </div>

    <!-- /Container -->



    <!-- Fullsize -->

    <div class="fullsize bg-color-light">

        <div class="white-space space-medium"></div>

        <div class="container">

            <div class="row">



                <!-- Content -->

                <div class="col-md-12">



                    <div class="row">

                        <div id="destacados" class="col-md-12">

                            <h4 class="fancy-title text-left"><span><strong>Productos destacados</strong></span></h4>



                        </div>

                    </div>

                    <!-- 3 filas de 4 productos  -->

                    <div class="row">

                        <?php 

							// PARAMS DE LISTADO DE PRODUCTOS

							// $row=3,$cols=4,$offset=0,$limit=4,$categorias=[]

							$prod = new Producto();

							echo $prod->listado()['rows'];

						?>



                    </div>

                    <!-- Pagination -->

                    <div class="row">

                        <div class="col-sm-12">

                            <div class="text-center">

                                <a href="listado-productos.php" class="btn btn-verde btn-md">Ver todos los productos</a>

                            </div>

                        </div>

                    </div>

                    <!-- /Pagination -->



                    <div class="white-space space-medium"></div>

                </div>

                <!-- /Content -->



            </div>

        </div>

    </div>

    <!-- /Fullsize -->

    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>



        <div class="row">

            <div class="col-md-12 columns">

                <h1 class="fancy-title script-font text-center color-verde"><span>¿Qué Estás Buscando?</span></h1>

            </div>

            <div class="col-md-10 col-md-offset-1">

                <p class="lead text-center">Actualmente distribuimos nuestros productos en toda la República Argentina y
                    exportamos a Estados Unidos, Canadá, Uruguay, Chile, Europa, Asia y Oceanía.</p>

                <div class="white-space space-medium"></div>

            </div>

            <div class="col-md-4">

                <a href="listado-productos.php?categoria=10">

                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="600"
                        style="background-image:url('img/demo/content/01_Bowl_Mix de Berries_web.jpg');background-size:100%;background-repeat: no-repeat;text-align: center;font-size:45px;line-height:360px;text-shadow: 2px 2px 2px #333333;">

                        frutas congeladas

                    </div>
                </a>

                <div class="white-space space-small"></div>

            </div>

            <div class="col-md-4">

                <a href="listado-productos.php?categoria=18">

                    <!-- <img src="img/demo/content/01_Bowl_Primavera_web.jpg" class="img-responsive aligncenter animation fadeInUp" alt="Verduras congeladas"> -->

                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="600"
                        style="background-image:url('img/demo/content/01_Bowl_Primavera_web.jpg');background-size:100%;background-repeat: no-repeat;text-align: center;font-size:45px;line-height:360px;text-shadow: 2px 2px 2px #333333;">

                        <!-- <img src="img/demo/content/01_Bowl_Primavera_web.jpg" style="visibility: hidden;" /> -->

                        verduras congeladas

                    </div>
                </a>

                <div class="white-space space-small"></div>



            </div>

            <div class="col-md-4">

                <a href="listado-productos.php?categoria=17">

                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="600"
                        style="background-image:url('img/demo/content/jugos_categoria.jpg');background-size:100%;background-repeat: no-repeat;text-align: center;font-size:45px;line-height:360px;text-shadow: 2px 2px 2px #333333;">

                        jugos naturales

                    </div>
                </a>

                <div class="white-space space-small"></div>



            </div>

            <div class="col-md-4">

                <a href="listado-productos.php?categoria=11">

                    <div class="tp-caption large_bold_white script-font customin customout tp-resizeme rs-parallaxlevel-3"
                        data-x="center" data-y="600"
                        style="background-image:url('img/demo/content/frutos_secos_categoria.jpg');background-size:100%;background-repeat: no-repeat;text-align: center;font-size:45px;line-height:360px;text-shadow: 2px 2px 2px #333333;">

                        lafem tienda saludable

                    </div>
                </a>

                <div class="white-space space-small"></div>


            </div>


        </div>



    </div>

    <!-- /Container -->

    <!-- Parallax -->
    <?php include("parallax_gergal.php"); ?>
    <!-- /Parallax -->



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>

        <div class="row">

            <div class="col-md-12">

                <h4 class="fancy-title text-left"><span><strong>Novedades</strong></span></h4>



            </div>

        </div>



        <div class="row">

            <div class="col-md-12">

                <!-- Carousel -->

                <div class="carousel-box">

                    <div class="carousel carousel-nav-out carousel-simple" data-carousel-autoplay="false"
                        data-carousel-items="4" data-carousel-nav="true" data-carousel-pagination="false"
                        data-carousel-speed="1000">

                        <?php echo (new Producto())->carrousel_items(6);?>



                    </div>

                </div>

                <!-- /Carousel -->

                <div class="white-space space-big"></div>

            </div>

        </div>

    </div>

    <!-- /Container -->





    <!-- Container Recetas desactivado-->





    <!-- <div class="fullsize bg-color-light">

            <div class="row">

                    <div class="col-md-12">

                        <div class="white-space space-medium"></div>

                        	<h1 class="script-font text-center color-rojo">Recetas</h1>

                            

                        </div>

                    </div>

            	<div id="recetas" class="white-space space-medium"></div>

            

                <div  class="container bg-color-white">

                	<div class="white-space space-small"></div>

                	

					<div class="row">

                    	<div class="col-md-12">

							-->



    <!-- Carousel desactivado

						

						<div class="carousel-box" >

								<div class="carousel carousel-simple" data-carousel-autoplay="9000" data-carousel-items="3" data-carousel-nav="false" data-carousel-pagination="true"  data-carousel-speed="1000">

								<?php 

									//echo (new Receta())->get_carousel_boxes(0,6);

								?>

								

                                                                        

								</div>

                        	</div>

                        -->

    <!--	

					</div>

                    </div>

                    <div class="white-space space-big"></div>

                </div> 

                </div> 

            /cierra el Container de recetas (desactivado) -->



</div>

<!-- /Main Container -->



<!-- Footer Container -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->



<!-- login modal -->

<?php include 'loginView_test.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts_test.php'?>

<script>
document.title = "Gergal - Inicio";
</script>

</body>

</html>