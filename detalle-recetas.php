<?php 
	require_once 'clases/Receta.php';
	require_once 'clases/Producto.php';
	require_once 'clases/CarritoCompras.php';
	$cart = new CarritoCompras	;
	require_once 'main_head.php';
	require_once 'header.php'; 
	$r = new Receta();
?>

		<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras" data-stellar-background-ratio="0.4">
			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h1 class="script-font" style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">Recetas</h1>                    
                    	</div>
                    	
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            
            	<div class="row">

					<div class="col-sm-12">                    
                    	<div class="white-space space-big"></div>
                        
                    	<!-- Blog Content -->
                        <div class="row">                        
							<?php
							if(isset($_GET) && array_key_exists('id',$_GET)){
								$rx = $r->getReceta($_GET['id']);
								if($rx){
							?>
							
							<div class="col-sm-12">
								<!-- blog post -->
								<div class="blog-post post-format-gallery">
                                	<div class="blog-post-content">
										<div class="post-media">
									    	<img src="<?php echo $rx['image_path']?>" width="1225" height="550" alt="Blog One">
                                        </div>
										<div class="post-info">
											<h3 class="post-title"><a href="#" class="color-verde"><?php echo $rx['titulo']?></a></h3>
                    						
										</div>  
										<div class="post-content">
                                        <strong>INGREDIENTES</strong><p><?php echo $rx['ingredientes']?></p>
                                        <strong>PREPARACIÓN</strong><p><?php echo $rx['preparacion']?></p>
                                        </div>
                                    </div> 
                                </div>
								<!-- /blog post -->                             
							  </div>
							<!-- /Blog Content -->
							<?	}
						  } ?>		
                        </div>
                    	<!-- <div class="white-space space-big"></div> -->
                    </div>
                </div>          
            </div>
			<!-- /Container -->
		</div>
		<!-- /Main Container --> 
        
        <!-- Container Prensa -->
            <!-- Fullsize -->
            <div class="fullsize bg-color-light">
            <div class="row">
                    	<div class="col-md-12">
                        <div class="white-space space-medium"></div>
                        	<h1 class="script-font text-center color-rojo">Últimas Recetas</h1>
                        </div>
                    </div>
            	<div class="white-space space-medium"></div>
            
                <div class="container bg-color-white">
                	<div class="white-space space-small"></div>
                	
					<div class="row">
                    	<div class="col-md-12">
							<!-- Carousel -->
							<div class="carousel-box" >
								<div class="carousel carousel-simple" data-carousel-autoplay="9000" data-carousel-items="3" data-carousel-nav="false" data-carousel-pagination="true"  data-carousel-speed="1000">
                                <?php 
									
									echo $r->get_carousel_boxes(0,6);
								?>
								</div>
                        	</div>
                            <!-- /Carousel -->
                        </div>
                    </div>
                    <div class="white-space space-big"></div>
                </div> 
                </div> 
            <!-- /Container -->      

		<!-- Footer Container -->
		<?php include "footer.php"; ?>
		<!-- /Footer Container -->

	</div>	

	<!-- Back To Top -->
	<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>
	<!-- /Back To Top -->
 
	<!-- login modal -->
	<?php include 'loginView.php'?>

	<!-- scripts del template -->
	<?php include 'theme_scripts.php'?>
	<!-- custom scripts -->
	<?php include 'custom_scripts.php'?>
	<script>
		document.title = "Gergal - Detalle de Receta" ;
	</script>
  </body>
</html>