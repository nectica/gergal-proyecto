<?php 
	require_once 'clases/Receta.php';
	require_once 'clases/Producto.php';
	require_once 'clases/CarritoCompras.php';
	$cart = new CarritoCompras	;
	require_once 'main_head.php';
	require_once 'header.php'; 
?>
		<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras" data-stellar-background-ratio="0.4">
			
        

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h1 class="script-font" style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">Recetas</h1>                    
                    	</div>
                    	
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            
            	<div class="row">

					<div class="col-sm-12">                    
                    	<div class="white-space space-big"></div>
                        
                    	<!-- /Blog Content -->
                        <div class="row">                        
                            <div class="col-sm-12">
								<div class="portfolio clearfix">
									<div class="portfolio-grid portfolio-3-cols portfolio-classic">
									<?php
										if(isset($_GET) && array_key_exists('offset',$_GET)){
											$offset = $_GET['offset'];
											$limit = $_GET['limit'];
											$currPage = $_GET['currPage'];
											
										}else{
											$offset = 0;
											$limit = 6;
											$currPage = 1;
										}

										$c = (new Receta())->getRecetas($offset,$limit);
										// echo 'pages:'.$c['pages'];
										$pages = $c['pages'];
										if(count($c['boxes']) > 0){
											foreach($c['boxes'] as $item){
									?>
	                                	<!-- item receta -->
                                    	<div class="element isotope-item">
                                        	<div class="element-inner">
											<!-- blog post -->
												<div class="blog-post post-format-image">
													<div class="blog-post-content">
														<div class="post-media">
									    				<img src="<?php echo $item['image_path']?>" width="1225" height="550" alt="Blog One">
													</div>
													<div class="post-info">
														<h5><a href="detalle-recetas.php?id=<?php echo $item['id']?>" class="color-verde"><?php echo $item['titulo']?></a></h5>
													</div>  
													<div class="post-content" style="margin-bottom:10px;min-height:130px;max-height:130px;overflow:hidden;">
														<p><?php echo $item['ingredientes']?></p>
													</div>
														<a class="btn btn-primary btn-sm" href="detalle-recetas.php?id=<?php echo $item['id']?>">Leer receta<i class="fa iconright fa-arrow-circle-right"></i></a>
													</div>
												</div>
											<!-- /blog post -->                                       
                                        	</div>
                                        </div>
                                         <!-- item receta -->
										 <?php } } ?>		
                                        
                            </div>
                        </div>
                    	<!-- /Blog Content -->
                        
                    	<!-- Pagination -->
                        <div class="row">                        
                            <div class="col-sm-12">
                            	<div class="text-center">
			                		<ul class="pagination">
									<?php 
											if($pages > 1 ){
												$last = $currPage - 1;
												$currPage -= 1;
												if($last > 0){
													$x = $offset - $limit;
													echo "<li><a href={$_SERVER['PHP_SELF']}?offset={$x}&limit={$limit}&currPage={$currPage}><i class='fa fa-angle-left'></i></a></li>";
												}
												echo "<li id='pbot_1'><a href={$_SERVER['PHP_SELF']}?offset=0&limit={$limit}&currPage=1>1</a></li>";
												for ($i=1; $i < $pages ; $i++) { 
													$p = $i+1;
													$l = $limit*$i;
													echo "<li id='pbot_{$p}'><a href={$_SERVER['PHP_SELF']}?offset={$l}&limit={$limit}&currPage={$p}>$p</a></li>";
												}
												$next = $currPage+1;
												if($next < $pages){
													$n = $offset += $limit;
													echo "<li><a href={$_SERVER['PHP_SELF']}?ofset={$n}&limit={$limit}&currPage={$next}><i class='fa fa-angle-right'></i></a></li>";
												}
											}
										?>			
									
									</ul>
                                </div>
                            </div>                                                  
                        </div>
                    	<!-- /Pagination -->
						
                        <div class="white-space space-big"></div>
                    </div>
                
                </div>          
            	
            </div>
			<!-- /Container -->
                                   
		</div>
		<!-- /Main Container -->      

		<!-- Footer Container -->
		<?php include("footer.php"); ?>
		<!-- /Footer Container -->
	</div>	
	<!-- Back To Top -->
	<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>
	<!-- /Back To Top -->

	<!-- login modal -->
	<?php include 'loginView.php'?>

	<!-- scripts del template -->
	<?php include 'theme_scripts.php'?>
<!-- custom scripts -->
<?php include 'custom_scripts.php'?>
	<script>
		document.title = "Gergal - Inicio" ;
	</script>
  </body>
</html>