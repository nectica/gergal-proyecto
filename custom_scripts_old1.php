<!-- Geocoder Api -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBD9PqkcjYtEfSuqLCP_gSOxx-aP4XpD4Q"></script>


<script>
		function updateCartItem(obj,id){
            $.get("carroAccion.php", {action:"updateCartItem", id:id, cant:obj.value}, function(data){
                console.log('receive',data)
                if(data !== 'ok'){
                    alert('falló la conexion con el servidor, intente nuevamente.')
                }
            });
        }
        
        function callCarroAccion(orig,currId){
			let item = orig+'_'+currId;
			let currCant = $("#"+item).val();
			$.get("carroAccion.php", {action:"addToCart", id:currId, cant:currCant}, function(data){
				console.log('respuesta de carro accion',data)
				const res = JSON.parse(data);
				 
				$('#headerCarroCantItems').html(res.items);
				$('#headerCarroTotal').html(res.total.toFixed(2));
			});
			
			// window.location.href=`carroAccion.php?action=addToCart&id=${id}&cant=${cant};return false;`;
			// console.log('showwww')
			// toast show
			var alert = $(".alert-container");
			alert.addClass('z-index',10000);
			alert.slideDown();
				window.setTimeout(function() {
				alert.slideUp();
				}, 2500);
		}
		// buscador en NavBar
		function search(){
			const s = $('#search_txt').val().replace(/[^A-Za-z0-9-ñÑáéíóúÁÉÍÓÚ]+/g, "");
			window.location.assign(`listado-productos.php?search=${s}`)
		}
		// orden de productos en listado 
		function selectOrder(){
			const order = $('#orderSelection').val();
			const currPage = <?php echo (isset($_REQUEST['page']))?$_REQUEST['page']:1 ;?> ;
			const currCat = <?php echo (isset($cat))?$cat:0;?>;
			const currSearch = '';
			window.location.href=`listado-productos.php?categoria=${currCat}&search=${currSearch}&page=${currPage}&order=${order}`;
		}
	
		// validacion de login
		function validateRecover(){
			const email = $('#recover-email').val();
			const elem = 'fg-recover';
			form = $('#Forgot-Password-Form')
			if(validateEmail(email,elem) == 1){
				console.log('sending...',form.serialize())
				$.ajax({
					type: "POST",
					url: 'loginAccion.php',
					data: form.serialize(), 
					
					success: function(data)
					{
                        console.log('res',data); // respuesta del servidor
                        window.location.href="index.php";
					}
					});
				alert("Se envió un email con instrucciones para resetear la clave de acceso");
				return false
			}else{
 				alert("El email ingresado no pertenece a un usuario registrado.");
				return false
			}
		}
		function validateAddress(){
			const lt = $('#latitud').val();
			const lg = $('#longitud').val();
			if(lt == 0 || lg == 0){
				alert('debe seleccionar calle, numero y localidad entre las opciones desplegadas. ')
				// $('#signup-direccion').val('');
				$('#fg-direccion').removeClass('has-success').addClass('has-error');
				return 0;
			}else{
				$('#fg-direccion').removeClass('has-error').addClass('has-success');
				return 1;
			}
		}

		function resetLatLong(){
			$('#latitud').val(0);
			$('#longitud').val(0);
		}
		function resetItem(i){
			if(i == 'signup-direccion'){
				resetLatLong();
				$('#fg-'+i).removeClass('has-error').removeClass('has-success');
			}else{
				$('#'+i).val('');
				$('#fg-'+i).removeClass('has-error').removeClass('has-success');
			}
		}
		
		function checkDni(){
			const dni = $('#signup-dni').val();
			console.log('dni chek',(parseInt(dni) < 1000000 || isNaN(parseInt(dni)) ));
			if(parseInt(dni) < 1000000 || isNaN(parseInt(dni))){
				validateNotEmpty($('#signup-dni').val(),'fg-signup-dni');
				$('#fg-signup-dni').removeClass('has-success').addClass('has-error');
				// $('#signup-dni').val('Nro DNI No Valido');
				// $('#signup-dni').focus();
				alert('Nro de DNI no valido');
				return 0
			}else{
				$('#fg-signup-dni').removeClass('has-error').addClass('has-success');
				return 1;
			}
			
		}
		function validateSignup(){
			let vt = 0;
			vt += validateText($('#signup-nombre').val(),'fg-signup-nombre');
			vt += validateText($('#signup-apellido').val(),'fg-signup-apellido');
			vt += checkDni($('#signup-dni').val(),'fg-signup-dni');
			vt += validateAddress($('#signup-direccion').val(),'fg-signup-direccion');
			vt += validateText($('#signup-telefono').val(),'fg-signup-telefono');
			vt += validateEmail($('#signup-email').val(),'fg-signup-email');
			vt += validatePass($('#signup-password').val(),'fg-signup-password');
			if( vt == 7 ){
				form = $('#signin-form')
				// console.log('sending',form.serialize());
				$.ajax({
					type: "POST",
					url: 'loginAccion.php',
					data: form.serialize(), 
					success: function(data)
					{
                        console.log('res',data); // respuesta del servidor
                        window.location.assign(data);
					}
					});
				
			}else{
				console.log('NOT sending');
				alert('Debe completar los campos requeridos')
				return false;	
			}
			return false;
		}
		function validateLogin(){
			const email = $('#login-email').val();
			const pass = $('#login-password').val();
			form = $('#login-form')
			if(validateEmail(email,'fg-login-email') == 1 && validatePass(pass,'fg-login-password') == 1){
				$.ajax({
					type: "POST",
					url: 'loginAccion.php',
					data: form.serialize(), 
                    success: function(data)
					{
                        console.log('res',data); // respuesta del servidor
                        window.location.assign(data);
					}
					});
			}else{
				console.log('NOT sending');
				alert('Usuario o clave no valido');
				return false
			}
		}
		function validateEmail(email,formGroup) {
			const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			const x = re.test(String(email).toLowerCase());
			if(x){
				$('#'+formGroup).removeClass('has-error').addClass('has-success');
				return 1
			}else{
				$('#'+formGroup).removeClass('has-success').addClass('has-error');
				return false
			}
		}
		function validatePass(pass,formGroup) {
			const re = /^[~`!@#$%^&*()_+-=a-z0-9]+$/i;
			const x = re.test(String(pass));
			if(x){
				$('#'+formGroup).removeClass('has-error').addClass('has-success');
				return 1
			}else{
				$('#'+formGroup).removeClass('has-success').addClass('has-error');
				return false
			}
			return false
		}
		function validateText(v,formGroup) {
			const re = /^[A-Za-z0-9-_, ]+$/g;
			const x = re.test(String(v));
			if(x){
				$('#'+formGroup).removeClass('has-error').addClass('has-success');
				return 1
			}else{
				$('#'+formGroup).removeClass('has-success').addClass('has-error');
				return 0
			}
			return false
		}
		// function validateConfirm(a,b,fg){
		// 	if(a == b){
		// 		$('#'+fg).removeClass('has-error').addClass('has-success');
		// 		return true
		// 	}else{
		// 		$('#'+fg).removeClass('has-success').addClass('has-error');
		// 		return false
		// 	}
		// 	return false
		// }
		function validateNotEmpty(v,formGroup) {
			if(v.length > 0){
				$('#'+formGroup).removeClass('has-error').addClass('has-success');
				return true
			}else{
				$('#'+formGroup).removeClass('has-success').addClass('has-error');
				return false
			}
			return false
		}
		
		// end validacion de login

		function seleccionaRevendedor(){
			if($('input[name=revendedor]:checked').val() === undefined){
				alert('Debes seleccionar un Revendedor');
				return false;
			}
			console.log('radio selected',$('input[name=revendedor]:checked').val())
			enviarPedidoConRevendedor($('input[name=revendedor]:checked').val())
		}

        function enviarPedidoConRevendedor(revId){
			window.location.href=`carroAccion.php?action=placeOrder&revId=${revId}`;
		}
		function enviarPedidoSinRevendedor(){
			window.location.href=`carroAccion.php?action=placeOrder&revId=${0}&forma_pago=`+$('input[name=forma_pago]:checked').val();
		}

		function enviosCaba(){
			
			$('#modalTitle').html('Envios C.A.B.A')
			$('#modal-content').html("<img src='img/envios-caba.jpeg' width='100%' alt='Mapa de envios C.A.B.A'>")
			$('#login-signup-modal').modal({
				keyboard: false
			})
		}

		function enviosGba(){
			// $('#ModalCenter').modal()
			$('#modalTitle').html('Envios Gran Buenos Aires')
			$('#modal-content').html("<img src='img/envios-gba.jpeg' width='100%' alt='Mapa de envios GBA'>")
			$('#ModalCenter').modal({
				keyboard: false
			})
		}
        
	// Init --- 
		$(document).ready(function(){
			if (window.innerWidth <= 800) {
				$('.size-wrapper_prod').removeClass('size-wrapper_prod');
				$('.size-wrapper_catg').removeClass('size-wrapper_catg');
			}
			
			<?php 
			// $log = 'session:'.json_encode($_SESSION);
			// file_put_contents('log/errors.txt', $log, FILE_APPEND);
			
			?>
			// ******** elementos del header
			<?php
			if(isset($cart) && isset($_SESSION)){
			?>
			$('#headerCarroCantItems').html(<?php echo (isset($cart))?$cart->total_items():0; ?>)
			$('#headerCarroTotal').html(<?php echo (Array_key_exists('tipo_usuario_id',$_SESSION) && $_SESSION['tipo_usuario_id'] == 2)?$cart->total_revendedor():$cart->total(); ?>)
			<?php
			}
			?>
			
			
			
			$('#search_txt').keypress(function(e) {
				var keycode = (e.keyCode ? e.keyCode : e.which);
				if (keycode == '13') {
					search();
					e.preventDefault();
					return false;
				}
			});	
			//*** Modal de Login Mi Cuenta  
			$('#login-signup-modal').on('shown.bs.modal', function (e) {
				// EVITA EL SCROLLING
				document.body.style.overflow = "hidden"; 
    			document.body.style.height = "100%"; 
			})
			$('#login-signup-modal').on('hidden.bs.modal', function (e) {
				// DEVUELVE EL SCROLLING
				document.body.style.overflow = "auto";
				document.body.style.height = "auto"; 
			})
			$('#signupModal').click(function(){         
				$('#login-modal-content').fadeOut('fast', function(){
					$('#signup-modal-content').fadeIn('fast');
				});
			});
			$('#loginModal').click(function(){          
				$('#signup-modal-content').fadeOut('fast', function(){
					$('#login-modal-content').fadeIn('fast');
				});
			});
			$('#FPModal').click(function(){         
				$('#login-modal-content').fadeOut('fast', function(){
					$('#forgot-password-modal-content').fadeIn('fast');
				});
			});
			$('#loginModal1').click(function(){          
				$('#forgot-password-modal-content').fadeOut('fast', function(){
					$('#login-modal-content').fadeIn('fast');
				});
			});
			$('#signup-modal-content').hide()
			$('#forgot-password-modal-content').hide()
			
			var input = document.getElementById('signup-direccion');
			var autocomplete = new google.maps.places.Autocomplete(input, {
				types: ['geocode'],
				componentRestrictions: {
					country: "ARG"
				}
			});
			
			google.maps.event.addListener(autocomplete, 'place_changed', function () {
				var near_place = autocomplete.getPlace();
				document.getElementById('latitud').value = near_place.geometry.location.lat();
				document.getElementById('longitud').value = near_place.geometry.location.lng();
			});

			
			$('.main-wrapper').append("<div class='alert-container'><div class='custom-alert'>Se agrego al carrito de compras...</div></div>");
			$('.alert-container').hide();




			// TEST DE NOTIFICACION DE PAGOS
			// $.ajax({
			// 	type: "POST",
			// 	url: 'notificacion_pago_mp.php',
			// 	data: JSON.stringify({"action":"payment.created","api_version":"v1","data":{"id":"1232346204"},"date_created":"2020-12-24T13:56:32Z","id":6837579497,"live_mode":false,"type":"payment","user_id":"692348675"}), 
			// 	success: function(data)
			// 	{
            //     	console.log('res',data); // respuesta del servidor
            //         window.location.assign(data);
			// 	}
			// });
			
		});
		// $( window ).resize(function() {
		// 	if (window.innerWidth <= 800) {
		// 		$('img').removeClass('size-wrapper');
		// 	}else{
		// 		$('img').addClass('size-wrapper');
		// 	}
		// });
		

	</script>