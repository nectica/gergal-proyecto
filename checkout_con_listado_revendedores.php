<?php 

	require_once 'clases/Receta.php';

	require_once 'clases/Producto.php';

	require_once 'clases/CarritoCompras.php';

	require_once 'clases/checkout.php';

	$cart = new CarritoCompras	;

	$chkOut = (isset($_SESSION['loggedInId']))?new Checkout($_SESSION['loggedInId']):false;

	require_once 'main_head.php';

	require_once 'header.php'; 

?>



<!-- Main Wrapper Header -->

<div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras"
    data-stellar-background-ratio="0.4">

    <div class="container">

        <div class="row">

            <div class="col-sm-12 columns">

                <div class="page-title">

                    <h1 class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        Checkout</h1>

                </div>

            </div>

        </div>

    </div>

</div>

<!-- /Main Wrapper Header -->

<!-- /Main Wrapper Header -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>

        <div class="row">

            <div class="col-sm-6">

                <h3 class="fancy-title"><span>Seleccioná un revendedor</span></h3>

                <div class="row">

                    <div class="col-md-12">

                        <p></p>

                        <ul class="list-default divider-list biglist list-unstyled">

                            <?php 

									if(!empty($chkOut)){

										$rv = $chkOut->getRevendedoresCercanos();

										foreach ($rv as $key => $rev) {

											echo "<li>

											<input name='revendedor' type='radio' value='{$rev['id']}' > <span class='cl-effect-1'><a>{$rev['nombre']}</a></span><br>{$rev['email']} | Tel: {$rev['telefono']}</li>

											";

										}

										echo "</div>

										</div>	

										<div class='row'>

											<div class='col-md-12'>

												<div class='btn btn-primary pull-left' onClick='seleccionaRevendedor()'>Seleccionar Revendedor <i class='fa fa-angle-right fa-margin-left'></i></div>

											</div>

										</div>";

									}else{

										 echo "</div></div>	

										 <div class='row'>

											 <div class='col-md-12'>

												 <div class='btn btn-primary pull-left' data-toggle='modal' data-target='#login-signup-modal'>Clickea Aqui para Ingresar a tu cuenta o Registrarte <i class='fa fa-angle-right fa-margin-left'></i></div>

											 </div>

										 </div>

										";

									}

								?>

                            <!-- <div class="white-space space-big"></div> -->

                    </div>

                    <div class="col-md-6">

                        <h3 class="fancy-title"><span>Formas de Pago</span></h3>

                        <h5>Podés abonar tu pedido en efectivo al recibirlo, o por transferencia bancaria, </h5>

                        <ul class="list-default divider-list biglist list-unstyled">

                            <li>

                                <input name="forma_pago" type="radio" value="efectivo"> <span
                                    class="cl-effect-1"><a>Pago En efectivo al recibir la compra</a></span><br>

                            </li>

                            <li>

                                <input type="radio" name="forma_pago" value="transferencia"><span
                                    class='cl-effect-1'><a> Transferencia Bancaria</a></span><br />Banco Galicia -
                                Sucursal 64 Cuenta Corriente: GERGAL S.A. ALIAS BRONCE.TUNICA.INDIGO Nº
                                0008161-2/064-2<br />

                                CBU 0070064120000008161224 CUIT 30-63265369-3

                            </li>

                        </ul>

                        <div class="row p-4"></div>

                        <div class="row">

                            <div class="col-md-12">

                                <div onClick='enviarPedidoSinRevendedor()' class="btn btn-primary pull-left">Enviar
                                    Pedido <i class="fa fa-angle-right fa-margin-left"></i></div>

                            </div>

                        </div>

                        <div class="white-space space-small"></div>

                    </div>

                    <div class="col-md-12 ">

                        <div class="accordion panel-group" id="accordion3">

                            <div class="panel panel-default">

                                <div class="panel-heading bg-color-white">

                                    <h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed"
                                            data-toggle="collapse" href="#collapse_caba"><span
                                                class="icon gfx-question-1 iconleft"></span>Zonas de envío CABA</a></h5>

                                </div>

                                <div id="collapse_caba" class="panel-collapse collapse">

                                    <div class="panel-body"><img src='img/envios-caba.jpeg' width='100%'
                                            alt='Mapa de envios C.A.B.A'></div>

                                </div>

                            </div>

                            <div class="panel panel-default">

                                <div class="panel-heading bg-color-white">

                                    <h5 class="panel-title"><a class="accordion-toggle collapsed-icon collapsed"
                                            data-toggle="collapse" href="#collapse_gba"><span
                                                class="icon gfx-question-1 iconleft"></span>Zonas de envío GBA</a></h5>

                                </div>

                                <div id="collapse_gba" class="panel-collapse collapse">

                                    <div class="panel-body"><img src='img/envios-gba.jpeg' width='100%'
                                            alt='Mapa de envios GBA'></div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <!-- <div class="white-space space-big"></div> -->

                <!-- /Container -->

            </div>

            <!-- /Main Container -->

            <!-- Parallax -->
            <?php include("parallax_gergal.php"); ?>
            <!-- /Parallax -->



        </div>

        <!-- /Main Container -->



        <!-- Footer Container -->

        <?php include("footer.php"); ?>

        <!-- /Footer Container -->



    </div>



    <!-- Back To Top -->

    <a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

    <!-- /Back To Top -->







    <!-- login modal -->

    <?php include 'loginView.php'?>



    <!-- scripts del template -->

    <?php include 'theme_scripts.php'?>

    <!-- custom scripts -->

    <?php include 'custom_scripts.php'?>

    <script>
    document.title = "Gergal - Check Out";
    </script>