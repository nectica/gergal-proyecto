<?php 

	// require_once 'clases/Receta.php';

	// require_once 'clases/Producto.php';

	// require_once 'clases/CarritoCompras.php';

	// $cart = new CarritoCompras	;

	require_once 'main_head.php';

	require_once 'header_no_loginbar.php'; 

	$email = $_REQUEST['email'];

?>

<!-- Main Wrapper Header -->

<div class="main-wrapper-header fancy-header dark-header parallax parallax-verduras"
    data-stellar-background-ratio="0.4">







    <div class="container">



        <div class="row">

            <div class="col-sm-12 columns">

                <div class="page-title">

                    <h1 class="script-font"
                        style="text-shadow: 2px 2px 2px #333333; font-size:70px; padding-top:20px; padding-bottom:30px">
                        Resetear Clave</h1>

                </div>



            </div>

        </div>



    </div>



</div>

<!-- /Main Wrapper Header -->



<!-- Main Container -->

<div class="main-wrapper">



    <!-- Container -->

    <div class="container">

        <div class="white-space space-big"></div>



        <div class="row">

            <div class="col-md-12">

                <h3 class="fancy-title"><span>Ingresa la nueva clave para el usuario
                        <?echo $email;?>
                    </span></h3>



                <!-- Form -->

                <form method="post" role="form" novalidate="" action="action_reset_password.php"
                    class="form-horizontal">

                    <input type="hidden" name="email" value=<?echo $email;?>>

                    <div class="form-group">

                        <div class="col-sm-8">

                            <input type="password" name="password" class="form-control" id="password" minlength="8"
                                placeholder="Ingresa una clave, minimo 8 caracteres">

                        </div>



                        <div class="col-sm-12">

                            <button type="submit" class="btn btn-primary btn-block">Enviar</button>

                        </div>

                    </div>

                </form>

                <!-- /Form -->



                <div class="white-space space-small"></div>

            </div>

        </div>



        <div class="white-space space-medium"></div>

    </div>

    <!-- /Container -->







</div>

<!-- /Main Container -->



<!-- Container destacado -->

<!-- Fullsize -->

<!-- Parallax -->
<?php include("parallax_gergal.php"); ?>
<!-- /Parallax -->

<!-- /Container -->



<!-- Footer destacado -->

<?php include("footer.php"); ?>

<!-- /Footer Container -->



</div>



<!-- Back To Top -->

<a href="#page-top" class="scrollup smooth-scroll"><span class="fa fa-angle-up"></span></a>

<!-- /Back To Top -->





<!-- login modal -->

<?php include 'loginView.php'?>



<!-- scripts del template -->

<?php include 'theme_scripts.php'?>

<!-- custom scripts -->

<?php include 'custom_scripts.php'?>

<script>
document.title = "Gergal - Contacto";
</script>