<div class="header-wrapper">
	<!-- Header Top Container -->
		<div class="header-top">
			<!-- Container -->
    <div class="container">
        <div class="row">
            <!-- Left -->
            <div class="col-md-6 col-sm-6 columns">
            	<div class="header-top-left">
                	<ul class="social-top">
                    	<li><a href="https://www.facebook.com/Gergal-Berries-103417629786681/" class="ToolTip" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                        <li><a href="https://www.instagram.com/gergalberries/" class="ToolTip" title="Instagram"><span class="fa fa-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <!-- /Left -->
            <!-- Right -->
                <div class="col-md-6 col-sm-6 columns">
                    	<div class="header-top-right">
								<!-- Right Menu -->
									<ul class="top-menu">
										<li><a href="carritoView.php"><i class="fa fa-shopping-cart"></i><span id='headerCarroCantItems'>0 </span> item(s) - $<span id='headerCarroTotal'>0.00</span></a></li>
										<li>											<?if (!isset($_SESSION['loggedIn'])) {?>											<a href="#" data-toggle="modal" data-target="#login-signup-modal">												<span >Mi cuenta</span>											</a>											<?}else{?>											<a href="#" data-toggle="modal" data-target="#login-signup-modal" title="Modificá tu cuenta">												<? echo("&nbsp;&nbsp;&nbsp;".$_SESSION['loggedIn']."&nbsp;&nbsp;<span class=\"glyphicon glyphicon-edit\"></span>&nbsp;"); ?>											</a>											<?}?>										</li>
										<?if (!isset($_SESSION['loggedIn'])) {?>											<li><a href="#" data-toggle="modal" data-target="#login-signup-modal" class="color-rojo"><strong>Soy revendedor</strong></a></li>										<?}?>
										<li><a href="loginAccion.php?action=logout" >Logout</a></li>
									</ul>
								<!-- /Right Menu -->
                        </div>
                    </div>
                    <!-- /right -->
                
                </div>
        
        </div>
				<!-- /Container -->

		    </div>   
			<!-- /Header Top Container -->

			<!-- Header Main Container -->
			<div class="header-main">

        <!-- Container -->
        <div class="container">
            <!-- Main Navigation & Logo -->                    
			<div class="main-navigation">
            	<div class="row">
                    <!-- Main Navigation -->
                    <div class="col-md-12 columns">
							<nav class="navbar navbar-default gfx-mega nav-left" role="navigation">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<a class="navbar-toggle" data-toggle="collapse" data-target="#gfx-collapse"></a>
								<div class="logo">
									<a href="index.php"><img src="img/theme/logo.png" alt="Logo"></a>
								</div>
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="gfx-collapse">
										<ul id='list_navbar' class="nav navbar-nav gfx-nav">
											<li id='li_home'><a href="index.php">Home</a></li>
                            	    <li id='li_productos'><a href="productos.php">Productos</a></li>
                                   
                                    <!-- <li id='li_recetas'><a href="listado-recetas.php" >Recetas</a></li> -->
											<li id='li_instalaciones'><a href="instalaciones.php">Instalaciones</a></li>
                                    <li id='li_contacto'><a href="contacto.php" >Contacto</a></li>
                                    <li id='li_busqueda' ><div class="input-group" style="width:340px; padding-top:26px">
											<input id="search_txt" class="form-control" type="text" placeholder="Buscar productos..."><span class="input-group-btn"><button onClick=search() class="btn btn-primary" type="button"><i class="fa fa-search"></i></button></span>
											</div>
                        			</li>
                        		</ul>
								</div><!-- /.navbar-collapse -->
							</nav>
						</div>
					<!-- /Main Navigation -->
					</div>              
            </div>
         <!-- /Main Navigation & Logo -->
        </div>
		<!-- /Container -->
	</div>   
	<!-- /Header Main Container -->
</div>
